-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Wed Oct  9 15:29:42 2019 
-- * LUN file: F:/00-GITS/p_gesproj2-grp-villegas/1-DB/db-printers.lun 
-- * Schema: db_printers 
-- ********************************************* 


-- Database Section
-- ________________ 

drop database if exists db_printers;
create database db_printers;
use db_printers;


-- Tables Section
-- _____________ 

create table t_builder (
     idBuilder int not null auto_increment,
     buiName varchar(50) not null,
     buiBrand varchar(50) not null,
     constraint ID_t_Builder_ID primary key (idBuilder));

create table t_evoPrice (
     idEvoPrice int not null auto_increment,
     evoDate date not null,
     evoPrice int not null,
     idPrinter int not null,
     constraint ID_t_evoPrice_ID primary key (idEvoPrice));

create table t_ink (
     idInk int not null auto_increment,
     inkName varchar(50) not null,
     inkImageFilePath varchar(100) not null,
     inkType varchar(50) not null,
     inkKind varchar(50) not null,
     inkPrice int not null,
     constraint ID_t_ink_ID primary key (idInk));

create table t_paper (
     idPaper int not null auto_increment,
     papName varchar(50) not null,
     papType varchar(50) not null,
     constraint ID_t_paper_ID primary key (idPaper));

create table t_printer (
     idPrinter int not null auto_increment,
     priName varchar(50) not null,
     priImageFilePath varchar(100) not null,
     priLink varchar(300) not null,
     priPrintResolution varchar(50) not null,
     priScanResolution varchar(50) not null,
     priPrintSpeed int not null,
     priDouble_Sided char not null,
     priWireless char not null,
     priWitdh int not null,
     priLenght int not null,
     priHeight int not null,
     priWeight int not null,
     priDateRelease date not null,
     idTechnology int not null,
     idProvider int not null,
     idBuilder int not null,
     constraint ID_t_printer_ID primary key (idPrinter));

create table t_provider (
     idProvider int not null auto_increment,
     proName varchar(50) not null,
     proImageFilePath varchar(100) not null,
     proLink varchar(255) not null,
     constraint ID_t_provider_ID primary key (idProvider));

create table t_technology (
     idTechnology int not null auto_increment,
     tecType varchar(50) not null,
     constraint ID_t_Technology_ID primary key (idTechnology));

create table t_useInk (
     idInk int not null,
     idPrinter int not null,
     constraint ID_t_use_ink_ID primary key (idPrinter, idInk));

create table t_usePaper (
     idPaper int not null,
     idPrinter int not null,
     constraint ID_t_use_paper_ID primary key (idPrinter, idPaper));


-- Constraints Section
-- ___________________ 

alter table t_evoPrice add constraint FKevoluer_FK
     foreign key (idPrinter)
     references t_printer (idPrinter);

-- Not implemented
-- alter table t_printer add constraint ID_t_printer_CHK
--     check(exists(select * from t_useInk
--                  where t_useInk.idPrinter = idPrinter)); 

-- Not implemented
-- alter table t_printer add constraint ID_t_printer_CHK
--     check(exists(select * from t_usePaper
--                  where t_usePaper.idPrinter = idPrinter)); 

alter table t_printer add constraint FKuse_FK
     foreign key (idTechnology)
     references t_technology (idTechnology);

alter table t_printer add constraint FKsell_FK
     foreign key (idProvider)
     references t_provider (idProvider);

alter table t_printer add constraint FKbuild_FK
     foreign key (idBuilder)
     references t_builder (idBuilder);

alter table t_useInk add constraint FKt_u_t_p
     foreign key (idPrinter)
     references t_printer (idPrinter);

alter table t_useInk add constraint FKt_u_t_i_FK
     foreign key (idInk)
     references t_ink (idInk);

alter table t_usePaper add constraint FKt_u_t_p_2
     foreign key (idPrinter)
     references t_printer (idPrinter);

alter table t_usePaper add constraint FKt_u_t_p_1_FK
     foreign key (idPaper)
     references t_paper (idPaper);


-- Index Section
-- _____________ 

create unique index ID_t_Builder_IND
     on t_builder (idBuilder);

create unique index ID_t_evoPrice_IND
     on t_evoPrice (idEvoPrice);

create index FKevoluer_IND
     on t_evoPrice (idPrinter);

create unique index ID_t_ink_IND
     on t_ink (idInk);

create unique index ID_t_paper_IND
     on t_paper (idPaper);

create unique index ID_t_printer_IND
     on t_printer (idPrinter);

create index FKuse_IND
     on t_printer (idTechnology);

create index FKsell_IND
     on t_printer (idProvider);

create index FKbuild_IND
     on t_printer (idBuilder);

create unique index ID_t_provider_IND
     on t_provider (idProvider);

create unique index ID_t_Technology_IND
     on t_technology (idTechnology);

create unique index ID_t_use_ink_IND
     on t_useInk (idPrinter, idInk);

create index FKt_u_t_i_IND
     on t_useInk (idInk);

create unique index ID_t_use_paper_IND
     on t_usePaper (idPrinter, idPaper);

create index FKt_u_t_p_1_IND
     on t_usePaper (idPaper);

