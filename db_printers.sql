-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Mer 13 Novembre 2019 à 14:27
-- Version du serveur :  5.7.11
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `db_printers`
--
CREATE DATABASE IF NOT EXISTS `db_printers` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_printers`;

-- --------------------------------------------------------

--
-- Structure de la table `t_builder`
--

CREATE TABLE IF NOT EXISTS `t_builder` (
  `idBuilder` int(11) NOT NULL AUTO_INCREMENT,
  `buiName` varchar(50) NOT NULL,
  `buiBrand` varchar(50) NOT NULL,
  PRIMARY KEY (`idBuilder`),
  UNIQUE KEY `ID_t_Builder_IND` (`idBuilder`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_builder`
--

INSERT INTO `t_builder` (`idBuilder`, `buiName`, `buiBrand`) VALUES
(1, 'brother', 'MFC'),
(2, 'brother', 'DCP'),
(3, 'lexmark', '-'),
(4, 'Canon', 'I-Sensys'),
(5, 'Canon', 'PIXMA'),
(6, 'EPSON', 'WorkForce Pro'),
(7, 'EPSON', 'ECOTANK'),
(8, 'EPSON', 'EXPRESSION PREMIUM'),
(9, 'HP', 'Smart'),
(10, 'HP', 'Deskjet'),
(11, 'HP', 'Color LaserJet'),
(12, 'HP', 'LaserJet'),
(13, 'HP', 'OfficeJet Pro'),
(14, 'HP', 'LaserJet Pro'),
(15, 'HP', 'OfficeJet');

-- --------------------------------------------------------

--
-- Structure de la table `t_evoprice`
--

CREATE TABLE IF NOT EXISTS `t_evoprice` (
  `idEvoPrice` int(11) NOT NULL AUTO_INCREMENT,
  `EvoDate` varchar(10) NOT NULL,
  `EvoPrice` double(11,2) NOT NULL,
  `idPrinter` int(11) NOT NULL,
  PRIMARY KEY (`idEvoPrice`),
  UNIQUE KEY `ID_t_evoPrice_IND` (`idEvoPrice`),
  KEY `FKevoluer_IND` (`idPrinter`)
) ENGINE=InnoDB AUTO_INCREMENT=304 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_evoprice`
--

INSERT INTO `t_evoprice` (`idEvoPrice`, `EvoDate`, `EvoPrice`, `idPrinter`) VALUES
(1, '2019-08-01', 265.00, 1),
(2, '2019-09-01', 263.00, 1),
(3, '2019-08-01', 48.45, 2),
(4, '2019-09-01', 51.00, 2),
(5, '2017-11-01', 307.90, 3),
(6, '2017-12-01', 240.65, 3),
(7, '2018-01-01', 280.00, 3),
(8, '2018-02-01', 248.00, 3),
(9, '2018-03-01', 298.80, 3),
(10, '2018-04-01', 258.95, 3),
(11, '2018-05-01', 248.00, 3),
(12, '2018-06-01', 248.00, 3),
(13, '2018-07-01', 248.00, 3),
(14, '2018-08-01', 248.00, 3),
(15, '2018-09-01', 248.00, 3),
(16, '2018-10-01', 229.00, 3),
(17, '2018-11-01', 244.45, 3),
(18, '2018-12-01', 248.00, 3),
(19, '2019-01-01', 248.00, 3),
(20, '2019-02-01', 248.00, 3),
(21, '2019-03-01', 248.00, 3),
(22, '2019-04-01', 248.00, 3),
(23, '2019-05-01', 248.00, 3),
(24, '2019-06-01', 248.00, 3),
(25, '2019-07-01', 248.00, 3),
(26, '2019-08-01', 248.00, 3),
(27, '2019-09-01', 248.00, 3),
(28, '2019-01-01', 792.00, 4),
(29, '2019-02-01', 789.00, 4),
(30, '2019-03-01', 762.00, 4),
(31, '2019-04-01', 762.00, 4),
(32, '2019-05-01', 679.00, 4),
(33, '2019-06-01', 762.00, 4),
(34, '2019-07-01', 762.00, 4),
(35, '2019-08-01', 762.00, 4),
(36, '2019-09-01', 762.00, 4),
(37, '2019-07-01', 186.16, 5),
(38, '2019-08-01', 180.95, 5),
(39, '2019-09-01', 198.00, 5),
(40, '2017-01-01', 235.00, 6),
(41, '2017-02-01', 233.35, 6),
(42, '2017-03-01', 226.75, 6),
(43, '2017-04-01', 213.55, 6),
(44, '2017-05-01', 214.20, 6),
(45, '2017-06-01', 202.80, 6),
(46, '2017-07-01', 199.80, 6),
(47, '2017-08-01', 199.80, 6),
(48, '2017-09-01', 155.95, 6),
(49, '2017-10-01', 192.90, 6),
(50, '2017-11-01', 189.00, 6),
(51, '2017-12-01', 176.00, 6),
(52, '2018-01-01', 174.00, 6),
(53, '2018-02-01', 189.90, 6),
(54, '2018-03-01', 188.00, 6),
(55, '2018-04-01', 189.90, 6),
(56, '2018-05-01', 187.80, 6),
(57, '2018-06-01', 187.85, 6),
(58, '2018-07-01', 179.80, 6),
(59, '2018-08-01', 179.80, 6),
(60, '2018-09-01', 174.80, 6),
(61, '2018-10-01', 179.80, 6),
(62, '2018-11-01', 177.80, 6),
(63, '2018-12-01', 177.90, 6),
(64, '2019-01-01', 175.00, 6),
(65, '2019-02-01', 175.00, 6),
(66, '2019-03-01', 172.20, 6),
(67, '2019-04-01', 172.20, 6),
(68, '2019-05-01', 169.00, 6),
(69, '2019-06-01', 169.00, 6),
(70, '2019-07-01', 137.00, 6),
(71, '2019-08-01', 172.00, 6),
(72, '2019-09-01', 172.00, 6),
(73, '2019-08-01', 130.02, 7),
(74, '2019-09-01', 120.00, 7),
(75, '2017-10-01', 279.00, 8),
(76, '2017-11-01', 279.00, 8),
(77, '2017-12-01', 294.00, 8),
(78, '2018-01-01', 297.00, 8),
(79, '2018-02-01', 297.90, 8),
(80, '2018-03-01', 299.00, 8),
(81, '2018-04-01', 302.10, 8),
(82, '2018-05-01', 295.75, 8),
(83, '2018-06-01', 295.75, 8),
(84, '2018-07-01', 295.75, 8),
(85, '2018-08-01', 284.00, 8),
(86, '2018-09-01', 264.90, 8),
(87, '2018-10-01', 290.00, 8),
(88, '2018-11-01', 239.70, 8),
(89, '2018-12-01', 275.90, 8),
(90, '2019-01-01', 272.00, 8),
(91, '2019-02-01', 268.90, 8),
(92, '2019-03-01', 272.00, 8),
(93, '2019-04-01', 278.90, 8),
(94, '2019-05-01', 277.50, 8),
(95, '2019-06-01', 269.05, 8),
(96, '2019-07-01', 269.05, 8),
(97, '2019-08-01', 269.00, 8),
(98, '2019-09-01', 279.00, 8),
(99, '2017-01-01', 229.35, 9),
(100, '2017-02-01', 229.35, 9),
(101, '2017-03-01', 227.00, 9),
(102, '2017-04-01', 230.70, 9),
(103, '2017-05-01', 236.00, 9),
(104, '2017-06-01', 230.70, 9),
(105, '2017-07-01', 229.95, 9),
(106, '2017-08-01', 167.10, 9),
(107, '2017-09-01', 178.95, 9),
(108, '2017-10-01', 185.00, 9),
(109, '2017-11-01', 229.40, 9),
(110, '2017-12-01', 229.40, 9),
(111, '2018-01-01', 230.45, 9),
(112, '2018-02-01', 230.50, 9),
(113, '2018-03-01', 230.50, 9),
(114, '2018-04-01', 235.95, 9),
(115, '2018-05-01', 212.50, 9),
(116, '2018-06-01', 230.65, 9),
(117, '2018-07-01', 230.65, 9),
(118, '2018-08-01', 230.65, 9),
(119, '2018-09-01', 226.45, 9),
(120, '2018-10-01', 239.00, 9),
(121, '2018-11-01', 258.00, 9),
(122, '2018-12-01', 275.00, 9),
(123, '2019-01-01', 262.00, 9),
(124, '2019-02-01', 273.00, 9),
(125, '2019-03-01', 270.00, 9),
(126, '2019-04-01', 277.00, 9),
(127, '2019-05-01', 260.00, 9),
(128, '2019-06-01', 265.00, 9),
(129, '2019-07-01', 255.00, 9),
(130, '2019-08-01', 279.00, 9),
(131, '2019-09-01', 279.00, 9),
(132, '2019-08-01', 64.00, 10),
(133, '2019-09-01', 64.00, 10),
(134, '2019-08-01', 89.90, 11),
(135, '2019-09-01', 90.00, 11),
(136, '2017-01-01', 71.95, 12),
(137, '2017-02-01', 72.85, 12),
(138, '2017-03-01', 72.95, 12),
(139, '2017-04-01', 74.90, 12),
(140, '2017-05-01', 73.95, 12),
(141, '2017-06-01', 76.90, 12),
(142, '2017-07-01', 76.90, 12),
(143, '2017-08-01', 81.90, 12),
(144, '2017-09-01', 82.90, 12),
(145, '2017-10-01', 89.95, 12),
(146, '2017-11-01', 89.95, 12),
(147, '2017-12-01', 89.95, 12),
(148, '2018-01-01', 91.10, 12),
(149, '2018-02-01', 80.00, 12),
(150, '2018-03-01', 99.70, 12),
(151, '2018-04-01', 90.00, 12),
(152, '2018-05-01', 92.00, 12),
(153, '2018-06-01', 89.90, 12),
(154, '2018-07-01', 99.00, 12),
(155, '2018-08-01', 79.00, 12),
(156, '2018-09-01', 79.00, 12),
(157, '2018-10-01', 88.90, 12),
(158, '2018-11-01', 49.00, 12),
(159, '2018-12-01', 86.80, 12),
(160, '2019-01-01', 86.30, 12),
(161, '2019-02-01', 79.95, 12),
(162, '2019-03-01', 79.95, 12),
(163, '2019-04-01', 79.95, 12),
(164, '2019-05-01', 79.95, 12),
(165, '2019-06-01', 79.95, 12),
(166, '2019-07-01', 79.95, 12),
(167, '2019-08-01', 79.95, 12),
(168, '2019-09-01', 83.00, 12),
(169, '2017-01-01', 89.00, 13),
(170, '2017-02-01', 89.00, 13),
(171, '2017-03-01', 80.95, 13),
(172, '2017-04-01', 80.90, 13),
(173, '2017-05-01', 80.95, 13),
(174, '2017-06-01', 77.25, 13),
(175, '2017-07-01', 49.95, 13),
(176, '2017-08-01', 68.90, 13),
(177, '2017-09-01', 68.90, 13),
(178, '2017-10-01', 49.95, 13),
(179, '2017-11-01', 49.95, 13),
(180, '2017-12-01', 61.30, 13),
(181, '2018-01-01', 69.00, 13),
(182, '2018-02-01', 68.90, 13),
(183, '2018-03-01', 68.90, 13),
(184, '2018-04-01', 69.00, 13),
(185, '2018-05-01', 69.90, 13),
(186, '2018-06-01', 49.95, 13),
(187, '2018-07-01', 49.95, 13),
(188, '2018-08-01', 49.00, 13),
(189, '2018-09-01', 59.00, 13),
(190, '2018-10-01', 64.00, 13),
(191, '2018-11-01', 47.90, 13),
(192, '2018-12-01', 58.90, 13),
(193, '2019-01-01', 49.00, 13),
(194, '2019-02-01', 48.50, 13),
(195, '2019-03-01', 49.50, 13),
(196, '2019-04-01', 49.00, 13),
(197, '2019-05-01', 49.95, 13),
(198, '2019-06-01', 59.90, 13),
(199, '2019-07-01', 59.90, 13),
(200, '2019-08-01', 50.00, 13),
(201, '2019-09-01', 50.00, 13),
(202, '2019-07-01', 368.00, 14),
(203, '2019-08-01', 390.95, 14),
(204, '2019-09-01', 408.00, 14),
(205, '2019-08-01', 250.00, 15),
(206, '2019-09-01', 239.00, 15),
(207, '2019-08-01', 299.00, 16),
(208, '2019-09-01', 310.00, 16),
(209, '2019-08-01', 119.00, 17),
(210, '2019-09-01', 119.00, 17),
(211, '2019-08-01', 216.00, 18),
(212, '2019-09-01', 221.00, 18),
(213, '2019-06-01', 493.00, 19),
(214, '2019-07-01', 491.40, 19),
(215, '2019-08-01', 449.00, 19),
(216, '2019-09-01', 461.00, 19),
(217, '2018-11-01', 59.00, 20),
(218, '2018-12-01', 56.20, 20),
(219, '2019-01-01', 56.99, 20),
(220, '2019-02-01', 61.00, 20),
(221, '2019-03-01', 59.70, 20),
(222, '2019-04-01', 58.70, 20),
(223, '2019-05-01', 43.85, 20),
(224, '2019-06-01', 42.90, 20),
(225, '2019-07-01', 42.90, 20),
(226, '2019-08-01', 34.90, 20),
(227, '2019-09-01', 74.00, 20),
(228, '2019-07-01', 98.00, 21),
(229, '2019-08-01', 98.00, 21),
(230, '2019-09-01', 111.00, 21),
(231, '2019-08-01', 285.00, 22),
(232, '2019-09-01', 284.00, 22),
(233, '2018-06-01', 159.90, 23),
(234, '2018-07-01', 169.95, 23),
(235, '2018-08-01', 139.00, 23),
(236, '2018-09-01', 159.00, 23),
(237, '2018-10-01', 159.90, 23),
(238, '2018-11-01', 159.90, 23),
(239, '2018-12-01', 139.90, 23),
(240, '2019-01-01', 139.90, 23),
(241, '2019-02-01', 139.00, 23),
(242, '2019-03-01', 159.90, 23),
(243, '2019-04-01', 182.90, 23),
(244, '2019-05-01', 159.90, 23),
(245, '2019-06-01', 159.95, 23),
(246, '2019-07-01', 159.95, 23),
(247, '2019-08-01', 159.95, 23),
(248, '2019-09-01', 159.95, 23),
(249, '2018-09-01', 113.90, 24),
(250, '2018-10-01', 111.40, 24),
(251, '2018-11-01', 109.80, 24),
(252, '2018-12-01', 93.40, 24),
(253, '2019-01-01', 106.00, 24),
(254, '2019-02-01', 108.90, 24),
(255, '2019-03-01', 107.00, 24),
(256, '2019-04-01', 107.90, 24),
(257, '2019-05-01', 98.90, 24),
(258, '2019-06-01', 108.00, 24),
(259, '2019-07-01', 98.00, 24),
(260, '2019-08-01', 106.90, 24),
(261, '2019-09-01', 107.00, 24),
(262, '2019-02-01', 327.90, 25),
(263, '2019-03-01', 298.90, 25),
(264, '2019-04-01', 323.90, 25),
(265, '2019-05-01', 298.90, 25),
(266, '2019-06-01', 308.20, 25),
(267, '2019-07-01', 306.00, 25),
(268, '2019-08-01', 322.95, 25),
(269, '2019-09-01', 258.00, 25),
(270, '2019-01-01', 255.00, 26),
(271, '2019-02-01', 224.00, 26),
(272, '2019-03-01', 269.90, 26),
(273, '2019-04-01', 224.00, 26),
(274, '2019-05-01', 224.00, 26),
(275, '2019-06-01', 298.00, 26),
(276, '2019-07-01', 277.00, 26),
(277, '2019-08-01', 276.00, 26),
(278, '2019-09-01', 230.00, 26),
(279, '2019-09-01', 230.00, 27),
(280, '2018-11-01', 136.55, 28),
(281, '2018-12-01', 142.90, 28),
(282, '2019-01-01', 141.90, 28),
(283, '2019-02-01', 129.00, 28),
(284, '2019-03-01', 135.00, 28),
(285, '2019-04-01', 142.00, 28),
(286, '2019-05-01', 119.90, 28),
(287, '2019-06-01', 119.90, 28),
(288, '2019-07-01', 98.90, 28),
(289, '2019-08-01', 118.00, 28),
(290, '2019-09-01', 122.00, 28),
(291, '2018-11-01', 96.00, 29),
(292, '2018-12-01', 101.00, 29),
(293, '2019-01-01', 110.00, 29),
(294, '2019-02-01', 99.00, 29),
(295, '2019-03-01', 107.75, 29),
(296, '2019-04-01', 110.50, 29),
(297, '2019-05-01', 110.95, 29),
(298, '2019-06-01', 107.00, 29),
(299, '2019-07-01', 108.00, 29),
(300, '2019-08-01', 98.00, 29),
(301, '2019-09-01', 104.00, 29),
(302, '2019-08-01', 92.00, 30),
(303, '2019-09-01', 92.00, 30);

-- --------------------------------------------------------

--
-- Structure de la table `t_ink`
--

CREATE TABLE IF NOT EXISTS `t_ink` (
  `idInk` int(11) NOT NULL AUTO_INCREMENT,
  `inkName` varchar(50) NOT NULL,
  `inkImageFilePath` char(1) NOT NULL,
  `inkType` varchar(50) NOT NULL,
  `inkPrice` int(11) NOT NULL,
  PRIMARY KEY (`idInk`),
  UNIQUE KEY `ID_t_ink_IND` (`idInk`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_ink`
--

INSERT INTO `t_ink` (`idInk`, `inkName`, `inkImageFilePath`, `inkType`, `inkPrice`) VALUES
(1, 'HP302', '', 'Encre', 15),
(2, 'HP304', '', 'Encre', 11),
(3, 'HP205A', '', 'Laser', 48),
(4, 'HP201x', '', 'Laser', 242),
(5, 'HP963', '', 'Encre', 22),
(6, 'Hp30x', '', 'Laser', 88),
(7, 'HP44a', '', 'Laser', 48),
(8, 'HP62', '', 'Encre', 18),
(9, 'CanonPG-560,CL-561', '', 'Encre', 64),
(10, 'CanonPGI-570PGBK,CLI-571', '', 'Encre', 56),
(11, 'Canon PG-545', '', 'Encre', 31),
(12, 'Canon 055', '', 'Laser', 70),
(13, 'Canon GI-590', '', 'Encre', 10),
(14, 'Canon 051', '', 'Laser', 80),
(15, 'Canon Cli 581', '', 'Encre', 39),
(16, 'Canon 737', '', 'Laser', 72),
(17, 'LC3213', '', 'Encre', 44),
(18, 'TN-243', '', 'Laser', 155),
(19, 'C13T346x4010', '', 'Encre', 44),
(20, 'C13T00Px40', '', 'Encre', 11),
(21, '71B00x0', '', 'Laser', 237),
(22, 'C13T334x4012', '', 'Encre', 13),
(23, 'C13T02Fx4010', '', 'Encre', 13),
(24, 'C13T03Ux4010', '', 'Encre', 16);

-- --------------------------------------------------------

--
-- Structure de la table `t_paper`
--

CREATE TABLE IF NOT EXISTS `t_paper` (
  `idPaper` int(11) NOT NULL AUTO_INCREMENT,
  `papName` varchar(50) NOT NULL,
  PRIMARY KEY (`idPaper`),
  UNIQUE KEY `ID_t_paper_IND` (`idPaper`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_paper`
--

INSERT INTO `t_paper` (`idPaper`, `papName`) VALUES
(1, 'A2'),
(2, 'A4'),
(3, 'A5'),
(4, 'A6'),
(5, 'B5'),
(6, 'B6'),
(7, 'C6'),
(8, 'RA4'),
(9, '16K'),
(10, 'Enveloppes'),
(11, 'CartePostale'),
(12, 'DL'),
(13, 'LTR'),
(14, 'EXE'),
(15, 'Photo'),
(16, 'N10'),
(17, 'LT'),
(18, 'EX'),
(19, 'ETC');

-- --------------------------------------------------------

--
-- Structure de la table `t_printer`
--

CREATE TABLE IF NOT EXISTS `t_printer` (
  `idPrinter` int(11) NOT NULL AUTO_INCREMENT,
  `priName` varchar(100) NOT NULL,
  `priImageFilePath` varchar(100) NOT NULL,
  `priLink` varchar(300) NOT NULL,
  `priPrintResolution` varchar(50) NOT NULL,
  `priScanResolution` varchar(50) NOT NULL,
  `priPrintSpeed` int(11) NOT NULL,
  `priDouble_Sided` varchar(20) NOT NULL,
  `priWireless` varchar(20) NOT NULL,
  `priWitdh` int(11) NOT NULL,
  `priLenght` int(11) NOT NULL,
  `priHeight` int(11) NOT NULL,
  `priWeight` int(11) NOT NULL,
  `priDateRelease` date NOT NULL,
  `idTechnology` int(11) NOT NULL,
  `idProvider` int(11) NOT NULL,
  `idBuilder` int(11) NOT NULL,
  PRIMARY KEY (`idPrinter`),
  UNIQUE KEY `ID_t_printer_IND` (`idPrinter`),
  KEY `FKuse_IND` (`idTechnology`),
  KEY `FKsell_IND` (`idProvider`),
  KEY `FKbuild_IND` (`idBuilder`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_printer`
--

INSERT INTO `t_printer` (`idPrinter`, `priName`, `priImageFilePath`, `priLink`, `priPrintResolution`, `priScanResolution`, `priPrintSpeed`, `priDouble_Sided`, `priWireless`, `priWitdh`, `priLenght`, `priHeight`, `priWeight`, `priDateRelease`, `idTechnology`, `idProvider`, `idBuilder`) VALUES
(1, 'Tank 555', '', 'https://www.digitec.ch/de/s1/product/hp-smart-tank-plus-555-wlan-tinte-farbe-drucker-11806327?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=11806327', '4800x1200', '1200x1200', 5, 'Manuelle', 'Oui', 449, 605, 258, 5, '2019-08-17', 2, 1, 9),
(2, '2620', '', 'https://www.fust.ch/de/p/pc-tablet-handy/drucker-und-scanner/drucker/hp/deskjet-2620-white-8430302.html?utm_source=Toppreise&utm_campaign=Preisvergleich&utm_medium=cpc', '4800x1200', '1200x1200', 3, 'Manuelle', 'Oui', 425, 304, 149, 4, '2019-01-22', 2, 2, 10),
(3, 'MFP M181fw', '', 'https://www.microspot.ch/de/computer-gaming/drucker-scanner/laserdrucker--c531000/hp-pro-mfp-m181fw-laser-led-farbe--p0001415008', '600x600', '1200x1200', 16, 'Manuelle', 'Oui', 420, 340, 380, 16, '2017-11-11', 1, 3, 11),
(4, 'M608dn', '', 'https://www.microspot.ch/de/computer-gaming/drucker-scanner/laserdrucker--c531000/hp-laserjet-enterprise-m608dn--p0001353877', '1200x1200', '1200x1200', 50, 'Automatique', 'Oui', 597, 497, 539, 22, '2019-01-19', 1, 3, 12),
(5, '9010 AIO', '', 'https://www.microspot.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/hp-officejet-pro-9010-aio-couleur-wlan--p0001796386', '4800x1200', '1200x1200', 18, 'Automatique', 'Oui', 487, 325, 409, 9, '2019-07-05', 2, 3, 13),
(6, '8720', '', 'https://www.digitec.ch/fr/s1/product/hp-officejet-pro-8720-wifi-encre-couleur-impression-recto-verso-imprimantes-5755953?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=5755953', '1200x1200', '1200x1200', 20, 'Automatique', 'Oui', 500, 530, 339, 15, '2017-05-11', 2, 1, 13),
(7, 'MFP M28w', '', 'https://www.digitec.ch/fr/s1/product/hp-laserjet-pro-m28w-wifi-laserled-noir-et-blanc-imprimantes-11053582?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=11053582', '600x600', '600x600', 18, 'Manuelle', 'Oui', 360, 264, 197, 5, '2019-08-25', 1, 1, 14),
(8, '250', '', 'https://www.microspot.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/hp-officejet-250-mobile-all-in-one-pour-hp-officejet-250--p0001209925', '4800x1200', '600x600', 6, 'Manuelle', 'Oui', 380, 270, 401, 3, '2017-10-26', 2, 3, 15),
(9, 'M227sdn', '', 'https://www.digitec.ch/fr/s1/product/hp-m227sdn-laserjet-pro-laserled-noir-et-blanc-impression-recto-verso-imprimantes-5982938?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=5982938', '1200x1200', '1200x1200', 18, 'Automatique', 'Non', 403, 407, 490, 9, '2017-01-14', 1, 1, 14),
(10, '3760', '', 'https://www.digitec.ch/fr/s1/product/hp-deskjet-3760-wifi-encre-couleur-imprimantes-9813754?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=9813754', '4800x1200', '1200x1200', 6, 'Manuelle', 'Oui', 403, 177, 141, 2, '2019-08-08', 2, 1, 10),
(11, 'TS5352', '', 'https://www.melectronics.ch/fr/p/785300146749/canon-pixma-ts5352?utm_source=toppreise.ch&utm_medium=affiliate&utm_campaign=2018_affiliate_toppreise', '4800x1200', '1200x1200', 7, 'Automatique', 'Oui', 403, 315, 148, 6, '2019-08-27', 2, 4, 5),
(12, 'MG5750', '', 'https://www.digitec.ch/fr/s1/product/canon-pixma-mg5750-wifi-encre-couleur-impression-recto-verso-imprimantes-5605025?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=5605025', '4800x1200', '1200x1200', 9, 'Automatique', 'Oui', 455, 148, 369, 6, '2017-01-10', 2, 1, 5),
(13, 'TS5050', '', 'https://www.interdiscount.ch/de/computer-gaming/drucker-scanner/tintendrucker--c532000/canon-pixma-ts5050--p0001223483', '4800x1200', '1200x1200', 9, 'Manuelle', 'Oui', 372, 126, 315, 6, '2017-01-25', 2, 5, 5),
(14, 'MF742CDW', '', 'https://www.microspot.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-laser--c531000/canon-i-sensys-mf742cdw-laser-led-couleur--p0001782728', '1200x1200', '600x600', 27, 'Automatique', 'Oui', 471, 469, 460, 27, '2019-06-22', 1, 3, 4),
(15, 'G4510', '', 'https://www.digitec.ch/fr/s1/product/canon-pixma-g4510-megatank-wifi-encre-couleur-imprimantes-8354917?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=8354917', '4800x1200', '1200x1200', 5, 'Manuelle', 'Oui', 445, 197, 330, 7, '2019-08-30', 2, 1, 5),
(16, 'MF264DW', '', 'https://store.canon.ch/fr/canon-imprimante-laser-monochrome-3-en-1-canon-i-sensys-mf264dw/2925C027/', '1200x1200', '9600x9600', 18, 'Automatique', 'Oui', 390, 405, 375, 14, '2019-08-03', 1, 6, 4),
(17, 'TS6350', '', 'https://www.melectronics.ch/fr/p/785300146746/canon-pixma-ts6350?utm_source=toppreise.ch&utm_medium=affiliate&utm_campaign=2018_affiliate_toppreise', '4800x1200', '2400x2400', 10, 'Automatique', 'Oui', 376, 359, 141, 6, '2019-08-20', 2, 4, 5),
(18, 'MF237W', '', 'https://www.digitec.ch/fr/s1/product/canon-i-sensys-mf237w-schwarzweiss-laser-mfp-drucken-kopieren-scannen-faxen-23-seitenmin-laserled-no-10284162?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=10284162', '1200x1200', '600x600', 23, 'Automatique', 'Oui', 390, 360, 371, 11, '2019-08-20', 1, 1, 4),
(19, 'MF744W', '', 'https://www.microspot.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-laser--c531000/canon-i-sensys-mf744cdw-laser-led-couleur--p0001782727', '1200x1200', '9600x9600', 27, 'Automatique', 'Oui', 471, 469, 460, 27, '2019-06-15', 1, 3, 4),
(20, 'MX495', '', 'https://www.microspot.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-%C3%A0-jet-d-encre--c532000/canon-pixma-mx495-schwarz--p0000978082', '4800x1200', '1200x1200', 4, 'Manuelle', 'Oui', 435, 189, 295, 6, '2018-11-25', 2, 3, 5),
(21, 'J497DW', '', 'https://www.microspot.ch/de/computer-gaming/drucker-scanner/tintendrucker--c532000/brother-mfc-j497dw-farbe-wlan-wi-fi--p0001733431', '6000x1200', '2400x2400', 6, 'Automatique', 'Oui', 400, 341, 172, 8, '2019-06-28', 2, 3, 1),
(22, 'L3510CDW', '', 'https://www.alternate.ch/Brother/DCP-L3510CDW-Multifunktionsdrucker/html/product/1472756?campaign=Multifunktionsger?t/Brother/1472756&zanpid=9309_1568209743_0d4b14f47527e6ca1a25dbff924c0fbd&awc=9309_1568209743_0d4b14f47527e6ca1a25dbff924c0fbd', '2400x600', '2400x2400', 18, 'Automatique', 'Oui', 410, 475, 368, 22, '2019-08-22', 1, 7, 2),
(23, 'J895DW', '', 'https://www.fust.ch/de/p/pc-tablet-handy/drucker-und-scanner/drucker/brother/mfc-j895dw-8371054.html?utm_source=Toppreise&utm_campaign=Preisvergleich&utm_medium=cpc', '6000x1200', '2400x2400', 23, 'Automatique', 'Oui', 420, 341, 172, 9, '2018-06-08', 2, 2, 1),
(24, 'WF-3720DWF', '', 'https://www.microspot.ch/de/computer-gaming/drucker-scanner/tintendrucker--c532000/epson-workforce-wf-3720dwf-multifunktionsdrucker--p0001317535', '4800x2400', '2400x2400', 20, 'Automatique', 'Oui', 425, 378, 249, 9, '2018-09-18', 2, 3, 6),
(25, 'ET-4700', '', 'https://www.microspot.ch/de/computer-gaming/drucker-scanner/tintendrucker--c532000/epson-ecotank-et-4700--p0001680795', '5760x1440', '2400x2400', 15, 'Manuelle', 'Oui', 375, 347, 237, 5, '2019-02-25', 2, 3, 7),
(26, 'L3550CDW', '', 'https://www.interdiscount.ch/fr/ordinateurs-et-jeux/imprimantes-et-scanners/imprimantes-laser--c531000/brother-dcp-l3550cdw-laser-led-couleur--p0001575202', '1200x2400', '600x600', 18, 'Automatique', 'Oui', 410, 475, 568, 23, '2019-01-18', 1, 5, 2),
(27, 'CX317dn', '', 'https://www.digitec.ch/de/s1/product/lexmark-cx317dn-laserled-farbe-duplexdruck-drucker-6395859?pcscpId=1&utm_source=toppreise&utm_campaign=preisvergleich&utm_medium=cpc&utm_content=6395859', '2400x600', '600x600', 23, 'Automatique', 'Oui', 567, 558, 594, 27, '2019-09-07', 1, 1, 1),
(28, 'XP-7100', '', 'https://www.microspot.ch/de/computer-gaming/drucker-scanner/tintendrucker--c532000/epson-expression-premium-xp-7100-farbe-wi-fi-wlan--p0001616555', '5760X1440', '4800x4800', 11, 'Automatique', 'Oui', 390, 339, 183, 8, '2018-11-30', 2, 3, 8),
(29, 'XP-6100', '', 'https://www.microspot.ch/de/computer-gaming/drucker-scanner/tintendrucker--c532000/epson-xp-6100-expression-premium--p0001616556', '5760X1440', '4800x4800', 11, 'Automatique', 'Oui', 349, 340, 142, 7, '2018-11-20', 2, 3, 8),
(30, 'WF-2830DWF', '', 'https://www.alternate.ch/Epson/Workforce-WF-2830DWF-Multifunktionsdrucker/html/product/1575794?campaign=Multifunktionsger%C3%A4t/Epson/1575794&zanpid=9309_1569420294_3615ddff68fa817483d692769a2f3c77&awc=9309_1569420294_3615ddff68fa817483d692769a2f3c77', '5760X1440', '2400x2400', 5, 'Automatique', 'Oui', 375, 300, 218, 5, '2019-08-20', 2, 7, 6);

-- --------------------------------------------------------

--
-- Structure de la table `t_provider`
--

CREATE TABLE IF NOT EXISTS `t_provider` (
  `idProvider` int(11) NOT NULL AUTO_INCREMENT,
  `proName` varchar(50) NOT NULL,
  `proImageFilePath` varchar(100) NOT NULL,
  `proLink` varchar(255) NOT NULL,
  PRIMARY KEY (`idProvider`),
  UNIQUE KEY `ID_t_provider_IND` (`idProvider`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_provider`
--

INSERT INTO `t_provider` (`idProvider`, `proName`, `proImageFilePath`, `proLink`) VALUES
(1, 'Digitec', 'https://www.digitec.ch/fr', ''),
(2, 'Fust', 'https://www.fust.ch/fr/home.html', ''),
(3, 'Microspot', 'https://www.microspot.ch/fr', ''),
(4, 'Melectronics', 'https://www.melectronics.ch/fr', ''),
(5, 'Interdiscount', 'https://www.interdiscount.ch/fr', ''),
(6, 'Canon', 'https://fr.canon.ch/', ''),
(7, 'Alternate', 'https://www.alternate.ch/', '');

-- --------------------------------------------------------

--
-- Structure de la table `t_technology`
--

CREATE TABLE IF NOT EXISTS `t_technology` (
  `idTechnology` int(11) NOT NULL AUTO_INCREMENT,
  `tecType` varchar(50) NOT NULL,
  PRIMARY KEY (`idTechnology`),
  UNIQUE KEY `ID_t_Technology_IND` (`idTechnology`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_technology`
--

INSERT INTO `t_technology` (`idTechnology`, `tecType`) VALUES
(1, 'Laser'),
(2, 'Jet d\'encre');

-- --------------------------------------------------------

--
-- Structure de la table `t_useink`
--

CREATE TABLE IF NOT EXISTS `t_useink` (
  `idInk` int(11) NOT NULL,
  `idPrinter` int(11) NOT NULL,
  PRIMARY KEY (`idPrinter`,`idInk`),
  UNIQUE KEY `ID_t_use_ink_IND` (`idPrinter`,`idInk`),
  KEY `FKt_u_t_i_IND` (`idInk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_useink`
--

INSERT INTO `t_useink` (`idInk`, `idPrinter`) VALUES
(1, 1),
(2, 2),
(2, 10),
(3, 3),
(4, 4),
(5, 5),
(5, 6),
(6, 9),
(7, 7),
(8, 8),
(9, 11),
(10, 12),
(10, 13),
(11, 20),
(12, 14),
(12, 19),
(13, 15),
(14, 16),
(15, 17),
(16, 18),
(17, 21),
(17, 23),
(18, 22),
(18, 26),
(19, 24),
(20, 25),
(21, 27),
(22, 28),
(23, 29),
(24, 30);

-- --------------------------------------------------------

--
-- Structure de la table `t_usepaper`
--

CREATE TABLE IF NOT EXISTS `t_usepaper` (
  `idPaper` int(11) NOT NULL,
  `idPrinter` int(11) NOT NULL,
  PRIMARY KEY (`idPrinter`,`idPaper`),
  UNIQUE KEY `ID_t_use_paper_IND` (`idPrinter`,`idPaper`),
  KEY `FKt_u_t_p_1_IND` (`idPaper`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_usepaper`
--

INSERT INTO `t_usepaper` (`idPaper`, `idPrinter`) VALUES
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(3, 1),
(3, 3),
(3, 4),
(3, 7),
(3, 8),
(3, 9),
(3, 11),
(3, 12),
(3, 13),
(3, 14),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 21),
(3, 22),
(3, 24),
(3, 25),
(3, 26),
(3, 27),
(3, 28),
(3, 29),
(3, 30),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 14),
(4, 21),
(4, 22),
(4, 25),
(4, 26),
(4, 28),
(4, 29),
(4, 30),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(5, 6),
(5, 9),
(5, 11),
(5, 12),
(5, 13),
(5, 14),
(5, 16),
(5, 17),
(5, 18),
(5, 19),
(5, 24),
(5, 25),
(5, 28),
(5, 29),
(5, 30),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 8),
(6, 14),
(6, 24),
(6, 25),
(6, 28),
(6, 29),
(6, 30),
(7, 8),
(7, 21),
(7, 24),
(7, 25),
(7, 28),
(7, 29),
(7, 30),
(8, 4),
(8, 14),
(9, 4),
(9, 14),
(10, 3),
(10, 4),
(10, 5),
(10, 6),
(10, 7),
(10, 10),
(10, 12),
(10, 14),
(10, 16),
(10, 18),
(10, 19),
(10, 20),
(11, 4),
(11, 5),
(11, 12),
(11, 14),
(11, 15),
(11, 16),
(11, 19),
(12, 8),
(12, 24),
(12, 25),
(12, 28),
(12, 29),
(12, 30),
(13, 21),
(13, 22),
(13, 27),
(13, 28),
(13, 29),
(13, 30),
(14, 21),
(14, 22),
(15, 21),
(15, 23),
(16, 24),
(17, 25),
(17, 26),
(18, 26),
(18, 27);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_evoprice`
--
ALTER TABLE `t_evoprice`
  ADD CONSTRAINT `FKevoluer_FK` FOREIGN KEY (`idPrinter`) REFERENCES `t_printer` (`idPrinter`);

--
-- Contraintes pour la table `t_printer`
--
ALTER TABLE `t_printer`
  ADD CONSTRAINT `FKbuild_FK` FOREIGN KEY (`idBuilder`) REFERENCES `t_builder` (`idBuilder`),
  ADD CONSTRAINT `FKsell_FK` FOREIGN KEY (`idProvider`) REFERENCES `t_provider` (`idProvider`),
  ADD CONSTRAINT `FKuse_FK` FOREIGN KEY (`idTechnology`) REFERENCES `t_technology` (`idTechnology`);

--
-- Contraintes pour la table `t_useink`
--
ALTER TABLE `t_useink`
  ADD CONSTRAINT `FKt_u_t_i_FK` FOREIGN KEY (`idInk`) REFERENCES `t_ink` (`idInk`),
  ADD CONSTRAINT `FKt_u_t_p` FOREIGN KEY (`idPrinter`) REFERENCES `t_printer` (`idPrinter`);

--
-- Contraintes pour la table `t_usepaper`
--
ALTER TABLE `t_usepaper`
  ADD CONSTRAINT `FKt_u_t_p_1_FK` FOREIGN KEY (`idPaper`) REFERENCES `t_paper` (`idPaper`),
  ADD CONSTRAINT `FKt_u_t_p_2` FOREIGN KEY (`idPrinter`) REFERENCES `t_printer` (`idPrinter`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
