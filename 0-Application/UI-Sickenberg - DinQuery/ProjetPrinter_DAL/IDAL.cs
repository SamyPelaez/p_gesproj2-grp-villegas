﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace ProjetPrinter.Model
{
    interface IDAL
    {
        string ServerName { get; set; }
        string DBName { get; set; }
        string UserName { get; set; }
        string UserPassword { get; set; }

        //variable de connection à la base mySQL
        MySqlConnection mySqlConnection { get; set; }

    }
}
