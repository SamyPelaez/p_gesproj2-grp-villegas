﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using ProjetPrinter.Controller;
using ProjetPrinter.Model;
using ProjetPrinter.View;

namespace ProjetPrinter
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Création de la vue, du model et du contoller 

            /// <summary>
            /// Creation and initialize all the views
            /// </summary>
            MainView mainView = new MainView();
            PrinterView printerView = new PrinterView();
            ChartView chartView = new ChartView();
            LoginView loginView = new LoginView();
            SignView signView = new SignView();
            BasketView basketView = new BasketView();

            /// <summary>
            /// All views are passed to IoC container
            /// </summary>
            List<IView> views = new List<IView>();
            views.Add(mainView);
            views.Add(printerView);
            views.Add(chartView);
            views.Add(loginView);
            views.Add(signView);
            views.Add(basketView);

            /// <summary>
            /// Create and initialize all the models
            /// </summary>
            MainModel mainModel = new MainModel();
            DAL_Printer dalPrinter = new DAL_Printer();

            /// <summary>
            /// All the models are passed to IoC container
            /// </summary>
            //List<IModel> models = new List<IModel>();

            /// <summary>
            /// Creation of the Main Controller and pass both, view and model IoC containers
            /// </summary>
            MainController mainController = new MainController(views, mainModel);
            mainView.LoadComboBoxCollections();
            Application.Run(mainView);
        }
    }
}
