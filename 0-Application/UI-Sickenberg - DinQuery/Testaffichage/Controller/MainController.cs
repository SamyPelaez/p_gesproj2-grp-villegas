﻿using ProjetPrinter.Model;
using ProjetPrinter.View;
using System.Collections.Generic;
using System.Linq;

namespace ProjetPrinter.Controller
{
    public class MainController
    {
        private List<IView> Views { get; set; }
        //private List<IModel> Models { get; set; }
        private MainModel MainModel;
        //private Uwamp aUwamp;

        public MainController(List<IView> views, MainModel _aModel)
        {
            //aUwamp = new Uwamp();

            Views = views;

            MainModel = _aModel;

            /// <summary>
            /// Assign the pertinent controller to their respective views
            /// </summary>
            foreach (IView i in Views)
            {
                i.ViewController = this;
            }


            //aView.UChome = new Home();
        }

        /// <summary>
        /// [ACTION] DEFAULT PrinterModel-PrinterView Transaction
        /// </summary>
        /// <returns></returns>
        public List<PrinterModel> GetDefaultPrinterModel()
        {            
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.Default();
        }

        ///// <summary>
        ///// CUSTOM PrinterModel-PrinterView Transaction
        ///// </summary>
        ///// <returns></returns>
        //public List<PrinterModel> GetCustomPrinterModel(PrinterModel pm)
        //{
        //    DAL_Printer dal_printer = new DAL_Printer();
        //    return dal_printer.Custom(pm);
        //}

        public PrinterView GetDefaultPrinterView()
        {
            PrinterView printerViewUserControl = (PrinterView)Views[1];
            return printerViewUserControl;


        }
        public PrinterView UpdatePrinterView(PrinterModel pm)
        {
            PrinterView printerViewUserControl = (PrinterView)Views[1];
            DAL_Printer dal_printer = new DAL_Printer();
            printerViewUserControl.GetCustomPrinterModel(dal_printer.Custom(pm));
            return printerViewUserControl;


        }
        public void HideDefaultPrinterView(){ PrinterView pv = (PrinterView)Views[1]; pv.Visible = false; }

        ///// <summary>
        ///// WIP
        ///// </summary>
        //public void CloseUwamp()
        //{
        //    aUwamp.CloseUwamp();
        //}

        public void ShowPriceChart()
        {
            //update the view

            //Show
        }

       
        public List<string> GetPrinterNames()
        {
            DAL_Printer dal_printer = new DAL_Printer(); //Get the DAL to class prop
            return dal_printer.GetPrinterNames();
        }
        public List<string> GetBrands()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetBrands();
        }
        public List<string> GetBuilders()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetBuilders();
        }
        public List<string> GetProviders()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetProviders();
        }
        public List<string> GetScanResolutions()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetScanResolutions();
        }
        public List<string> GetPrintResolution()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetPrintResolution();
        }
        public List<string> GetInksResolution()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetInksResolution();
        }

    }
}
