﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using ProjetPrinter.Model;
using System.Windows.Forms;
using System.Text;

namespace ProjetPrinter
{
    public class DAL_Printer : IDAL
    {
        public string ServerName { get; set; }
        public string DBName { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }

        //variable de connection à la base mySQL
        public MySqlConnection mySqlConnection { get; set; }

        public DAL_Printer()
        {
            this.ServerName = "localhost";
            this.DBName = "db_printers";
            this.UserName = "root";
            this.UserPassword = "root";

            mySqlConnection = new MySqlConnection($"server={ServerName}; database={DBName}; uid={UserName}; pwd={UserPassword};");

        }

        string strWHEREAllDataPrint = @"t_printer.priName != 'Tank555'";

        string strSelectAllDataPrint = @"t_printer.idPrinter,
                                                t_printer.priLink, 
                                                t_printer.priName, 
                                                t_printer.priPrintResolution, 
                                                t_printer.priScanResolution, 
                                                t_printer.priPrintSpeed, 
                                                t_printer.priDouble_Sided,
                                                t_printer.priWireless, 
                                                t_printer.priWitdh, 
                                                t_printer.priLenght, 
                                                t_printer.priHeight, 
                                                t_printer.priWeight, 
                                                t_printer.priDateRelease,
                                                
                                                t_evoprice.Evoprice,

                                                t_technology.tecType, 
                                                
                                                t_provider.idProvider, 
                                                t_provider.proName, 
                                                t_provider.proLink, 

                                                t_builder.idBuilder, 
                                                t_builder.buiName, 
                                                t_builder.buiBrand";

        string strJoinAllDataPrint = "t_printer Natural Join t_builder Natural Join t_provider Natural Join t_technology Natural Join t_useink Natural Join t_ink Natural Join t_evoPrice";
        string groupByPrint = "Group By t_printer.priName";

        string orderByIdPrint = "ORDER BY t_printer.idPrinter";

        string strPaper = "t_printer Natural Join t_usepaper Natural Join t_paper ";
        string strSelectPaper = "t_paper.papName, t_paper.idPaper";
        string strInk = "t_printer Natural Join t_useink Natural Join t_ink ";
        string strSelectInk = "t_ink.idInk, t_ink.inkName, t_ink.inkType, t_ink.inkPrice";
        string idPrint = "3";

        string strSelectProvider = "t_provider.idProvider, t_provider.proName ,t_provider.proLink";
        string strFromProvider = "t_evoprice Natural Join t_printer";

        string strSelectBuilder = "t_builder.idBuilder, t_builder.buiName ,t_builder.buibrand";
        string strFromBuilder = "t_evoprice Natural Join t_printer";

        string strSelectLastPrice = "t_evoprice.EvoPrice";
        string strFromLastPrice = "t_evoprice";

        string strSelectEvoPrice = "t_evoprice.EvoPrice, t_evoprice.EvoDate";
        string strFromEvoPrice = "t_evoprice Natural Join t_printer";

        public List<PrinterModel> Default()
        {
            List<PrinterModel> ReturnedObjList = new List<PrinterModel>();


            //se connecte à la base de donnée
            //mySqlConnectionPrice.Open();



            //se connecte à la base de donnée
            //mySqlConnectionInk.Open();

            //Parameters of the Query
            
            //Build up the Query
            //MySqlCommand select = new MySqlCommand("SELECT " + strSelectAllDataPrint + " FROM " + strJoinAllDataPrint + " WHERE " + strWHEREAllDataPrint + groupByPrint, mySqlConnection);
            //MySqlCommand selectPrinter = new MySqlCommand("SELECT * FROM t_printer", mySqlConnectionPrinter);
            //déclarations des commandes
            //MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " WHERE " + strWHEREAllDataPrint", mySqlConnectionPaper);
            //déclarations des commandes
            //MySqlCommand selectInk = new MySqlCommand("SELECT * FROM t_ink", mySqlConnectionInk);
            ////déclarations des commandes
            //MySqlCommand selectPrice = new MySqlCommand("SELECT * FROM t_evoprice", mySqlConnectionPrice);


            //se connecte à la base de donnée
            using (mySqlConnection)
            {
                mySqlConnection.Open();

                MySqlCommand selectPrinter = new MySqlCommand($"SELECT {strSelectAllDataPrint} FROM t_printer {strJoinAllDataPrint} {groupByPrint} {orderByIdPrint}", mySqlConnection);
                //QueryDB
                MySqlDataReader printerReader = selectPrinter.ExecuteReader();
                //Read and Store the returned table
                while (printerReader.Read())
                {
                    PrinterModel pm = new PrinterModel();
                    ReturnedObjList.Add(pm);


                    pm.Id = Convert.ToInt32(printerReader["idPrinter"]);
                    pm.Name = printerReader["priName"].ToString();

                    pm.Size.Width = Convert.ToInt32(printerReader["priWitdh"]);
                    pm.Size.Length = Convert.ToInt32(printerReader["priLenght"]);
                    pm.Size.Heigth = Convert.ToInt32(printerReader["priHeight"]);

                    pm.Weight = Convert.ToInt32(printerReader["priWeight"]);
                    pm.RectoVerso = printerReader["priDouble_Sided"].ToString();
                    pm.ArticleLink = printerReader["priLink"].ToString();
                    pm.ReleaseDate = Convert.ToDateTime(printerReader["priDateRelease"]);
                    pm.PrintResolution = printerReader["priPrintResolution"].ToString();
                    pm.ScanResolution = printerReader["priScanResolution"].ToString();
                    pm.Wireless = printerReader["priWireless"].ToString();

                    pm.Provider.Id = Convert.ToInt32(printerReader["idProvider"].ToString());
                    pm.Provider.Name = printerReader["proName"].ToString();
                    pm.Provider.Link = printerReader["proLink"].ToString();

                    pm.Builder.Id = Convert.ToInt32(printerReader["idBuilder"].ToString());
                    pm.Builder.Name = printerReader["buiName"].ToString();
                    pm.Builder.Brand = printerReader["buiBrand"].ToString();

                }
                printerReader.Close();
                mySqlConnection.Close();
            }

           
            // GET Last Prices //

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectLastPrices = new MySqlCommand("SELECT " + strSelectLastPrice + " FROM " + strFromLastPrice + " WHERE " + "t_evoprice.idPrinter=" + pm.Id + " AND t_evoprice.evodate =(select max(t_evoprice.EvoDate) from t_evoprice where + t_evoprice.idPrinter = " + pm.Id + ")", mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectLastPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        pm.LastPrice = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET ALL PRICES//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPrices = new MySqlCommand("SELECT " + strSelectEvoPrice + " FROM " + strFromEvoPrice + " WHERE " + "t_printer.idPrinter=" + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        PriceInTime pic = new PriceInTime();
                        pm.PriceEvolution.Add(pic);

                        pic.Date = Convert.ToDateTime(reader["evodate"]);
                        pic.Price = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET COMPATIBLE PAPERS//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPaper.ExecuteReader();

                    while (reader.Read())
                    {
                        PapersCompatible pc = new PapersCompatible();
                        pm.Papers.Add(pc);

                        pc.Id = Convert.ToInt32(reader["idPaper"]);
                        pc.Name = reader["papName"].ToString();
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }

            //GET COMPATIBLE INKS

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible inks
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectInk = new MySqlCommand("SELECT " + strSelectInk + " FROM " + strInk + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Db connect
                    MySqlDataReader reader = selectInk.ExecuteReader();

                    while (reader.Read())
                    {
                        InkCompatible ic = new InkCompatible();
                        pm.Inks.Add(ic);

                        ic.Id = Convert.ToInt32(reader["idInk"]);
                        ic.Name = reader["inkName"].ToString();
                        ic.Type = reader["inkType"].ToString();
                        ic.Price = Convert.ToInt32(reader["inkPrice"]);

                    }

                    reader.Close();
                }

                mySqlConnection.Close();

            }

            return ReturnedObjList;
        }

        public List<PrinterModel> Custom(PrinterModel pmReceived)
        {
            List<PrinterModel> ReturnedObjList = new List<PrinterModel>();

            //se connecte à la base de donnée
            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //DINAMIC QUERY
                MySqlCommand cmdBASE = new MySqlCommand();
                cmdBASE.CommandText =   $"SELECT {strSelectAllDataPrint}, max(t_evoprice.EvoDate) as \"lastDate\" " +
                                        $"FROM  t_printer {strJoinAllDataPrint} {groupByPrint} " +
                                        $"WHERE ";
                cmdBASE.Connection = mySqlConnection;
                    
                //DINAMIC QUERY PARAMETERS

                //Matches
                StringBuilder cmdParam = new StringBuilder();
                if(pmReceived.Name                              != null){ cmdParam.Append("@Name ,");               cmdBASE.Parameters.AddWithValue("@Name",              "t_printer.priName =="              +  pmReceived.Name  + " AND ");  };
                if(pmReceived.Builder.Brand                     != null){ cmdParam.Append("@Brand ,");              cmdBASE.Parameters.AddWithValue("@Brand",             "t_builder.buiBrand =="             +  pmReceived.Builder.Brand + " AND ");  };
                if(pmReceived.Builder.Name                      != null){ cmdParam.Append("@Builder ,");            cmdBASE.Parameters.AddWithValue("@Builder",           "t_builder.buiName =="               + pmReceived.Builder.Name  + " AND ");  };
                if(pmReceived.Provider.Name                     != null){ cmdParam.Append("@Provider ,");           cmdBASE.Parameters.AddWithValue("@Provider",          "t_provider.proName == "             + pmReceived.Provider.Name  + " AND ");  };
                if(pmReceived.ScanResolution                    != null){ cmdParam.Append("@ScanResolution ,");     cmdBASE.Parameters.AddWithValue("@ScanResolution",    "t_printer.priScanResolution == "    + pmReceived.ScanResolution  + " AND ");  };
                if(pmReceived.PrintResolution                   != null){ cmdParam.Append("@PrintResolution ,");    cmdBASE.Parameters.AddWithValue("@PrintResolution",   "t_printer.priPrintResolution == "   + pmReceived.PrintResolution  + " AND ");  };
                if(pmReceived.Inks.Count                        != 0)   { cmdParam.Append("@InkCompatible ,");      cmdBASE.Parameters.AddWithValue("@InkCompatible",     "t_Ink.inkName == " + pmReceived.Inks[0]); };
                
                //SELECT t_printer.priName, t_evoprice.EvoPrice, max(t_evoprice.EvoDate) FROM t_printer NATURAL JOIN t_evoprice Group by t_evoprice.idPrinter ORDER BY `t_evoprice`.`EvoPrice` ASC 

                //GROUP BY
                cmdParam.Append("@DefaultGroup ,");
                cmdBASE.Parameters.AddWithValue("@DefaultGroup", "group by t_printer.idPrinter");

                //Order By
                if (pmReceived.WeightSort != null || pmReceived.Size.WidthSort != null || pmReceived.Size.HeigthSort != null || pmReceived.Size.LengthSort != null || pmReceived.PriceSort != null)
                {
                    cmdParam.Append("@Order");

                    cmdBASE.Parameters.AddWithValue("@Order", "Order by");

                    //1-PRICE
                    if (Convert.ToString(pmReceived.PriceSort)             != null)
                    {
                        cmdParam.Append("@PriceSort ,");
                        if (Convert.ToString(pmReceived.PriceSort) == "Croissant")
                            cmdBASE.Parameters.AddWithValue("@PriceSort", " t_evoprice.EvoPrice DESC");
                        if (Convert.ToString(pmReceived.PriceSort) == "Decroissant")
                            cmdBASE.Parameters.AddWithValue("@PriceSort", " t_evoprice.EvoPrice ASC");

                    }

                    //PrintSpeed
                    if (Convert.ToString(pmReceived.PrintSpeedSort) != null)
                    {
                        cmdParam.Append("@PrintSpeedSort ,");

                        if (Convert.ToString(pmReceived.PrintSpeedSort) == "Croissant")
                            cmdBASE.Parameters.AddWithValue("@PrintSpeedSort", ", t_evoprice.priPrintSpeed DESC");
                        if (Convert.ToString(pmReceived.PrintSpeedSort) == "Decroissant")
                            cmdBASE.Parameters.AddWithValue("@PrintSpeedSort", ", t_evoprice.priPrintSpeed ASC");

                    }

                    //WEIGHT
                    if (Convert.ToString(pmReceived.WeightSort)         != null)
                    {
                        cmdParam.Append("@WeightSort ,");
                        if (Convert.ToString(pmReceived.WeightSort) == "Croissant")
                            cmdBASE.Parameters.AddWithValue("@WeightSort", ", t_printer.priWeight DESC");
                        if (Convert.ToString(pmReceived.WeightSort) == "Decroissant")
                            cmdBASE.Parameters.AddWithValue("@WeightSort", ", t_printer.priWeight ASC");
                    }

                    //DIMENSIONS -SIZES
                    if (Convert.ToString(pmReceived.Size.WidthSort)     != null)
                    {
                        cmdParam.Append("@WidthtSort ,");

                        if (Convert.ToString(pmReceived.Size.WidthSort) == "Croissant")
                            cmdBASE.Parameters.AddWithValue("@WidthtSort", ", t_printer.priWitdh DESC");
                        if (Convert.ToString(pmReceived.Size.WidthSort) == "Decroissant")
                            cmdBASE.Parameters.AddWithValue("@WidthtSort", ", t_printer.priWitdh ASC");

                    }

                    if (Convert.ToString(pmReceived.Size.HeigthSort) != null)
                    {
                        cmdParam.Append("@HeigthtSort ,");

                        if (Convert.ToString(pmReceived.Size.HeigthSort) == "Croissant")
                            cmdBASE.Parameters.AddWithValue("@HeigthtSort", ", t_printer.priHeight DESC");
                        if (Convert.ToString(pmReceived.Size.HeigthSort) == "Decroissant")
                            cmdBASE.Parameters.AddWithValue("@HeigthtSort", ", t_printer.priHeight ASC");

                    }

                    if (Convert.ToString(pmReceived.Size.LengthSort) != null)
                    {
                        cmdParam.Append("@LengthtSort ");

                        if (Convert.ToString(pmReceived.Size.LengthSort) == "Croissant")
                            cmdBASE.Parameters.AddWithValue("@LengthtSort", ", t_printer.priLength DESC");
                        if (Convert.ToString(pmReceived.Size.LengthSort) == "Decroissant")
                            cmdBASE.Parameters.AddWithValue("@LengthtSort", ", t_printer.priLength ASC");

                    }
                    

                    
                }
                if (pmReceived.WeightSort == null || pmReceived.Size.WidthSort == null || pmReceived.Size.HeigthSort == null || pmReceived.Size.LengthSort == null || pmReceived.PriceSort == null)
                {
                    cmdParam.Append("@DefaultSort");

                    cmdBASE.Parameters.AddWithValue("@DefaultSort", ", t_printer.idPrinter DESC");
                }

                cmdBASE.CommandText += cmdParam.ToString();
                //QueryDB
                MySqlDataReader printerReader = cmdBASE.ExecuteReader();


                //Read and Store the returned table
                while (printerReader.Read())
                {
                    PrinterModel pm = new PrinterModel();
                    ReturnedObjList.Add(pm);


                    pm.Id = Convert.ToInt32(printerReader["idPrinter"]);
                    pm.Name = printerReader["priName"].ToString();

                    pm.Size.Width = Convert.ToInt32(printerReader["priWitdh"]);
                    pm.Size.Length = Convert.ToInt32(printerReader["priLenght"]);
                    pm.Size.Heigth = Convert.ToInt32(printerReader["priHeight"]);

                    pm.Weight = Convert.ToInt32(printerReader["priWeight"]);
                    pm.RectoVerso = printerReader["priDouble_Sided"].ToString();
                    pm.ArticleLink = printerReader["priLink"].ToString();
                    pm.ReleaseDate = Convert.ToDateTime(printerReader["priDateRelease"]);
                    pm.PrintResolution = printerReader["priPrintResolution"].ToString();
                    pm.ScanResolution = printerReader["priScanResolution"].ToString();
                    pm.Wireless = printerReader["priWireless"].ToString();

                    pm.Provider.Id = Convert.ToInt32(printerReader["idProvider"].ToString());
                    pm.Provider.Name = printerReader["proName"].ToString();
                    pm.Provider.Link = printerReader["proLink"].ToString();

                    pm.Builder.Id = Convert.ToInt32(printerReader["idBuilder"].ToString());
                    pm.Builder.Name = printerReader["buiName"].ToString();
                    pm.Builder.Brand = printerReader["buiBrand"].ToString();

                    pm.LastPrice = Convert.ToInt32(printerReader["EvoPrice"].ToString());

                }

                printerReader.Close();
                mySqlConnection.Close();
            }


            //// GET Last Prices //

            //using (mySqlConnection)
            //{
            //    mySqlConnection.Open();

            //    //For each printer gets the compatible papers
            //    foreach (PrinterModel pm in ReturnedObjList)
            //    {
            //        MySqlCommand selectLastPrices = new MySqlCommand("SELECT " + strSelectLastPrice + " FROM " + strFromLastPrice + " WHERE " + "t_evoprice.idPrinter=" + pm.Id + " AND t_evoprice.evodate =(select max(t_evoprice.EvoDate) from t_evoprice where + t_evoprice.idPrinter = " + pm.Id + ")", mySqlConnection);
            //        //Execute query and read
            //        MySqlDataReader reader = selectLastPrices.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            pm.LastPrice = Convert.ToInt32(reader["evoprice"]);
            //        }

            //        reader.Close();
            //    }

            //    mySqlConnection.Close();
            //}


            //GET ALL PRICES//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPrices = new MySqlCommand("SELECT " + strSelectEvoPrice + " FROM " + strFromEvoPrice + " WHERE " + "t_printer.idPrinter=" + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        PriceInTime pic = new PriceInTime();
                        pm.PriceEvolution.Add(pic);

                        pic.Date = Convert.ToDateTime(reader["evodate"]);
                        pic.Price = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET COMPATIBLE PAPERS//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPaper.ExecuteReader();

                    while (reader.Read())
                    {
                        PapersCompatible pc = new PapersCompatible();
                        pm.Papers.Add(pc);

                        pc.Id = Convert.ToInt32(reader["idPaper"]);
                        pc.Name = reader["papName"].ToString();
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }

            //GET COMPATIBLE INKS

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible inks
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectInk = new MySqlCommand("SELECT " + strSelectInk + " FROM " + strInk + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Db connect
                    MySqlDataReader reader = selectInk.ExecuteReader();

                    while (reader.Read())
                    {
                        InkCompatible ic = new InkCompatible();
                        pm.Inks.Add(ic);

                        ic.Id = Convert.ToInt32(reader["idInk"]);
                        ic.Name = reader["inkName"].ToString();
                        ic.Type = reader["inkType"].ToString();
                        ic.Price = Convert.ToInt32(reader["inkPrice"]);

                    }

                    reader.Close();
                }

                mySqlConnection.Close();

            }

            return ReturnedObjList;
        }

        public List<string> GetPrinterNames()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_printer.priName FROM t_printer group by t_printer.priName order by t_printer.priName", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["priName"].ToString());
            }
            return toReturn;
        }

        public List<string> GetBrands()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_builder.buiBrand FROM t_builder group by t_builder.buiBrand order by t_builder.buiBrand", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["buiBrand"].ToString());
            }
            return toReturn;
        }

        public List<string> GetBuilders()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_builder.buiName FROM t_builder group by t_builder.buiName order by t_builder.buiName", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["buiName"].ToString());
            }
            return toReturn;
        }
        public List<string> GetProviders()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_provider.proName FROM t_provider group by t_provider.proName order by t_provider.proName", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["proName"].ToString());
            }
            return toReturn;
        }
        public List<string> GetScanResolutions()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_printer.priScanResolution FROM t_printer group by t_printer.priScanResolution order by t_printer.priScanResolution", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["priScanResolution"].ToString());
            }
            return toReturn;
        }
        public List<string> GetPrintResolution()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_printer.priPrintResolution FROM t_printer group by t_printer.priPrintResolution order by t_printer.priPrintResolution", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["priPrintResolution"].ToString());
            }
            return toReturn;
        }
        public List<string> GetInksResolution()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_ink.inkName FROM t_ink group by t_ink.inkName  order by t_ink.inkName ", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["inkName"].ToString());
            }
            return toReturn;
        }

        public List<string> GetPapers()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_ink.inkName FROM t_ink group by t_ink.inkName  order by t_ink.inkName ", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["inkName"].ToString());
            }
            return toReturn;
        }
    }
}
