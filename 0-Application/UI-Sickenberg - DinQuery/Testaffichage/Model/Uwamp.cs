﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProjetPrinter.Model
{
    class Uwamp
    {
        private Process myProcess;

        public Uwamp()
        {
            myProcess = new Process();

            myProcess = SetUWAMP();
            myProcess.Start();
            Thread.Sleep(2000);
        }

        public void CloseUwamp()
        {
            myProcess.Kill();
        }

        public Process SetUWAMP()
        {
            string uwAmpPath = ".\\UwAmp\\UwAmp.exe";
            myProcess.StartInfo.FileName = uwAmpPath;
            myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            return myProcess;
        }
    }
}
