﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testaffichage.Model
{
    public interface IPrintDinQuery
    {
        string? NameParam { get; set; }
        string? BrandParam { get; set; }
        string? BuilderParam { get; set; }
        string? ProviderParam { get; set; }
        string? ScanResolutionParam { get; set; }
        string? PrintResolutionParam { get; set; }
        string? InkParam { get; set; }
        string? PaperParam { get; set; }
        string? PriceSort { get; set; }
        string? PrintSpeedSort { get; set; }
        string? SizeSort { get; set; }
        string? WeightSort { get; set; }
        int? RankingLimit { get; set; }

    }
}
