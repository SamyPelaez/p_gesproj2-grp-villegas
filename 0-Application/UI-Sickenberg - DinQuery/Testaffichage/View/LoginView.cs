﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetPrinter.Controller;

namespace ProjetPrinter.View
{
    public partial class LoginView : Form, IView
    {
        public MainController ViewController { get; set; }


        public LoginView()
        {
            InitializeComponent();
        }

        private void Close(object sender, EventArgs e)
        {
            this.Close();
        }

        private void check(object sender, EventArgs e)
        {
            TextBox check = (TextBox)sender;
            if (check.Name == username.Name)
            {
                //model
                string patern = @"^[A-za-z]{2,20}?[0-9]{0,9}$";
                //view
                if (!Regex.IsMatch(username.Text, patern))
                {
                    //error
                    usernamebox.ForeColor = Color.Red;
                }
                else
                {
                    //check
                    usernamebox.ForeColor = Color.Lime;
                }
            }
            if (check.Name == password.Name)
            {
                //model
                string patern = @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$";
                //view
                if (!Regex.IsMatch(password.Text, patern))
                {
                    //error
                    passwordbox.ForeColor = Color.Red;

                }
                else
                {
                    //check
                    passwordbox.ForeColor = Color.Lime;
                }
            }
        }

        private void Open(object sender, EventArgs e)
        {
            if (this.BackColor == Color.Silver)
            {
                //light mode
                foreach (Label x in this.Controls.OfType<Label>())
                {
                    x.ForeColor = Color.Black;
                }
            }
            else
            {
                //dark mode
                foreach (Label x in this.Controls.OfType<Label>())
                {
                    x.ForeColor = Color.White;
                }
            }
        }
    }
}
