﻿namespace ProjetPrinter.View
{
    partial class MainView
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bar = new System.Windows.Forms.Panel();
            this.Light = new System.Windows.Forms.Button();
            this.signin = new System.Windows.Forms.Button();
            this.login = new System.Windows.Forms.Button();
            this.Min = new System.Windows.Forms.Button();
            this.full = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.paneltop = new System.Windows.Forms.Panel();
            this.PaperComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BrandComboBox = new System.Windows.Forms.ComboBox();
            this.PrintSpeedComboBox = new System.Windows.Forms.ComboBox();
            this.RankingComboBox = new System.Windows.Forms.ComboBox();
            this.InksComboBox = new System.Windows.Forms.ComboBox();
            this.PriceComboBox = new System.Windows.Forms.ComboBox();
            this.WeightComboBox = new System.Windows.Forms.ComboBox();
            this.SizeComboBox = new System.Windows.Forms.ComboBox();
            this.NameComboBox = new System.Windows.Forms.ComboBox();
            this.BuilderComboBox = new System.Windows.Forms.ComboBox();
            this.ProviderComboBox = new System.Windows.Forms.ComboBox();
            this.btnpannier = new System.Windows.Forms.Button();
            this.NumericResComboBox = new System.Windows.Forms.ComboBox();
            this.PrintResComboBox = new System.Windows.Forms.ComboBox();
            this.animationTimer = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Home = new System.Windows.Forms.Button();
            this.Impr = new System.Windows.Forms.Button();
            this.Classement = new System.Windows.Forms.Button();
            this.Consom = new System.Windows.Forms.Button();
            this.Autre = new System.Windows.Forms.Button();
            this.barbtn = new System.Windows.Forms.Panel();
            this.shop = new System.Windows.Forms.Panel();
            this.printerViewUserControl = new ProjetPrinter.View.PrinterView();
            this.homeView = new ProjetPrinter.View.HomeView();
            this.bar.SuspendLayout();
            this.paneltop.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bar
            // 
            this.bar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.bar.Controls.Add(this.Light);
            this.bar.Controls.Add(this.signin);
            this.bar.Controls.Add(this.login);
            this.bar.Controls.Add(this.Min);
            this.bar.Controls.Add(this.full);
            this.bar.Controls.Add(this.Close);
            this.bar.Cursor = System.Windows.Forms.Cursors.Default;
            this.bar.Location = new System.Drawing.Point(0, 0);
            this.bar.Margin = new System.Windows.Forms.Padding(0);
            this.bar.Name = "bar";
            this.bar.Size = new System.Drawing.Size(1300, 50);
            this.bar.TabIndex = 5;
            // 
            // Light
            // 
            this.Light.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Light.BackgroundImage = global::Testaffichage.Properties.Resources.Moon;
            this.Light.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Light.FlatAppearance.BorderSize = 0;
            this.Light.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Light.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Light.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Light.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Light.ForeColor = System.Drawing.Color.White;
            this.Light.Location = new System.Drawing.Point(1067, 5);
            this.Light.Name = "Light";
            this.Light.Size = new System.Drawing.Size(40, 40);
            this.Light.TabIndex = 5;
            this.Light.UseVisualStyleBackColor = false;
            this.Light.Click += new System.EventHandler(this.Light_Click);
            // 
            // signin
            // 
            this.signin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.signin.FlatAppearance.BorderSize = 0;
            this.signin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signin.ForeColor = System.Drawing.Color.White;
            this.signin.Location = new System.Drawing.Point(12, 14);
            this.signin.Name = "signin";
            this.signin.Size = new System.Drawing.Size(75, 25);
            this.signin.TabIndex = 4;
            this.signin.Text = "Sign in";
            this.signin.UseVisualStyleBackColor = false;
            this.signin.Click += new System.EventHandler(this.Signin_Click);
            // 
            // login
            // 
            this.login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.login.FlatAppearance.BorderSize = 0;
            this.login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.ForeColor = System.Drawing.Color.White;
            this.login.Location = new System.Drawing.Point(93, 14);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(75, 25);
            this.login.TabIndex = 3;
            this.login.Text = "Login";
            this.login.UseVisualStyleBackColor = false;
            this.login.Click += new System.EventHandler(this.login_Click);
            // 
            // Min
            // 
            this.Min.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Min.FlatAppearance.BorderSize = 0;
            this.Min.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.Min.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.Min.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Min.ForeColor = System.Drawing.Color.White;
            this.Min.Location = new System.Drawing.Point(1120, 0);
            this.Min.Margin = new System.Windows.Forms.Padding(0);
            this.Min.Name = "Min";
            this.Min.Size = new System.Drawing.Size(60, 50);
            this.Min.TabIndex = 2;
            this.Min.Text = "--";
            this.Min.UseVisualStyleBackColor = false;
            this.Min.Click += new System.EventHandler(this.Min_Click);
            // 
            // full
            // 
            this.full.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.full.Enabled = false;
            this.full.FlatAppearance.BorderSize = 0;
            this.full.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(0)))), ((int)(((byte)(158)))));
            this.full.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.full.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.full.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.full.ForeColor = System.Drawing.Color.White;
            this.full.Location = new System.Drawing.Point(1180, 0);
            this.full.Margin = new System.Windows.Forms.Padding(0);
            this.full.Name = "full";
            this.full.Size = new System.Drawing.Size(60, 50);
            this.full.TabIndex = 1;
            this.full.Text = "[ ]";
            this.full.UseVisualStyleBackColor = false;
            // 
            // Close
            // 
            this.Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.Close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.ForeColor = System.Drawing.Color.White;
            this.Close.Location = new System.Drawing.Point(1240, 0);
            this.Close.Margin = new System.Windows.Forms.Padding(0);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(60, 50);
            this.Close.TabIndex = 0;
            this.Close.Text = "X";
            this.Close.UseVisualStyleBackColor = false;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.paneltop.Controls.Add(this.PaperComboBox);
            this.paneltop.Controls.Add(this.label1);
            this.paneltop.Controls.Add(this.BrandComboBox);
            this.paneltop.Controls.Add(this.PrintSpeedComboBox);
            this.paneltop.Controls.Add(this.RankingComboBox);
            this.paneltop.Controls.Add(this.InksComboBox);
            this.paneltop.Controls.Add(this.PriceComboBox);
            this.paneltop.Controls.Add(this.WeightComboBox);
            this.paneltop.Controls.Add(this.SizeComboBox);
            this.paneltop.Controls.Add(this.NameComboBox);
            this.paneltop.Controls.Add(this.BuilderComboBox);
            this.paneltop.Controls.Add(this.ProviderComboBox);
            this.paneltop.Controls.Add(this.btnpannier);
            this.paneltop.Controls.Add(this.NumericResComboBox);
            this.paneltop.Controls.Add(this.PrintResComboBox);
            this.paneltop.Location = new System.Drawing.Point(0, 50);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(1300, 40);
            this.paneltop.TabIndex = 16;
            // 
            // PaperComboBox
            // 
            this.PaperComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.PaperComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.PaperComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PaperComboBox.ForeColor = System.Drawing.Color.White;
            this.PaperComboBox.FormattingEnabled = true;
            this.PaperComboBox.Location = new System.Drawing.Point(620, 7);
            this.PaperComboBox.Name = "PaperComboBox";
            this.PaperComboBox.Size = new System.Drawing.Size(66, 24);
            this.PaperComboBox.TabIndex = 16;
            this.PaperComboBox.Text = "Papiers";
            this.PaperComboBox.SelectedIndexChanged += new System.EventHandler(this.PaperComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(692, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 15;
            this.label1.Text = "Tri :";
            // 
            // BrandComboBox
            // 
            this.BrandComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.BrandComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BrandComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BrandComboBox.ForeColor = System.Drawing.Color.White;
            this.BrandComboBox.FormattingEnabled = true;
            this.BrandComboBox.Location = new System.Drawing.Point(93, 7);
            this.BrandComboBox.Name = "BrandComboBox";
            this.BrandComboBox.Size = new System.Drawing.Size(75, 24);
            this.BrandComboBox.TabIndex = 14;
            this.BrandComboBox.Text = "Marque";
            this.BrandComboBox.SelectedIndexChanged += new System.EventHandler(this.BrandComboBox_SelectedIndexChanged);
            // 
            // PrintSpeedComboBox
            // 
            this.PrintSpeedComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.PrintSpeedComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.PrintSpeedComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintSpeedComboBox.ForeColor = System.Drawing.Color.White;
            this.PrintSpeedComboBox.FormattingEnabled = true;
            this.PrintSpeedComboBox.Location = new System.Drawing.Point(943, 7);
            this.PrintSpeedComboBox.Name = "PrintSpeedComboBox";
            this.PrintSpeedComboBox.Size = new System.Drawing.Size(109, 24);
            this.PrintSpeedComboBox.TabIndex = 13;
            this.PrintSpeedComboBox.Text = "Vitesse d\'Impression";
            this.PrintSpeedComboBox.SelectedIndexChanged += new System.EventHandler(this.PrintSpeedComboBox_SelectedIndexChanged);
            // 
            // RankingComboBox
            // 
            this.RankingComboBox.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.RankingComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.RankingComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.RankingComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RankingComboBox.ForeColor = System.Drawing.Color.White;
            this.RankingComboBox.FormattingEnabled = true;
            this.RankingComboBox.Location = new System.Drawing.Point(1059, 7);
            this.RankingComboBox.Name = "RankingComboBox";
            this.RankingComboBox.Size = new System.Drawing.Size(76, 24);
            this.RankingComboBox.TabIndex = 12;
            this.RankingComboBox.Text = "Ranking";
            this.RankingComboBox.SelectedIndexChanged += new System.EventHandler(this.PaperComboBox_SelectedIndexChanged);
            // 
            // InksComboBox
            // 
            this.InksComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.InksComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.InksComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InksComboBox.ForeColor = System.Drawing.Color.White;
            this.InksComboBox.FormattingEnabled = true;
            this.InksComboBox.Location = new System.Drawing.Point(548, 7);
            this.InksComboBox.Name = "InksComboBox";
            this.InksComboBox.Size = new System.Drawing.Size(66, 24);
            this.InksComboBox.TabIndex = 11;
            this.InksComboBox.Text = "Encres";
            // 
            // PriceComboBox
            // 
            this.PriceComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.PriceComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.PriceComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PriceComboBox.ForeColor = System.Drawing.Color.White;
            this.PriceComboBox.FormattingEnabled = true;
            this.PriceComboBox.Location = new System.Drawing.Point(725, 7);
            this.PriceComboBox.Name = "PriceComboBox";
            this.PriceComboBox.Size = new System.Drawing.Size(49, 24);
            this.PriceComboBox.TabIndex = 10;
            this.PriceComboBox.Text = "Prix";
            this.PriceComboBox.SelectedIndexChanged += new System.EventHandler(this.PriceComboBox_SelectedIndexChanged);
            // 
            // WeightComboBox
            // 
            this.WeightComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.WeightComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.WeightComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WeightComboBox.ForeColor = System.Drawing.Color.White;
            this.WeightComboBox.FormattingEnabled = true;
            this.WeightComboBox.Location = new System.Drawing.Point(780, 7);
            this.WeightComboBox.Name = "WeightComboBox";
            this.WeightComboBox.Size = new System.Drawing.Size(56, 24);
            this.WeightComboBox.TabIndex = 2;
            this.WeightComboBox.Text = "Poid";
            this.WeightComboBox.SelectedIndexChanged += new System.EventHandler(this.WeightComboBox_SelectedIndexChanged);
            // 
            // SizeComboBox
            // 
            this.SizeComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.SizeComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.SizeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SizeComboBox.ForeColor = System.Drawing.Color.White;
            this.SizeComboBox.FormattingEnabled = true;
            this.SizeComboBox.Items.AddRange(new object[] {
            "All",
            "Top 10",
            "Top 5",
            "Top 3"});
            this.SizeComboBox.Location = new System.Drawing.Point(842, 7);
            this.SizeComboBox.Name = "SizeComboBox";
            this.SizeComboBox.Size = new System.Drawing.Size(95, 24);
            this.SizeComboBox.TabIndex = 3;
            this.SizeComboBox.Text = "Taille";
            this.SizeComboBox.SelectedIndexChanged += new System.EventHandler(this.SizeComboBox_SelectedIndexChanged);
            // 
            // NameComboBox
            // 
            this.NameComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.NameComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.NameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameComboBox.ForeColor = System.Drawing.Color.White;
            this.NameComboBox.FormattingEnabled = true;
            this.NameComboBox.Location = new System.Drawing.Point(13, 7);
            this.NameComboBox.Name = "NameComboBox";
            this.NameComboBox.Size = new System.Drawing.Size(74, 24);
            this.NameComboBox.TabIndex = 9;
            this.NameComboBox.Text = "Modele";
            this.NameComboBox.SelectedIndexChanged += new System.EventHandler(this.NameComboBox_SelectedIndexChanged);
            // 
            // BuilderComboBox
            // 
            this.BuilderComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.BuilderComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BuilderComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BuilderComboBox.ForeColor = System.Drawing.Color.White;
            this.BuilderComboBox.FormattingEnabled = true;
            this.BuilderComboBox.Location = new System.Drawing.Point(174, 7);
            this.BuilderComboBox.Name = "BuilderComboBox";
            this.BuilderComboBox.Size = new System.Drawing.Size(80, 24);
            this.BuilderComboBox.TabIndex = 8;
            this.BuilderComboBox.Text = "Constructeur";
            this.BuilderComboBox.SelectedIndexChanged += new System.EventHandler(this.BuilderComboBox_SelectedIndexChanged);
            // 
            // ProviderComboBox
            // 
            this.ProviderComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.ProviderComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ProviderComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProviderComboBox.ForeColor = System.Drawing.Color.White;
            this.ProviderComboBox.FormattingEnabled = true;
            this.ProviderComboBox.Location = new System.Drawing.Point(260, 7);
            this.ProviderComboBox.Name = "ProviderComboBox";
            this.ProviderComboBox.Size = new System.Drawing.Size(83, 24);
            this.ProviderComboBox.TabIndex = 7;
            this.ProviderComboBox.Text = "Fournisseur";
            this.ProviderComboBox.SelectedIndexChanged += new System.EventHandler(this.ProviderComboBox_SelectedIndexChanged);
            // 
            // btnpannier
            // 
            this.btnpannier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.btnpannier.FlatAppearance.BorderSize = 0;
            this.btnpannier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpannier.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpannier.ForeColor = System.Drawing.Color.White;
            this.btnpannier.Location = new System.Drawing.Point(1141, 3);
            this.btnpannier.Name = "btnpannier";
            this.btnpannier.Size = new System.Drawing.Size(149, 33);
            this.btnpannier.TabIndex = 6;
            this.btnpannier.Text = "Panier";
            this.btnpannier.UseVisualStyleBackColor = false;
            this.btnpannier.Click += new System.EventHandler(this.btnpannier_Click);
            // 
            // NumericResComboBox
            // 
            this.NumericResComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.NumericResComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.NumericResComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumericResComboBox.ForeColor = System.Drawing.Color.White;
            this.NumericResComboBox.FormattingEnabled = true;
            this.NumericResComboBox.Location = new System.Drawing.Point(349, 7);
            this.NumericResComboBox.Name = "NumericResComboBox";
            this.NumericResComboBox.Size = new System.Drawing.Size(88, 24);
            this.NumericResComboBox.TabIndex = 5;
            this.NumericResComboBox.Text = "Res. Numerisation";
            this.NumericResComboBox.SelectedIndexChanged += new System.EventHandler(this.NumericResComboBox_SelectedIndexChanged);
            // 
            // PrintResComboBox
            // 
            this.PrintResComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.PrintResComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.PrintResComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrintResComboBox.ForeColor = System.Drawing.Color.White;
            this.PrintResComboBox.FormattingEnabled = true;
            this.PrintResComboBox.Location = new System.Drawing.Point(443, 7);
            this.PrintResComboBox.Name = "PrintResComboBox";
            this.PrintResComboBox.Size = new System.Drawing.Size(99, 24);
            this.PrintResComboBox.TabIndex = 4;
            this.PrintResComboBox.Text = "Res. Impression";
            this.PrintResComboBox.SelectedIndexChanged += new System.EventHandler(this.PrintResComboBox_SelectedIndexChanged);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.Home);
            this.flowLayoutPanel1.Controls.Add(this.Impr);
            this.flowLayoutPanel1.Controls.Add(this.Classement);
            this.flowLayoutPanel1.Controls.Add(this.Consom);
            this.flowLayoutPanel1.Controls.Add(this.Autre);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 615);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1300, 200);
            this.flowLayoutPanel1.TabIndex = 18;
            // 
            // Home
            // 
            this.Home.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Home.FlatAppearance.BorderSize = 0;
            this.Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Home.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home.ForeColor = System.Drawing.Color.White;
            this.Home.Location = new System.Drawing.Point(0, 0);
            this.Home.Margin = new System.Windows.Forms.Padding(0);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(260, 200);
            this.Home.TabIndex = 15;
            this.Home.Text = "Home";
            this.Home.UseVisualStyleBackColor = false;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // Impr
            // 
            this.Impr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Impr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Impr.FlatAppearance.BorderSize = 0;
            this.Impr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Impr.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Impr.ForeColor = System.Drawing.Color.White;
            this.Impr.Location = new System.Drawing.Point(260, 0);
            this.Impr.Margin = new System.Windows.Forms.Padding(0);
            this.Impr.Name = "Impr";
            this.Impr.Size = new System.Drawing.Size(260, 200);
            this.Impr.TabIndex = 16;
            this.Impr.Text = "Imprimante";
            this.Impr.UseVisualStyleBackColor = false;
            this.Impr.Click += new System.EventHandler(this.Impr_Click);
            // 
            // Classement
            // 
            this.Classement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Classement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Classement.FlatAppearance.BorderSize = 0;
            this.Classement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Classement.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Classement.ForeColor = System.Drawing.Color.White;
            this.Classement.Location = new System.Drawing.Point(520, 0);
            this.Classement.Margin = new System.Windows.Forms.Padding(0);
            this.Classement.Name = "Classement";
            this.Classement.Size = new System.Drawing.Size(260, 200);
            this.Classement.TabIndex = 20;
            this.Classement.Text = "Classement";
            this.Classement.UseVisualStyleBackColor = false;
            this.Classement.Click += new System.EventHandler(this.Classement_Click);
            // 
            // Consom
            // 
            this.Consom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Consom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Consom.FlatAppearance.BorderSize = 0;
            this.Consom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Consom.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consom.ForeColor = System.Drawing.Color.White;
            this.Consom.Location = new System.Drawing.Point(780, 0);
            this.Consom.Margin = new System.Windows.Forms.Padding(0);
            this.Consom.Name = "Consom";
            this.Consom.Size = new System.Drawing.Size(260, 200);
            this.Consom.TabIndex = 17;
            this.Consom.Text = "Consommable";
            this.Consom.UseVisualStyleBackColor = false;
            this.Consom.Click += new System.EventHandler(this.Consom_Click);
            // 
            // Autre
            // 
            this.Autre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Autre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Autre.FlatAppearance.BorderSize = 0;
            this.Autre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Autre.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Autre.ForeColor = System.Drawing.Color.White;
            this.Autre.Location = new System.Drawing.Point(1040, 0);
            this.Autre.Margin = new System.Windows.Forms.Padding(0);
            this.Autre.Name = "Autre";
            this.Autre.Size = new System.Drawing.Size(260, 200);
            this.Autre.TabIndex = 18;
            this.Autre.Text = "Autre";
            this.Autre.UseVisualStyleBackColor = false;
            this.Autre.Click += new System.EventHandler(this.Autre_Click);
            // 
            // barbtn
            // 
            this.barbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.barbtn.ForeColor = System.Drawing.Color.Black;
            this.barbtn.Location = new System.Drawing.Point(0, 600);
            this.barbtn.Name = "barbtn";
            this.barbtn.Size = new System.Drawing.Size(260, 15);
            this.barbtn.TabIndex = 19;
            // 
            // shop
            // 
            this.shop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.shop.Location = new System.Drawing.Point(1300, 90);
            this.shop.Name = "shop";
            this.shop.Size = new System.Drawing.Size(300, 525);
            this.shop.TabIndex = 20;
            // 
            // printerViewUserControl
            // 
            this.printerViewUserControl.Location = new System.Drawing.Point(0, 90);
            this.printerViewUserControl.Name = "printerViewUserControl";
            this.printerViewUserControl.Size = new System.Drawing.Size(1300, 510);
            this.printerViewUserControl.TabIndex = 21;
            this.printerViewUserControl.ViewController = null;
            this.printerViewUserControl.Visible = false;
            this.printerViewUserControl.Click += new System.EventHandler(this.getfocus);
            // 
            // homeView
            // 
            this.homeView.Location = new System.Drawing.Point(0, 90);
            this.homeView.Name = "homeView";
            this.homeView.Size = new System.Drawing.Size(1300, 510);
            this.homeView.TabIndex = 21;
            this.homeView.ViewController = null;
            this.homeView.Visible = false;
            this.homeView.Click += new System.EventHandler(this.getfocus);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.ClientSize = new System.Drawing.Size(1300, 815);
            this.Controls.Add(this.printerViewUserControl);
            this.Controls.Add(this.barbtn);
            this.Controls.Add(this.shop);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.paneltop);
            this.Controls.Add(this.bar);
            this.Controls.Add(this.homeView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainView";
            this.bar.ResumeLayout(false);
            this.paneltop.ResumeLayout(false);
            this.paneltop.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bar;
        private System.Windows.Forms.Button Min;
        private System.Windows.Forms.Button full;
        private new System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button signin;
        private System.Windows.Forms.Button login;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.ComboBox NumericResComboBox;
        private System.Windows.Forms.ComboBox PrintResComboBox;
        private System.Windows.Forms.ComboBox SizeComboBox;
        private System.Windows.Forms.ComboBox WeightComboBox;
        private System.Windows.Forms.Timer animationTimer;
        private System.Windows.Forms.Button btnpannier;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button Home;
        private System.Windows.Forms.Button Impr;
        private System.Windows.Forms.Button Classement;
        private System.Windows.Forms.Button Autre;
        private System.Windows.Forms.Button Consom;
        private System.Windows.Forms.Panel barbtn;
        private System.Windows.Forms.Panel shop;
        private System.Windows.Forms.Button Light;
        private PrinterView printerViewUserControl;
        private HomeView homeView;
        private System.Windows.Forms.ComboBox NameComboBox;
        private System.Windows.Forms.ComboBox BuilderComboBox;
        private System.Windows.Forms.ComboBox ProviderComboBox;
        private System.Windows.Forms.ComboBox InksComboBox;
        private System.Windows.Forms.ComboBox PriceComboBox;
        private System.Windows.Forms.ComboBox RankingComboBox;
        private System.Windows.Forms.ComboBox PrintSpeedComboBox;
        private System.Windows.Forms.ComboBox BrandComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox PaperComboBox;
    }
}

