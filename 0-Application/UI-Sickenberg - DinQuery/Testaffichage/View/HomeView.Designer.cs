﻿namespace ProjetPrinter.View
{
    partial class HomeView
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.Test = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // Test
            // 
            this.Test.AutoScroll = true;
            this.Test.BackgroundImage = global::Testaffichage.Properties.Resources.Wallpaper;
            this.Test.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Test.Location = new System.Drawing.Point(0, 0);
            this.Test.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Test.Name = "Test";
            this.Test.Size = new System.Drawing.Size(1977, 785);
            this.Test.TabIndex = 1;
            // 
            // HomeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.Test);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "HomeView";
            this.Size = new System.Drawing.Size(1950, 785);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.FlowLayoutPanel Test;
    }
}
