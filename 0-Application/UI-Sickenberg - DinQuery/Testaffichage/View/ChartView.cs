﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetPrinter.View;
using ProjetPrinter.Controller;

namespace ProjetPrinter.View
{
    public partial class ChartView : Form, IView
    {
        public MainController ViewController { get; set; }

        public ChartView()
        {
            InitializeComponent();
        }

        private void closthis_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void close_this(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
