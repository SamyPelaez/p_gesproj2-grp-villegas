﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition.
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.


using ProjetPrinter.Controller;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ProjetPrinter.Model;

namespace ProjetPrinter.View
{
    /// <summary>
    /// Principal Controller of the app
    /// </summary>
    public partial class MainView : Form, IView
    {
        /// <summary>
        /// Properties
        /// </summary>
        public MainController ViewController { get; set; }
        
        private PrinterModel pm { get; set; }

        //public Home UChome { get; set; }
        //public List<PrinterModel> Printers {get; set;}


        /// <summary>
        /// Sys. configuration which gives click and drag feature of the upper form panel
        /// </summary>
        /// <param name="hWnd">ref to user32.dll documentation</param>
        /// <param name="Msg">ref to user32.dll documentation</param>
        /// <param name="wParam">ref to user32.dll documentation</param>
        /// <param name="LPAR">ref to user32.dll documentation</param>
        /// <returns></returns>
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int LPAR);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        const int WM_NCLBUTTONDOWN = 0xA1;
        const int HT_CAPTION = 0x2;

        /// <summary>
        /// Click and drag event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Move_window(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(this.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public MainView()
        {
            InitializeComponent();
            this.pm = new PrinterModel(); //Container for user input
            homeView.Visible = true;
            bar.MouseDown += new MouseEventHandler(Move_window);
        }

        public void LoadComboBoxCollections()
        {
            NameComboBox.DataSource         = ViewController.GetPrinterNames();
            BrandComboBox.DataSource        = ViewController.GetBrands();
            BuilderComboBox.DataSource      = ViewController.GetBuilders();
            ProviderComboBox.DataSource     = ViewController.GetProviders();
            NumericResComboBox.DataSource   = ViewController.GetScanResolutions();
            PrintResComboBox.DataSource     = ViewController.GetPrintResolution();
            InksComboBox.DataSource         = ViewController.GetInksResolution();

            PriceComboBox.Items.Add("Croissant");
            PriceComboBox.Items.Add("Decroissant");

            WeightComboBox.Items.Add("Croissant");
            WeightComboBox.Items.Add("Decroissant");

            PrintSpeedComboBox.Items.Add("Croissant");
            PrintSpeedComboBox.Items.Add("Decroissant");

            RankingComboBox.Items.Add("Croissant");
            RankingComboBox.Items.Add("Decroissant");

            PriceComboBox.Items.Add("Top 3");
            PriceComboBox.Items.Add("Top 5");
            PriceComboBox.Items.Add("Top 10");
        }

        private void Home_Click(object sender, EventArgs e)
        {
            homeView.Visible = true;
            printerViewUserControl.Visible = false;
            barbtn.Location = new System.Drawing.Point(0, 600);
        }

        private void Impr_Click(object sender, EventArgs e)
        {
            homeView.Visible = false;

            barbtn.Location = new System.Drawing.Point(260, 600);
            printerViewUserControl.GetDefaultPrinterModel(ViewController.GetDefaultPrinterModel());
            printerViewUserControl.Focus();
            printerViewUserControl.Visible = true;
        }

        private void Consom_Click(object sender, EventArgs e)
        {
            homeView.Visible = false;
            printerViewUserControl.Visible = false;
            barbtn.Location = new System.Drawing.Point(780, 600);
        }

        private void Autre_Click(object sender, EventArgs e)
        {
            homeView.Visible = false;
            printerViewUserControl.Visible = false;
            barbtn.Location = new System.Drawing.Point(1040, 600);
        }
        private void Classement_Click(object sender, EventArgs e)
        {
            homeView.Visible = false;
            printerViewUserControl.Visible = false;
            barbtn.Location = new System.Drawing.Point(520, 600);
        }
        private void Min_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        
        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void btnpannier_Click(object sender, EventArgs e)
        {
            BasketView panier = new BasketView();
            panier.StartPosition = FormStartPosition.Manual;
            panier.Location = new Point((this.Location.X + this.Width / 2) - (panier.Width / 2), (this.Location.Y + this.Height / 2) - (panier.Height / 2));
            panier.Show();
        }


        //public Chart chart = new Chart();
        public bool LightONOFF = false;
        private void Light_Click(object sender, EventArgs e)
        {
            //Dark mode
            if (LightONOFF)
            {
                //image du bouton 
                Light.BackgroundImage = Image.FromFile(@"../../Resources\Sun.png");
                
                //form 
                this.BackColor = Color.FromArgb(80, 80, 80);

                //Top bar 1
                paneltop.BackColor = Color.FromArgb(55,55,55);

                //Top bar 2
                bar.BackColor = Color.FromArgb(40,40,40);
                Close.BackColor = Color.FromArgb(40,40,40);
                full.BackColor = Color.FromArgb(40,40,40);
                Min.BackColor = Color.FromArgb(40,40,40);
                Close.ForeColor = Color.White;
                full.ForeColor = Color.White;
                Min.ForeColor = Color.White;
                Light.BackColor= Color.FromArgb(40, 40, 40);
                Light.FlatAppearance.MouseDownBackColor = Color.FromArgb(40, 40, 40);
                Light.FlatAppearance.MouseOverBackColor = Color.FromArgb(40, 40, 40);
                //User control
                printerViewUserControl.BackColor = Color.FromArgb(80, 80, 80);
                //UChome.graphique.BackColor = Color.FromArgb(80, 80, 80);
                //bouton autre
                Autre.BackColor = Color.FromArgb(40, 40, 40);
                Autre.ForeColor = Color.White;

                //bouton Consomable
                Consom.BackColor = Color.FromArgb(40, 40, 40);
                Consom.ForeColor = Color.White;

                //bouton classement
                Classement.BackColor = Color.FromArgb(40, 40, 40);
                Classement.ForeColor = Color.White;

                //bouton imprimente
                Impr.BackColor = Color.FromArgb(40, 40, 40);
                Impr.ForeColor = Color.White;

                //bouton home
                Home.BackColor = Color.FromArgb(40, 40, 40);
                Home.ForeColor = Color.White;

                //bool on on light
                LightONOFF = false;
            }
            //Light mode
            //#eeeeee • #dddddd • #cccccc • #bbbbbb • #aaaaaa
            else
            {
                Light.BackgroundImage = Image.FromFile(@"../../Resources\Moon.png");

                //form
                this.BackColor = Color.FromArgb(221,221,221);
                //Top bar 1
                paneltop.BackColor = Color.FromArgb(204,204,204);

                //Top bar 2
                bar.BackColor = Color.Silver;
                Close.BackColor = Color.Silver;
                full.BackColor = Color.Silver;
                Min.BackColor = Color.Silver;                
                Light.BackColor = Color.Silver;
                Light.FlatAppearance.MouseDownBackColor = Color.Silver;
                Light.FlatAppearance.MouseOverBackColor = Color.Silver;
                full.ForeColor = Color.Black;
                Min.ForeColor = Color.Black;
                Close.ForeColor = Color.Black;
                

                //User control
                printerViewUserControl.BackColor = Color.FromArgb(221, 221, 221);
                //bouton autre
                Autre.BackColor = Color.Silver;
                Autre.ForeColor = Color.Black;

                //bouton Consomable
                Consom.BackColor = Color.Silver;
                Consom.ForeColor = Color.Black;

                //bouton classement
                Classement.BackColor = Color.Silver;
                Classement.ForeColor = Color.Black;

                //bouton imprimente
                Impr.BackColor = Color.Silver;
                Impr.ForeColor = Color.Black;

                //bouton home
                Home.BackColor = Color.Silver;
                Home.ForeColor = Color.Black;

                //UChome.panel[0,0].BackColor = Color.Green;

                //bool on off light
                LightONOFF = true;
            }
            
        }

        private void getfocus(object sender, EventArgs e)
        {
            printerViewUserControl.Focus();
        }

        private void Signin_Click(object sender, EventArgs e)
        {
            SignView sign = new SignView();
            switch(LightONOFF)
            {
                case true:
                    sign.BackColor = Color.Silver;
                    break;
                case false:
                    sign.BackColor = Color.FromArgb(50, 50, 50);
                    break;
            }
            sign.StartPosition = FormStartPosition.Manual;
            sign.Location = new Point((this.Location.X + this.Width / 2) - (sign.Width / 2), (this.Location.Y + this.Height / 2) - (sign.Height / 2));
            sign.Show();
        }

        private void login_Click(object sender, EventArgs e)
        {
            LoginView log = new LoginView();
            switch (LightONOFF)
            {
                case true:
                    log.BackColor = Color.Silver;
                    break;
                case false:
                    log.BackColor = Color.FromArgb(50, 50, 50);
                    break;
            }
            log.StartPosition = FormStartPosition.Manual;
            log.Location = new Point((this.Location.X + this.Width / 2) - (log.Width / 2), (this.Location.Y + this.Height / 2) - (log.Height / 2));
            log.Show();
        }

        private void UpdatePrinterView()
        {
           // this.ViewController.UpdatePrinterView(this.pm);
        }

        private void NameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.Name = NameComboBox.Text;
            UpdatePrinterView();
        }

        private void BuilderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.Builder.Name = BuilderComboBox.Text;
            UpdatePrinterView();
        }

        private void ProviderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.Provider.Name = ProviderComboBox.Text;
            UpdatePrinterView();
        }

        private void NumericResComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.ScanResolution = NumericResComboBox.Text;
            UpdatePrinterView();
        }

        private void PrintResComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.PrintResolution = PrintResComboBox.Text;
            UpdatePrinterView();
        }

        private void TechTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //pm.Technology = tech.Text;
            UpdatePrinterView();
        }


        private void InksComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.Inks[0].Name = InksComboBox.Text;
            UpdatePrinterView();
        }

        private void BrandComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.Builder.Brand = BrandComboBox.Text;
            UpdatePrinterView();
        }

        private void PaperComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.Papers[0].Name = PaperComboBox.Text;
            UpdatePrinterView();
        }

        private void PriceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.PriceSort = PriceComboBox.Text;
            UpdatePrinterView();
        }

        private void PrintSpeedComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.PrintSpeedSort = PrintSpeedComboBox.Text;
            UpdatePrinterView();
        }

        private void WeightComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.WeightSort = InksComboBox.Text;
            UpdatePrinterView();
        }

        private void SizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            pm.Size.HeigthSort = InksComboBox.Text;
            pm.Size.WidthSort = InksComboBox.Text;
            pm.Size.LengthSort = InksComboBox.Text;
            UpdatePrinterView();
        }
        
    }
}
