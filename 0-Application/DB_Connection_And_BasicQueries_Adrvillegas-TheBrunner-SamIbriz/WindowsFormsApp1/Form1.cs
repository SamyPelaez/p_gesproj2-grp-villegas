﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using MySql.Data.MySqlClient;


/*
 Auteur: Raphaël Vögeli
 Date: 10.01.2018
 Description: 
 Ce programme se connecte dans une base de donnée locale il demande qu'un base de donnée "test" aie été crée au préalable
 Le programme créer un table ajoute des nouvelles information puis les met à joue et les affiche dans le label prévu à cet effet  
*/

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        //variable de connection à la base mySQL
        MySqlConnection mySqlConnection = new MySqlConnection(@"server=localhost; database=db_printers; uid=root; pwd=root;");
        //variable de connection à la base mySQL
        MySqlConnection mySqlConnectionPaper = new MySqlConnection(@"server=localhost; database=db_printers; uid=root; pwd=root;");
        //variable de connection à la base mySQL
        MySqlConnection mySqlConnectionPrice = new MySqlConnection(@"server=localhost; database=db_printers; uid=root; pwd=root;");
        //variable de connection à la base mySQL
        MySqlConnection mySqlConnectionInk = new MySqlConnection(@"server=localhost; database=db_printers; uid=root; pwd=root;");

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            string strWhereAllDataPrint = @"t_printer.priName != 'Tank555'";
            string strSelectAllDataPrint = @"t_printer.priLink, t_printer.priName, t_printer.priPrintResolution, t_printer.priScanResolution, t_printer.priPrintSpeed, t_printer.priDouble_Sided
                              , t_printer.priWireless, t_printer.priWitdh, t_printer.priLenght, t_printer.priHeight, t_printer.priWeight, t_printer.priDateRelease
                              , t_technology.tecType, t_provider.proName, t_builder.buiName, t_builder.buiBrand, t_ink.inkName, t_ink.inkType";
            string strJoinAllDataPrint = "t_printer Natural Join t_builder Natural Join t_provider Natural Join t_technology Natural Join t_useink " +
                             "Natural Join t_ink" ;
            string groupByPrint = " Group By t_printer.priName";
            string strPaper = "t_printer Natural Join t_usepaper Natural Join t_paper ";
            string strSelectPaper = "t_paper.papName";
            string strInk = "t_printer Natural Join t_useink Natural Join t_ink ";
            string strSelectInk = "t_ink.inkName, t_ink.inkType, t_ink.inkPrice, t_printer.priName";
            string idPrint = "3";
            string strLastPrice = "SELECT t_evoprice.EvoPrice from t_evoprice where t_evoprice.evodate =(select max(t_evoprice.EvoDate) from t_evoprice)";
            string strTestEvoPrice = "SELECT t_evoprice.EvoPrice, t_evoprice.EvoDate from t_evoprice Natural Join t_printer WHERE t_printer.IdPrinter = " + idPrint;
            //se connecte à la base de donnée
            mySqlConnection.Open();
            //se connecte à la base de donnée   
            mySqlConnectionPaper.Open();
            //se connecte à la base de donnée
            mySqlConnectionPrice.Open();
            //se connecte à la base de donnée
            mySqlConnectionInk.Open();
            //déclarations des commandes
            MySqlCommand select = new MySqlCommand("SELECT " + strSelectAllDataPrint + " FROM "+ strJoinAllDataPrint+" WHERE " + strWhereAllDataPrint+ groupByPrint,mySqlConnection);
            //déclarations des commandes
            MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " Where " + strWhereAllDataPrint, mySqlConnectionPaper);
            //déclarations des commandes
            MySqlCommand selectInk = new MySqlCommand("SELECT " + strSelectInk + " FROM " + strInk +groupByPrint, mySqlConnectionInk);
            //déclarations des commandes
            MySqlCommand selectPrice = new MySqlCommand(strTestEvoPrice, mySqlConnectionPrice);
            lblQuery.Text = "Contenu de la requête: ";
            List<string> listOfQueriedLines = new List<string>();
            //Met dans la variable reader le contenu de la requete select
            MySqlDataReader reader = select.ExecuteReader();
            //Met dans la variable reader le contenu de la requete select
            MySqlDataReader readerPaper = selectPaper.ExecuteReader();
            //Met dans la variable reader le contenu de la requete select
            MySqlDataReader readerPrice = selectPrice.ExecuteReader();
            //Met dans la variable reader le contenu de la requete select
            MySqlDataReader readerInk = selectInk.ExecuteReader();
            while (reader.Read())
            {
                for (int x = 0; x < reader.FieldCount; x++)
                {
                    //met à jours l'affichage
                     lblQuery.Text += "\r\n" + Convert.ToString(reader[x]);
                    listOfQueriedLines.Add(Convert.ToString(reader[x]));
                }
            }
            /*while (readerPaper.Read())
            {
                for (int x = 0; x < readerPaper.FieldCount; x++)
                {
                    //met à jours l'affichage
                    lblQuery.Text += Convert.ToString(readerPaper[x]);
                    listOfQueriedLines.Add(Convert.ToString(readerPaper[x]));
                }
                lblQuery.Text += "\r\n";

            }
           while (readerPrice.Read())
           {
               for (int x = 0; x < readerPrice.FieldCount; x++)
               {
                   //met à jours l'affichage
                   lblQuery.Text += Convert.ToString(readerPrice[x]);
                   listOfQueriedLines.Add(Convert.ToString(readerPrice[x]));
               }
               lblQuery.Text += "\r\n";
           }*/
            //while (readerInk.Read())
            //{
            //    for (int x = 0; x < readerInk.FieldCount; x++)
            //    {
            //        //met à jours l'affichage
            //        lblQuery.Text += Convert.ToString(readerInk[x] + " ");
            //        listOfQueriedLines.Add(Convert.ToString(readerInk[x]));
            //    }
            //    lblQuery.Text += "\r\n";
            //}
            //listOfQueriedLines.Add(Convert.ToString());
            mySqlConnection.Close();
        }
        /*class PrinterModel
        {
            DateTime priDateRelease;


        }*/

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
