﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition.
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.

using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetPrinter.Model
{
    /// <summary>
    /// Contains the printer data obj definition
    /// </summary>
    public class PrinterModel
    {
        /// <summary>
        /// Obj properties
        /// 
        /// Id is been kept because it can be usefull for executing targeted specific queries further informations about the obj being observed in the db
        /// </summary>
        public int Id { get;  set; }
        public string Name { get;  set; }
        public Builders Builder { get;  set; }
        public double LastPrice { get;  set; }                             //SELECT t_evoprice.EvoPrice from t_evoprice where t_evoprice.evodate =(select max(t_evoprice.EvoDate) from t_evoprice)
        public List<PriceInTime> PriceEvolution { get;  set; }          //SELECT t_evoprice.EvoPrice, t_evoprice.EvoDate from t_evoprice Natural Join t_printer WHERE t_printer.IdPrinter = " + idPrint
        public string PriceSort { get;  set; }
        public Sizes Size { get;  set; } 
        public int Weight { get;  set; } 
        public string WeightSort { get;  set; }
        public int PrintSpeed{ get;  set; }
        public string PrintSpeedSort{ get;  set; }
        public string Technology { get;  set; }
        public Providers Provider { get;  set; }
        public string RectoVerso { get;  set; }
        public string ArticleLink { get; set; }
        public DateTime ReleaseDate { get;  set; }
        public string PrintResolution { get;  set; }
        public string ScanResolution { get;  set; }
        public string Wireless { get;  set; }
        public List<PapersCompatible> Papers { get;  set; }
        public List<InkCompatible> Inks { get;  set; }
        public int? QueryLimit { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public PrinterModel()
        {
            Size = new Sizes();
            Papers = new List<PapersCompatible>();
            Inks = new List<InkCompatible>();
            Provider = new Providers();
            Builder = new Builders();
            PriceEvolution = new List<PriceInTime>();
        }


        /// <summary>
        /// When the Chart Button is Click this method is executed to query the price list by lone item
        /// </summary>
        public void GetPriceEvolution()
        {
            //Data holder
            PriceEvolution = new List<PriceInTime>();

            //WIP                                                                                                       //WIP
            throw new NotImplementedException();
        }
    }

    /// <summary>
    /// Contains providers data definition
    /// </summary>
    public class Providers
    {
        /// <summary>
        /// Properties
        /// </summary>
        public int Id{ get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
    }

    /// <summary>
    /// Contains builders data definition
    /// </summary>
    public class Builders
    {
        /// <summary>
        /// Properties
        /// </summary>
        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
    }

    /// <summary>
    /// Contains sizes data definition
    /// </summary>
    public class Sizes
    {
        /// <summary>
        /// Properties
        /// </summary>
        public int Width { get;  set; }
        public string WidthSort { get; set; }

        public int Length { get;  set; }
        public string LengthSort { get; set; }

        public int Heigth { get;  set; }
        public string HeigthSort { get; set; }


        public override string ToString()
        {
            StringBuilder strbuilder = new StringBuilder();
            strbuilder.Append(Convert.ToString(Width));
            strbuilder.Append("x");
            strbuilder.Append(Convert.ToString(Heigth));
            strbuilder.Append("x"); 
            strbuilder.Append(Convert.ToString(Length));
            return strbuilder.ToString();
        }
    }


    /// <summary>
    /// Contains price in time data definition
    /// </summary>
    public class PriceInTime
    {
        /// <summary>
        /// Properties
        /// </summary>
        public DateTime Date { get;  set; }
        public double Price { get;  set; }
    }

    /// <summary>
    /// Contains papers compatible with the printe data definition
    /// </summary>
    public class PapersCompatible
    {
        /// <summary>
        /// Properties
        /// </summary>
        public int Id { get;  set; }
        public string Name { get;  set; }
    }

    /// <summary>
    /// This class is to implement as a List and contains all the inks compatible with the printer
    /// </summary>
    public class InkCompatible 
    {
        /// <summary>
        /// Properties
        /// </summary>
        public int Id { get;  set; }
        public string Name { get;  set; }
        public string Type { get;  set; }
        public int  Price { get;  set; }
    }
}
