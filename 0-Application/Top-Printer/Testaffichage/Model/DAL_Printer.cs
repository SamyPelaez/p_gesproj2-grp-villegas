﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition...
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;..
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;...
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;...
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.

using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using ProjetPrinter.Model;
using System.Windows.Forms;
using System.Text;

namespace ProjetPrinter
{
    /// <summary>
    /// This is the data access layer for the printers and sub-products contained in the database.
    /// 
    /// It connects to the database
    /// </summary>
    public class DAL_Printer : IDAL
    {
        public string ServerName { get; set; }
        public string DBName { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }

        //variable de connection à la base mySQL
        public MySqlConnection mySqlConnection { get; set; }

        public DAL_Printer()
        {
            this.ServerName = "localhost";
            this.DBName = "db_printers";
            this.UserName = "root";
            this.UserPassword = "root";

            mySqlConnection = new MySqlConnection($"server={ServerName}; database={DBName}; uid={UserName}; pwd={UserPassword};");

        }

        string strWHEREAllDataPrint = @"t_printer.priName != 'Tank555'";

        string strSelectAllDataPrint = @"t_printer.idPrinter,
                                                t_printer.priLink, 
                                                t_printer.priName, 
                                                t_printer.priPrintResolution, 
                                                t_printer.priScanResolution, 
                                                t_printer.priPrintSpeed, 
                                                t_printer.priDouble_Sided,
                                                t_printer.priWireless, 
                                                t_printer.priWitdh, 
                                                t_printer.priLenght, 
                                                t_printer.priHeight, 
                                                t_printer.priWeight, 
                                                t_printer.priDateRelease,
                                                
                                                t_evoprice.Evoprice,

                                                t_technology.tecType, 

                                                t_paper.idPaper,
                                                t_paper.papName,

                                                t_provider.idProvider, 
                                                t_provider.proName, 
                                                t_provider.proLink, 

                                                t_builder.idBuilder, 
                                                t_builder.buiName, 
                                                t_builder.buiBrand";

        string strJoinAllDataPrint = "t_printer Natural Join t_builder Natural Join t_provider Natural Join t_technology Natural Join t_useink Natural Join t_ink Natural Join t_evoPrice Natural Join t_usepaper Natural Join t_paper";
        string groupByPrint = "Group By t_printer.priName";

        string orderByIdPrint = "ORDER BY t_printer.idPrinter";

        string strPaper = "t_printer Natural Join t_usepaper Natural Join t_paper ";
        string strSelectPaper = "t_paper.papName, t_paper.idPaper";
        string strInk = "t_printer Natural Join t_useink Natural Join t_ink ";
        string strSelectInk = "t_ink.idInk, t_ink.inkName, t_ink.inkType, t_ink.inkPrice";
        string idPrint = "3";

        string strSelectProvider = "t_provider.idProvider, t_provider.proName ,t_provider.proLink";
        string strFromProvider = "t_evoprice Natural Join t_printer";

        string strSelectBuilder = "t_builder.idBuilder, t_builder.buiName ,t_builder.buibrand";
        string strFromBuilder = "t_evoprice Natural Join t_printer";

        string strSelectLastPrice = "t_evoprice.EvoPrice";
        string strFromLastPrice = "t_evoprice";

        string strSelectEvoPrice = "t_evoprice.EvoPrice, t_evoprice.EvoDate";
        string strFromEvoPrice = "t_evoprice Natural Join t_printer";

        public List<PrinterModel> Default()
        {
            List<PrinterModel> ReturnedObjList = new List<PrinterModel>();

            //se connecte à la base de donnée
            using (mySqlConnection)
            {
                mySqlConnection.Open();

                MySqlCommand selectPrinter = new MySqlCommand($"SELECT {strSelectAllDataPrint} FROM t_printer {strJoinAllDataPrint} {groupByPrint} {orderByIdPrint}", mySqlConnection);
                //QueryDB
                MySqlDataReader printerReader = selectPrinter.ExecuteReader();
                //Read and Store the returned table
                while (printerReader.Read())
                {
                    PrinterModel pm = new PrinterModel();
                    ReturnedObjList.Add(pm);


                    pm.Id = Convert.ToInt32(printerReader["idPrinter"]);
                    pm.Name = printerReader["priName"].ToString();

                    pm.Size.Width = Convert.ToInt32(printerReader["priWitdh"]);
                    pm.Size.Length = Convert.ToInt32(printerReader["priLenght"]);
                    pm.Size.Heigth = Convert.ToInt32(printerReader["priHeight"]);

                    pm.Weight = Convert.ToInt32(printerReader["priWeight"]);
                    pm.RectoVerso = printerReader["priDouble_Sided"].ToString();
                    pm.ArticleLink = printerReader["priLink"].ToString();
                    pm.ReleaseDate = Convert.ToDateTime(printerReader["priDateRelease"]);
                    pm.PrintResolution = printerReader["priPrintResolution"].ToString();
                    pm.ScanResolution = printerReader["priScanResolution"].ToString();
                    pm.Wireless = printerReader["priWireless"].ToString();

                    pm.Provider.Id = Convert.ToInt32(printerReader["idProvider"].ToString());
                    pm.Provider.Name = printerReader["proName"].ToString();
                    pm.Provider.Link = printerReader["proLink"].ToString();

                    pm.Builder.Id = Convert.ToInt32(printerReader["idBuilder"].ToString());
                    pm.Builder.Name = printerReader["buiName"].ToString();
                    pm.Builder.Brand = printerReader["buiBrand"].ToString();

                }
                printerReader.Close();
                mySqlConnection.Close();
            }


            // GET Last Prices //

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectLastPrices = new MySqlCommand("SELECT " + strSelectLastPrice + " FROM " + strFromLastPrice + " WHERE " + "t_evoprice.idPrinter=" + pm.Id + " AND t_evoprice.evodate =(select max(t_evoprice.EvoDate) from t_evoprice where + t_evoprice.idPrinter = " + pm.Id + ")", mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectLastPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        pm.LastPrice = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET ALL PRICES//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPrices = new MySqlCommand("SELECT " + strSelectEvoPrice + " FROM " + strFromEvoPrice + " WHERE " + "t_printer.idPrinter=" + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        PriceInTime pic = new PriceInTime();
                        pm.PriceEvolution.Add(pic);

                        pic.Date = Convert.ToDateTime(reader["evodate"]);
                        pic.Price = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET COMPATIBLE PAPERS//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPaper.ExecuteReader();

                    while (reader.Read())
                    {
                        PapersCompatible pc = new PapersCompatible();
                        pm.Papers.Add(pc);

                        pc.Id = Convert.ToInt32(reader["idPaper"]);
                        pc.Name = reader["papName"].ToString();
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }

            //GET COMPATIBLE INKS

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible inks
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectInk = new MySqlCommand("SELECT " + strSelectInk + " FROM " + strInk + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Db connect
                    MySqlDataReader reader = selectInk.ExecuteReader();

                    while (reader.Read())
                    {
                        InkCompatible ic = new InkCompatible();
                        pm.Inks.Add(ic);

                        ic.Id = Convert.ToInt32(reader["idInk"]);
                        ic.Name = reader["inkName"].ToString();
                        ic.Type = reader["inkType"].ToString();
                        ic.Price = Convert.ToInt32(reader["inkPrice"]);

                    }

                    reader.Close();
                }

                mySqlConnection.Close();

            }

            return ReturnedObjList;
        }

        /// <summary>
        /// Execute a dinamic query respecting the match with the received obj of the user input request
        /// </summary>
        /// <param name="pmReceived"></param>
        /// <returns></returns>
        public List<PrinterModel> Custom(PrinterModel pmReceived)
        {
            List<PrinterModel> ReturnedObjList = new List<PrinterModel>();

            //se connecte à la base de donnée
            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //DINAMIC QUERY
                MySqlCommand cmdBASE = new MySqlCommand();



                //LIMIT
                string limit = "";
                int? i = pmReceived.QueryLimit; ;
                switch (i)
                {
                    case null:
                        limit = "LIMIT 30"; ;
                        break;
                    case 3:
                        limit = "LIMIT 3"; ;
                        break;
                    case 5:
                        limit = "LIMIT 5"; ;
                        break;
                    case 10:
                        limit = "LIMIT 10";
                        break;
                }

                cmdBASE.CommandText = $"SELECT {strSelectAllDataPrint} FROM t_printer {strJoinAllDataPrint} "
                //CONDITION
                                      + $"WHERE(`priName` = @Name or @Name IS NULL)"
                                      + $"AND(`buiBrand` = @Brand or @Brand IS NULL)"
                                      + $"AND(`buiName` = @Builder or @Builder IS NULL)"
                                      + $"AND(`proName` = @Provider or @Provider IS NULL)"
                                      + $"AND(`priScanResolution` = @ScanResolution or @ScanResolution IS NULL)"
                                      + $"AND(`priPrintResolution` = @PrintResolution or @PrintResolution IS NULL)"
                                      + $"AND(`inkName` = @Ink or @Ink IS NULL)"
                                      + $"AND(`papName` = @Paper or @Paper IS NULL)"
                                      //GROUP
                                      + $"GROUP BY `idPrinter`"
                                      //SORT
                                      + $"ORDER BY"
                                      + $"(CASE WHEN @PriceSort = \"Croissant\" THEN `evoPrice` END) ASC,"
                                      + $"(CASE WHEN @PriceSort = \"Decroissant\" THEN `evoPrice` END) DESC,"

                                      + $"(CASE WHEN @PrintSpeedSort = \"Croissant\" THEN `priPrintSpeed` END) ASC,"
                                      + $"(CASE WHEN @PrintSpeedSort = \"Decroissant\" THEN `priPrintSpeed` END) DESC,"

                                      + $"(CASE WHEN @SizeSort = \"Croissant\" THEN `priWitdh` END) ASC,"
                                      + $"(CASE WHEN @SizeSort = \"Croissant\" THEN `priLenght` END) ASC,"
                                      + $"(CASE WHEN @SizeSort = \"Croissant\" THEN `priHeight` END) ASC,"
                                      + $"(CASE WHEN @SizeSort = \"Decroissant\" THEN `priWitdh` END) DESC,"
                                      + $"(CASE WHEN @SizeSort = \"Decroissant\" THEN `priLenght` END) DESC,"
                                      + $"(CASE WHEN @SizeSort = \"Decroissant\" THEN `priHeight` END) DESC,"

                                      + $"(CASE WHEN @WeightSort = \"Croissant\" THEN `priPrintSpeed` END) ASC,"
                                      + $"(CASE WHEN @WeightSort = \"Decroissant\" THEN `priPrintSpeed` END) DESC"
                                      + $" {limit} ";

                //  //LIMIT
                //  //SET @a = (SELECT group_limit FROM groups WHERE groupid = 7471);

                //  //PREPARE STMT FROM 'SELECT * FROM user LIMIT ?';
                //  //EXECUTE STMT USING @a;
                //+ $"SET @limit = ((CASE WHEN @Limit = NULL THEN 30 END), (CASE WHEN @Limit = \"Top 3\" THEN  3 END), (CASE WHEN @Limit = \"Top 5\" THEN 5 END),(CASE WHEN @Limit = \"Top 10\" THEN LIMIT 10 END))"




                cmdBASE.Connection = mySqlConnection;

                //DINAMIC QUERY PARAMETERS

                //Matches

                cmdBASE.Parameters.AddWithValue("@Name", pmReceived.Name);
                cmdBASE.Parameters.AddWithValue("@Brand", pmReceived.Builder.Brand);
                cmdBASE.Parameters.AddWithValue("@Builder", pmReceived.Builder.Name);
                cmdBASE.Parameters.AddWithValue("@Provider", pmReceived.Provider.Name);
                cmdBASE.Parameters.AddWithValue("@ScanResolution", pmReceived.ScanResolution);
                cmdBASE.Parameters.AddWithValue("@PrintResolution", pmReceived.PrintResolution);
                cmdBASE.Parameters.AddWithValue("@Ink", pmReceived.Inks[0].Name);
                cmdBASE.Parameters.AddWithValue("@Paper", pmReceived.Papers[0].Name);


                //GROUP BY

                //cmdBASE.Parameters.AddWithValue("@DefaultGroup", "idPrinter");

                //SORT BY
                cmdBASE.Parameters.AddWithValue("@PriceSort", pmReceived.PriceSort);
                cmdBASE.Parameters.AddWithValue("@PrintSpeedSort", pmReceived.PrintSpeedSort);
                cmdBASE.Parameters.AddWithValue("@SizeSort", pmReceived.Size.HeigthSort);
                cmdBASE.Parameters.AddWithValue("@WeightSort", pmReceived.WeightSort);

                ////LIMIT
                //switch ()
                //{
                //    case null:
                //        cmdBASE.Parameters.AddWithValue("@limit", "LIMIT 30");
                //        break;
                //    case 3:
                //        cmdBASE.Parameters.AddWithValue("@limit", "LIMIT 3");
                //        break;
                //    case 5:
                //        cmdBASE.Parameters.AddWithValue("@limit", "LIMIT 5");
                //        break;
                //    case 10:
                //        cmdBASE.Parameters.AddWithValue("@limit", "LIMIT 10");
                //        break;
                //}




                //QueryDB
                MySqlDataReader printerReader = cmdBASE.ExecuteReader();


                //Read and Store the returned table
                while (printerReader.Read())
                {
                    PrinterModel pm = new PrinterModel();
                    ReturnedObjList.Add(pm);


                    pm.Id = Convert.ToInt32(printerReader["idPrinter"]);
                    pm.Name = printerReader["priName"].ToString();

                    pm.Size.Width = Convert.ToInt32(printerReader["priWitdh"]);
                    pm.Size.Length = Convert.ToInt32(printerReader["priLenght"]);
                    pm.Size.Heigth = Convert.ToInt32(printerReader["priHeight"]);

                    pm.Weight = Convert.ToInt32(printerReader["priWeight"]);
                    pm.RectoVerso = printerReader["priDouble_Sided"].ToString();
                    pm.ArticleLink = printerReader["priLink"].ToString();
                    pm.ReleaseDate = Convert.ToDateTime(printerReader["priDateRelease"]);
                    pm.PrintResolution = printerReader["priPrintResolution"].ToString();
                    pm.ScanResolution = printerReader["priScanResolution"].ToString();
                    pm.Wireless = printerReader["priWireless"].ToString();

                    pm.Provider.Id = Convert.ToInt32(printerReader["idProvider"].ToString());
                    pm.Provider.Name = printerReader["proName"].ToString();
                    pm.Provider.Link = printerReader["proLink"].ToString();

                    pm.Builder.Id = Convert.ToInt32(printerReader["idBuilder"].ToString());
                    pm.Builder.Name = printerReader["buiName"].ToString();
                    pm.Builder.Brand = printerReader["buiBrand"].ToString();

                    pm.LastPrice = Convert.ToDouble(printerReader["EvoPrice"]);

                }
                printerReader.Close();

                mySqlConnection.Close();
            }

            //GET ALL PRICES//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPrices = new MySqlCommand("SELECT " + strSelectEvoPrice + " FROM " + strFromEvoPrice + " WHERE " + "t_printer.idPrinter=" + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        PriceInTime pic = new PriceInTime();
                        pm.PriceEvolution.Add(pic);

                        pic.Date = Convert.ToDateTime(reader["evodate"]);
                        pic.Price = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET COMPATIBLE PAPERS//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPaper.ExecuteReader();

                    while (reader.Read())
                    {
                        PapersCompatible pc = new PapersCompatible();
                        pm.Papers.Add(pc);

                        pc.Id = Convert.ToInt32(reader["idPaper"]);
                        pc.Name = reader["papName"].ToString();
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }

            //GET COMPATIBLE INKS

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible inks
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectInk = new MySqlCommand("SELECT " + strSelectInk + " FROM " + strInk + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Db connect
                    MySqlDataReader reader = selectInk.ExecuteReader();

                    while (reader.Read())
                    {
                        InkCompatible ic = new InkCompatible();
                        pm.Inks.Add(ic);

                        ic.Id = Convert.ToInt32(reader["idInk"]);
                        ic.Name = reader["inkName"].ToString();
                        ic.Type = reader["inkType"].ToString();
                        ic.Price = Convert.ToInt32(reader["inkPrice"]);

                    }

                    reader.Close();
                }

                mySqlConnection.Close();

            }

            return ReturnedObjList;
        }

        public List<string> GetPrinterNames()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_printer.priName FROM t_printer group by t_printer.priName order by t_printer.priName", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["priName"].ToString());
            }
            return toReturn;
        }


        /// <summary>
        /// Required to load combobox items collection
        /// </summary>
        /// <returns>A string List</returns>
        public List<string> GetBrands()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_builder.buiBrand FROM t_builder group by t_builder.buiBrand order by t_builder.buiBrand", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["buiBrand"].ToString());
            }
            return toReturn;
        }

        /// <summary>
        /// Required to load combobox items collection
        /// </summary>
        /// <returns>A string List</returns>
        public List<string> GetBuilders()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_builder.buiName FROM t_builder group by t_builder.buiName order by t_builder.buiName", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["buiName"].ToString());
            }
            return toReturn;
        }

        /// <summary>
        /// Required to load combobox items collection
        /// </summary>
        /// <returns>A string List</returns>
        public List<string> GetProviders()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_provider.proName FROM t_provider group by t_provider.proName order by t_provider.proName", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["proName"].ToString());
            }
            return toReturn;
        }

        /// <summary>
        /// Required to load combobox items collection
        /// </summary>
        /// <returns>A string List</returns>
        public List<string> GetScanResolutions()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_printer.priScanResolution FROM t_printer group by t_printer.priScanResolution order by t_printer.priScanResolution", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["priScanResolution"].ToString());
            }
            return toReturn;
        }

        /// <summary>
        /// Required to load combobox items collection
        /// </summary>
        /// <returns>A string List</returns>
        public List<string> GetPrintResolution()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_printer.priPrintResolution FROM t_printer group by t_printer.priPrintResolution order by t_printer.priPrintResolution", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["priPrintResolution"].ToString());
            }
            return toReturn;
        }

        /// <summary>
        /// Required to load combobox items collection
        /// </summary>
        /// <returns>A string List</returns>
        public List<string> GetInksResolution()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_ink.inkName FROM t_ink group by t_ink.inkName  order by t_ink.inkName ", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["inkName"].ToString());
            }
            return toReturn;
        }


        /// <summary>
        /// Required to load combobox items collection
        /// </summary>
        /// <returns>A string List</returns>
        public List<string> GetPapers()
        {
            List<string> toReturn = new List<string>();
            mySqlConnection.Open();
            MySqlCommand select = new MySqlCommand("SELECT t_paper.papName FROM t_paper group by t_paper.papName order by t_paper.idPaper ", mySqlConnection);
            MySqlDataReader reader = select.ExecuteReader();
            while (reader.Read())
            {
                toReturn.Add(reader["papName"].ToString());
            }
            return toReturn;
        }
    }
}
