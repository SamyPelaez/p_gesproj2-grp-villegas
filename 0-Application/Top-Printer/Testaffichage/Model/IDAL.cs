﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition.
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.

using MySql.Data.MySqlClient;


namespace ProjetPrinter.Model
{
    /// <summary>
    /// SRV Connection Interface
    /// </summary>
    interface IDAL
    {
        /// <summary>
        /// Properties required to the Interface
        /// </summary>
        string ServerName { get; set; }
        string DBName { get; set; }
        string UserName { get; set; }
        string UserPassword { get; set; }

        /// <summary>
        /// Connection obj.
        /// </summary>
        MySqlConnection mySqlConnection { get; set; }

    }
}
