﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition...
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;..
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;...
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;...
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.

using ProjetPrinter.Model;
using ProjetPrinter.View;
using System.Collections.Generic;

namespace ProjetPrinter.Controller
{
    public class MainController
    {
        private List<IView> Views { get; set; }
        private MainView mainView { get; set; }
        //private List<IModel> Models { get; set; }
        private MainModel MainModel;
        //private Uwamp aUwamp;

        public MainController(List<IView> views, MainModel _aModel)
        {
            //aUwamp = new Uwamp();

            Views = views;

            MainModel = _aModel;

            /// <summary>
            /// Assign the pertinent controller to their respective views
            /// </summary>
            foreach (IView i in Views)
            {
                i.ViewController = this;
            }

            

            mainView = (MainView)this.Views[0];
            mainView.GetDefaultPrinterModel(this.GetDefaultPrinterModel());
            //aView.UChome = new Home();
        }

        /// <summary>
        /// [ACTION] DEFAULT PrinterModel-MainView Transaction
        /// </summary>
        /// <returns></returns>
        public List<PrinterModel> GetDefaultPrinterModel()
        {            
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.Default();
        }

        public MainView GetPrinterView()
        {
            return (MainView)this.Views[0];
        }

        public List<PrinterModel> UpdatePrinterView(PrinterModel pm)
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.Custom(pm);


        }
        public void HideDefaultPrinterView(){ MainView pv = (MainView)Views[1]; pv.Visible = false; }

        public void ShowPriceChart()
        {
            //update the view

            //Show
        }

       
        public List<string> GetPrinterNames()
        {
            DAL_Printer dal_printer = new DAL_Printer(); //Get the DAL to class prop
            return dal_printer.GetPrinterNames();
        }
        public List<string> GetBrands()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetBrands();
        }
        public List<string> GetBuilders()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetBuilders();
        }
        public List<string> GetProviders()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetProviders();
        }
        public List<string> GetScanResolutions()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetScanResolutions();
        }
        public List<string> GetPrintResolution()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetPrintResolution();
        }
        public List<string> GetInksResolution()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetInksResolution();
        }
        public List<string> GetPapers()
        {
            DAL_Printer dal_printer = new DAL_Printer();
            return dal_printer.GetPapers();
        }

    }
}
