﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using MySql.Data.MySqlClient;
using ProjetPrinter.Controller;
using ProjetPrinter.View;

using ProjetPrinter.Model;

namespace ProjetPrinter
{
    public partial class Projet : Form
    {
        public MainController AController { get; set; }
        public Home UChome { get; set; }

        public List<PrinterModel> Printers {get; set;}

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int LPAR);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        const int WM_NCLBUTTONDOWN = 0xA1;
        const int HT_CAPTION = 0x2;

        private void Move_window(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(this.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        public Projet()
        {
            InitializeComponent();
            bar.MouseDown += new MouseEventHandler(Move_window);
        }

        public void GetDefaultModel(List<PrinterModel> printers)
        {
            Printers = printers;
        }
        
        private void Home_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            barbtn.Location = new System.Drawing.Point(0, 600);
        }

        private void Impr_Click(object sender, EventArgs e)
        {
            UChome = new Home();
            home1.Visible = true;
            home1.GetDefaultModel(Printers);
            home1.Focus();
            barbtn.Location = new System.Drawing.Point(260, 600);
        }

        private void Consom_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            barbtn.Location = new System.Drawing.Point(780, 600);
        }

        private void Autre_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            barbtn.Location = new System.Drawing.Point(1040, 600);
        }
        private void Classement_Click(object sender, EventArgs e)
        {
            home1.Visible = false;
            barbtn.Location = new System.Drawing.Point(520, 600);
        }
        private void Min_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        
        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void btnpannier_Click(object sender, EventArgs e)
        {
            Panier panier = new Panier();
            panier.StartPosition = FormStartPosition.Manual;
            panier.Location = new Point((this.Location.X + this.Width / 2) - (panier.Width / 2), (this.Location.Y + this.Height / 2) - (panier.Height / 2));
            panier.Show();
        }


        //public Chart chart = new Chart();
        public bool LightONOFF = false;
        private void Light_Click(object sender, EventArgs e)
        {
            //Dark mode
            if (LightONOFF)
            {
                //image du bouton 
                Light.BackgroundImage = Image.FromFile(@"../../Resources\Sun.png");
                
                //form 
                this.BackColor = Color.FromArgb(80, 80, 80);

                //Top bar 1
                paneltop.BackColor = Color.FromArgb(55,55,55);

                //Top bar 2
                bar.BackColor = Color.FromArgb(40,40,40);
                Close.BackColor = Color.FromArgb(40,40,40);
                full.BackColor = Color.FromArgb(40,40,40);
                Min.BackColor = Color.FromArgb(40,40,40);
                Close.ForeColor = Color.White;
                full.ForeColor = Color.White;
                Min.ForeColor = Color.White;
                Light.BackColor= Color.FromArgb(40, 40, 40);
                Light.FlatAppearance.MouseDownBackColor = Color.FromArgb(40, 40, 40);
                Light.FlatAppearance.MouseOverBackColor = Color.FromArgb(40, 40, 40);
                //User control
                home1.BackColor = Color.FromArgb(80, 80, 80);
                //UChome.graphique.BackColor = Color.FromArgb(80, 80, 80);
                //bouton autre
                Autre.BackColor = Color.FromArgb(40, 40, 40);
                Autre.ForeColor = Color.White;

                //bouton Consomable
                Consom.BackColor = Color.FromArgb(40, 40, 40);
                Consom.ForeColor = Color.White;

                //bouton classement
                Classement.BackColor = Color.FromArgb(40, 40, 40);
                Classement.ForeColor = Color.White;

                //bouton imprimente
                Impr.BackColor = Color.FromArgb(40, 40, 40);
                Impr.ForeColor = Color.White;

                //bouton home
                Home.BackColor = Color.FromArgb(40, 40, 40);
                Home.ForeColor = Color.White;

                //bool on on light
                LightONOFF = false;
            }
            //Light mode
            //#eeeeee • #dddddd • #cccccc • #bbbbbb • #aaaaaa
            else
            {
                Light.BackgroundImage = Image.FromFile(@"../../Resources\Moon.png");

                //form
                this.BackColor = Color.FromArgb(221,221,221);
                //Top bar 1
                paneltop.BackColor = Color.FromArgb(204,204,204);

                //Top bar 2
                bar.BackColor = Color.Silver;
                Close.BackColor = Color.Silver;
                full.BackColor = Color.Silver;
                Min.BackColor = Color.Silver;                
                Light.BackColor = Color.Silver;
                Light.FlatAppearance.MouseDownBackColor = Color.Silver;
                Light.FlatAppearance.MouseOverBackColor = Color.Silver;
                full.ForeColor = Color.Black;
                Min.ForeColor = Color.Black;
                Close.ForeColor = Color.Black;
                

                //User control
                home1.BackColor = Color.FromArgb(221, 221, 221);
                //bouton autre
                Autre.BackColor = Color.Silver;
                Autre.ForeColor = Color.Black;

                //bouton Consomable
                Consom.BackColor = Color.Silver;
                Consom.ForeColor = Color.Black;

                //bouton classement
                Classement.BackColor = Color.Silver;
                Classement.ForeColor = Color.Black;

                //bouton imprimente
                Impr.BackColor = Color.Silver;
                Impr.ForeColor = Color.Black;

                //bouton home
                Home.BackColor = Color.Silver;
                Home.ForeColor = Color.Black;

                //UChome.panel[0,0].BackColor = Color.Green;

                //bool on off light
                LightONOFF = true;
            }
            
        }

        private void getfocus(object sender, EventArgs e)
        {
            home1.Focus();
        }

        private void signin_Click(object sender, EventArgs e)
        {
            Sign sign = new Sign();
            switch(LightONOFF)
            {
                case true:
                    sign.BackColor = Color.Silver;
                    break;
                case false:
                    sign.BackColor = Color.FromArgb(50, 50, 50);
                    break;
            }
            sign.StartPosition = FormStartPosition.Manual;
            sign.Location = new Point((this.Location.X + this.Width / 2) - (sign.Width / 2), (this.Location.Y + this.Height / 2) - (sign.Height / 2));
            sign.Show();
        }

        private void login_Click(object sender, EventArgs e)
        {
            Login log = new Login();
            switch (LightONOFF)
            {
                case true:
                    log.BackColor = Color.Silver;
                    break;
                case false:
                    log.BackColor = Color.FromArgb(50, 50, 50);
                    break;
            }
            log.StartPosition = FormStartPosition.Manual;
            log.Location = new Point((this.Location.X + this.Width / 2) - (log.Width / 2), (this.Location.Y + this.Height / 2) - (log.Height / 2));
            log.Show();
        }
    }
}
