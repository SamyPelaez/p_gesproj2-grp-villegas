﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition...
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;..
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;...
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;...
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.


using ProjetPrinter.Controller;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ProjetPrinter.Model;
using System.Collections.Generic;
using System.Text;

namespace ProjetPrinter.View
{
    /// <summary>
    /// Principal Controller of the app
    /// </summary>
    public partial class MainView : Form, IView
    {
        /// <summary>
        /// Properties
        /// </summary>
        public MainController ViewController { get; set; }
         private List<PrinterModel> Printers { get; set; }
        //public Home UChome { get; set; }
        //public List<PrinterModel> Printers {get; set;}


        /// <summary>
        /// Sys. configuration which gives click and drag feature of the upper form panel
        /// </summary>
        /// <param name="hWnd">ref to user32.dll documentation</param>
        /// <param name="Msg">ref to user32.dll documentation</param>
        /// <param name="wParam">ref to user32.dll documentation</param>
        /// <param name="LPAR">ref to user32.dll documentation</param>
        /// <returns></returns>
        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int LPAR);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        const int WM_NCLBUTTONDOWN = 0xA1;
        const int HT_CAPTION = 0x2;

        /// <summary>
        /// Click and drag event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Move_window(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(this.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
       
        /// <summary>
        /// Constructor
        /// </summary>
        public MainView()
        {
            InitializeComponent();
            initPrinterDinQueryContainer();

            homeView.Visible = true;
            bar.MouseDown += new MouseEventHandler(Move_window);
        }

        ///// <summary>
        ///// [GET REQUEST] : Model-View transaction request
        ///// </summary>
        ///// <param name="printers"></param>
        public void GetDefaultPrinterModel(List<PrinterModel> printers)
        {
            Printers = printers;
            Start();
        }

        ///// <summary>
        ///// [GET REQUEST] : Model-View transaction request
        ///// </summary>
        ///// <param name="printers"></param>
        public void GetCustomPrinterModel(List<PrinterModel> printers)
        {
            this.Printers = printers;
        }

        public void ReseLayout()
        {
            if (printerFLowLayoutPanel.Controls.Count > 0)
            {
                for (int i = 0; i != printerFLowLayoutPanel.Controls.Count;)
                {
                    printerFLowLayoutPanel.Controls.Remove(printerFLowLayoutPanel.Controls[i]);
                }
            }

        }
        

        /// <summary>
        /// Initialize all the components and assign the pertinent values
        /// </summary>
        public void Start()
        {
            ReseLayout();
            int nbPrinter = Printers.Count;

            Button[] btninfo = new Button[nbPrinter];
            Panel[] panel = new Panel[nbPrinter];
            PictureBox[] img = new PictureBox[nbPrinter];
            TableLayoutPanel[] table = new TableLayoutPanel[nbPrinter];
            Label[] Infooneline = new Label[nbPrinter];
            //RichTextBox[] Description = new RichTextBox[nbPrinter];

            /// <summary>
            /// Init
            /// </summary>
            for (int H = 0; H < nbPrinter; H++)
            {
                panel[H] = new Panel();
                panel[H].Name = Convert.ToString(H);
                panel[H].Size = new Size(420, 450);
                panel[H].TabIndex = H;
                panel[H].BackColor = Color.White;
                panel[H].Margin = new Padding(6);
                printerFLowLayoutPanel.Controls.Add(panel[H]);
                Refresh();
                //table layout
                table[H] = new TableLayoutPanel();
                table[H].CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
                table[H].RowCount = 5;
                table[H].ColumnCount = 2;
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
                table[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));

                table[H].Location = new Point(5, 205);
                table[H].Name = "table1";
                table[H].Size = new Size(410, 100);
                table[H].BackColor = Color.White;

                Label NameLabel;
                Label PriceLabel;
                Label BuilderLabel;
                Label ProviderLabel;
                Label PrintResLabel;
                Label ScanResLabel;
                Label ReleaseDateLabel;
                Label SizeLabel;
                Label WifiLabel;
                Label RectoVersoLabel;
                //Label CompatiblePaperLabel;
                //Label InksLabel;

                NameLabel = new Label() { Name = "Namelbl", Text = "Nom : " + Convert.ToString(Printers[H].Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                PriceLabel = new Label() { Text = "Prix : " + Convert.ToString(Printers[H].LastPrice) + ".-", Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                BuilderLabel = new Label() { Text = "Constructeur : " + Convert.ToString(Printers[H].Builder.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ProviderLabel = new Label() { Text = "Fournisseur : " + Convert.ToString(Printers[H].Provider.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                PrintResLabel = new Label() { Text = "Résolution : " + Convert.ToString(Printers[H].PrintResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ScanResLabel = new Label() { Text = "Scan : " + Convert.ToString(Printers[H].ScanResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ReleaseDateLabel = new Label() { Text = "Date de sortie : " + Convert.ToString(Printers[H].ReleaseDate.Year), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                SizeLabel = new Label() { Text = "Taille : " + Convert.ToString(Printers[H].Size.Heigth + " * " + Printers[H].Size.Length + " * " + Printers[H].Size.Width), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                WifiLabel = new Label() { Text = "Sans-fil : " + Convert.ToString(Printers[H].Wireless), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                RectoVersoLabel = new Label() { Text = "Recto-Verso : " + Convert.ToString(Printers[H].RectoVerso), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                StringBuilder strBlr = new StringBuilder();
                foreach (PapersCompatible pp in Printers[H].Papers)
                {
                    strBlr.Append(pp.Name + ",");

                }
                //CompatiblePaperLabel = new Label() { Text = "Papier : " + strBlr.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                StringBuilder strInk = new StringBuilder();
                for (int i = 0; i < Printers[H].Inks.Count; i++)
                {
                    strInk.Append(Printers[H].Inks[i].Name+" | prix : "+Printers[H].Inks[i].Price+".-");
                }
                //InksLabel           = new Label() { Text = "Consomable : " + strInk.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                table[H].Controls.Add(NameLabel, 0, 0);
                table[H].Controls.Add(PriceLabel, 1, 0);
                table[H].Controls.Add(BuilderLabel, 2, 0);
                table[H].Controls.Add(ProviderLabel, 3, 0);
                table[H].Controls.Add(PrintResLabel, 4, 0);
                table[H].Controls.Add(ScanResLabel, 5, 0);
                table[H].Controls.Add(ReleaseDateLabel, 6, 0);
                table[H].Controls.Add(SizeLabel, 7, 0);
                table[H].Controls.Add(WifiLabel, 8, 0);
                table[H].Controls.Add(RectoVersoLabel, 9, 0);

                //table[H].Controls.Add(CompatiblePaperLabel  , 10, 0);
                //table[H].Controls.Add(InksLabel             , 11, 0);


                //Table1[H].Controls.Add(new Label() { Text = "", Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 11, 0);



                //this.label1.AutoSize = true;
                //this.label1.Location = new System.Drawing.Point(436, 202);
                //this.label1.Name = "label1";
                //this.label1.Size = new System.Drawing.Size(35, 13);
                //this.label1.TabIndex = 22;
                //this.label1.Text = "label1";


                Infooneline[H] = new Label();
                Infooneline[H].AutoSize = true;
                Infooneline[H].Location = new Point(8, 305);
                Infooneline[H].Name = "consomable" + H;
                Infooneline[H].Size = new Size(410, 20);
                Infooneline[H].TabIndex = H;
                Infooneline[H].Text = "Paper : " + strBlr.ToString() + "\nConsomable : " + strInk.ToString();
                Infooneline[H].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                panel[H].Controls.Add(Infooneline[H]);

                //Description[H] = new RichTextBox();
                //Description[H].AutoSize = true;
                //Description[H].BorderStyle = BorderStyle.None;
                //Description[H].Location = new Point(10, 350);
                //Description[H].Name = "description" + H;
                //Description[H].Size = new Size(410, 100);
                //Description[H].TabIndex = H;
                //Description[H].Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam id malesuada urna. Vestibulum felis sem, tristique ut magna nec, ullamcorper vulputate eros. Nam ullamcorper viverra nulla in fringilla. Curabitur id tellus lectus. In blandit posuere nisi vitae pharetra. In quis felis mollis, iaculis quam condimentum, posuere erat. Morbi luctus libero.";
                //Description[H].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                //panel[H].Controls.Add(Description[H]);

                panel[H].Controls.Add(table[H]);

                //bouton pour avoir les info des prix
                btninfo[H] = new Button();
                btninfo[H].Name = Convert.ToString(H);
                btninfo[H].Text = "Chart Prix";
                btninfo[H].Click += new EventHandler(this.Info_Click);
                btninfo[H].BackColor = Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
                btninfo[H].FlatStyle = FlatStyle.Flat;
                btninfo[H].Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                btninfo[H].ForeColor = Color.White;
                panel[H].Controls.Add(btninfo[H]);

                //image box pour l'emplacement des images
                img[H] = new PictureBox();
                panel[H].Controls.Add(img[H]);
                img[H].BackColor = Color.White;
                img[H].Name = "img" + H;
                img[H].BackgroundImageLayout = ImageLayout.Zoom;

                //emplacement + taille du bouton info
                btninfo[H].Location = new Point(245, 10);
                btninfo[H].Size = new Size(170, 50);

                //emplacement + taille + url fichier pour les image
                img[H].Location = new Point(5, 5);
                img[H].Size = new Size(200, 200);

                try
                {
                    img[H].BackgroundImage = Image.FromFile(@"../../../photo/Print Photo/" + Printers[H].Name + ".jpg");
                }
                catch
                {
                    img[H].BackgroundImage = Image.FromFile(@"../../Resources/Img/Error.png");
                }
                Refresh();
            }
        }
        private void Info_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            ChartView cv = new ChartView();
            cv.DrawArrayToGraph(Printers[Convert.ToInt32(button.Parent.Name)].PriceEvolution);
            cv.StartPosition = FormStartPosition.Manual;
            cv.Location = new Point((this.Location.X + this.Width / 2) - (cv.Width / 2), (this.Location.Y + this.Height / 2) - (cv.Height / 2));
            cv.Show(this);
        }
        private void Home_Click(object sender, EventArgs e)
        {
            homeView.Visible = true;
            printerFLowLayoutPanel.Visible = false;
            barbtn.Location = new System.Drawing.Point(0, 600);
        }

        private void Impr_Click(object sender, EventArgs e)
        {
            homeView.Visible = false;
            barbtn.Location = new System.Drawing.Point(435, 600);
            printerFLowLayoutPanel.Focus();
            printerFLowLayoutPanel.Visible = true;
        }

        private void Autre_Click(object sender, EventArgs e)
        {
            homeView.Visible = false;
            printerFLowLayoutPanel.Visible = false;
            barbtn.Location = new System.Drawing.Point(868, 600);
        }
        private void Min_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        
        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        private void ResetBttn_Click(object sender, EventArgs e)
        {
            printerFLowLayoutPanel.Visible = false;
            //Supress the select event
            NameComboBox.SelectedIndexChanged -= new EventHandler(NameComboBox_SelectedIndexChanged);
            BrandComboBox.SelectedIndexChanged -= new EventHandler(BrandComboBox_SelectedIndexChanged);
            BuilderComboBox.SelectedIndexChanged -= new EventHandler(BuilderComboBox_SelectedIndexChanged);
            ProviderComboBox.SelectedIndexChanged -= new EventHandler(ProviderComboBox_SelectedIndexChanged);
            NumericResComboBox.SelectedIndexChanged -= new EventHandler(NumericResComboBox_SelectedIndexChanged);
            PrintResComboBox.SelectedIndexChanged -= new EventHandler(PrintResComboBox_SelectedIndexChanged);
            InksComboBox.SelectedIndexChanged -= new EventHandler(InksComboBox_SelectedIndexChanged);
            PriceComboBox.SelectedIndexChanged -= new EventHandler(PriceComboBox_SelectedIndexChanged);
            WeightComboBox.SelectedIndexChanged -= new EventHandler(WeightComboBox_SelectedIndexChanged);
            PrintSpeedComboBox.SelectedIndexChanged -= new EventHandler(PrintSpeedComboBox_SelectedIndexChanged);
            RankingComboBox.SelectedIndexChanged -= new EventHandler(RankingComboBox_SelectedIndexChanged);            
            PaperComboBox.SelectedIndexChanged -= new EventHandler(PaperComboBox_SelectedIndexChanged);        
            SizeComboBox.SelectedIndexChanged -= new EventHandler(SizeComboBox_SelectedIndexChanged);
            

            //Default Options
            NameComboBox.SelectedItem = null;
            BrandComboBox.SelectedItem = null;
            BuilderComboBox.SelectedItem = null;
            ProviderComboBox.SelectedItem = null;
            NumericResComboBox.SelectedItem = null;
            PrintResComboBox.SelectedItem = null;
            InksComboBox.SelectedItem = null;
            PaperComboBox.SelectedItem = null;
            PriceComboBox.SelectedItem = null;
            RankingComboBox.SelectedItem = null;
            PrintSpeedComboBox.SelectedItem = null;
            SizeComboBox.SelectedItem = null;
            WeightComboBox.SelectedItem = null;

            NameComboBox.Text = "Modele";
            BrandComboBox.Text = "Marque";
            BuilderComboBox.Text = "Contructeur";
            ProviderComboBox.Text = "Fournisseur";
            NumericResComboBox.Text = "Res.Numerique";
            PrintResComboBox.Text = "Res.Impression";
            InksComboBox.Text = "Encres";
            PaperComboBox.Text = "Papiers";
            PriceComboBox.Text = "Price";
            RankingComboBox.Text = "Ranking";
            PrintSpeedComboBox.Text = "Vitesse d'Impression";
            SizeComboBox.Text = "Taille";
            WeightComboBox.Text = "Poid";

            //Supress the select event
            NameComboBox.SelectedIndexChanged += new EventHandler(NameComboBox_SelectedIndexChanged);
            BrandComboBox.SelectedIndexChanged += new EventHandler(BrandComboBox_SelectedIndexChanged);
            BuilderComboBox.SelectedIndexChanged += new EventHandler(BuilderComboBox_SelectedIndexChanged);
            ProviderComboBox.SelectedIndexChanged += new EventHandler(ProviderComboBox_SelectedIndexChanged);
            NumericResComboBox.SelectedIndexChanged += new EventHandler(NumericResComboBox_SelectedIndexChanged);
            PrintResComboBox.SelectedIndexChanged += new EventHandler(PrintResComboBox_SelectedIndexChanged);
            InksComboBox.SelectedIndexChanged += new EventHandler(InksComboBox_SelectedIndexChanged);
            PriceComboBox.SelectedIndexChanged += new EventHandler(PriceComboBox_SelectedIndexChanged);
            WeightComboBox.SelectedIndexChanged += new EventHandler(WeightComboBox_SelectedIndexChanged);
            PrintSpeedComboBox.SelectedIndexChanged += new EventHandler(PrintSpeedComboBox_SelectedIndexChanged);
            RankingComboBox.SelectedIndexChanged += new EventHandler(RankingComboBox_SelectedIndexChanged);
            PaperComboBox.SelectedIndexChanged += new EventHandler(PaperComboBox_SelectedIndexChanged);
            SizeComboBox.SelectedIndexChanged += new EventHandler(SizeComboBox_SelectedIndexChanged);

            initPrinterDinQueryContainer();
            Printers = ViewController.GetDefaultPrinterModel();
            Start();
            printerFLowLayoutPanel.Visible = true;
            getFocuse();

        }

        private void UpdatePrinterView()
        {
            Printers = ViewController.UpdatePrinterView(PrinterDinQuery);
            Start();
        }

        public bool LightONOFF = false;
        private void Light_Click(object sender, EventArgs e)
        {
            //Dark mode
            if (LightONOFF)
            {
                //image du bouton 
                Light.BackgroundImage = Image.FromFile(@"../../Resources\Sun.png");
                
                //form 
                this.BackColor = Color.FromArgb(80, 80, 80);

                //Top bar 1
                paneltop.BackColor = Color.FromArgb(55,55,55);

                //Top bar 2
                bar.BackColor = Color.FromArgb(40,40,40);
                Close.BackColor = Color.FromArgb(40,40,40);
                full.BackColor = Color.FromArgb(40,40,40);
                Min.BackColor = Color.FromArgb(40,40,40);
                Close.ForeColor = Color.White;
                full.ForeColor = Color.White;
                Min.ForeColor = Color.White;
                Light.BackColor= Color.FromArgb(40, 40, 40);
                Light.FlatAppearance.MouseDownBackColor = Color.FromArgb(40, 40, 40);
                Light.FlatAppearance.MouseOverBackColor = Color.FromArgb(40, 40, 40);
                //User control
                printerFLowLayoutPanel.BackColor = Color.FromArgb(80, 80, 80);
                //UChome.graphique.BackColor = Color.FromArgb(80, 80, 80);
                //bouton autre
                Autre.BackColor = Color.FromArgb(40, 40, 40);
                Autre.ForeColor = Color.White;

                //bouton imprimente
                Impr.BackColor = Color.FromArgb(40, 40, 40);
                Impr.ForeColor = Color.White;

                //bouton home
                Home.BackColor = Color.FromArgb(40, 40, 40);
                Home.ForeColor = Color.White;

                //bool on on light
                LightONOFF = false;
            }
            //Light mode
            //#eeeeee • #dddddd • #cccccc • #bbbbbb • #aaaaaa
            else
            {
                Light.BackgroundImage = Image.FromFile(@"../../Resources\Moon.png");

                //form
                this.BackColor = Color.FromArgb(221,221,221);
                //Top bar 1
                paneltop.BackColor = Color.FromArgb(204,204,204);

                //Top bar 2
                bar.BackColor = Color.Silver;
                Close.BackColor = Color.Silver;
                full.BackColor = Color.Silver;
                Min.BackColor = Color.Silver;                
                Light.BackColor = Color.Silver;
                Light.FlatAppearance.MouseDownBackColor = Color.Silver;
                Light.FlatAppearance.MouseOverBackColor = Color.Silver;
                full.ForeColor = Color.Black;
                Min.ForeColor = Color.Black;
                Close.ForeColor = Color.Black;


                //User control
                printerFLowLayoutPanel.BackColor = Color.FromArgb(221, 221, 221);
                //bouton autre
                Autre.BackColor = Color.Silver;
                Autre.ForeColor = Color.Black;

                //bouton imprimente
                Impr.BackColor = Color.Silver;
                Impr.ForeColor = Color.Black;

                //bouton home
                Home.BackColor = Color.Silver;
                Home.ForeColor = Color.Black;

                //UChome.panel[0,0].BackColor = Color.Green;

                //bool on off light
                LightONOFF = true;
            }
            
        }

        private void signin_Click(object sender, EventArgs e)
        {
            SignView sign = new SignView();
            switch(LightONOFF)
            {
                case true:
                    sign.BackColor = Color.Silver;
                    break;
                case false:
                    sign.BackColor = Color.FromArgb(50, 50, 50);
                    break;
            }
            sign.StartPosition = FormStartPosition.Manual;
            sign.Location = new Point((this.Location.X + this.Width / 2) - (sign.Width / 2), (this.Location.Y + this.Height / 2) - (sign.Height / 2));
            sign.Show();
        }

        private void login_Click(object sender, EventArgs e)
        {
            LoginView log = new LoginView();
            switch (LightONOFF)
            {
                case true:
                    log.BackColor = Color.Silver;
                    break;
                case false:
                    log.BackColor = Color.FromArgb(50, 50, 50);
                    break;
            }
            log.StartPosition = FormStartPosition.Manual;
            log.Location = new Point((this.Location.X + this.Width / 2) - (log.Width / 2), (this.Location.Y + this.Height / 2) - (log.Height / 2));
            log.Show();
        }

        private PrinterModel PrinterDinQuery;

        private void initPrinterDinQueryContainer()
        {
            PrinterDinQuery = new PrinterModel();
            PrinterDinQuery.Name = null;
            PrinterDinQuery.Builder.Brand = null;
            PrinterDinQuery.Builder.Name = null;
            PrinterDinQuery.Provider.Name = null;
            PrinterDinQuery.ScanResolution = null;
            PrinterDinQuery.PrintResolution = null;
            PrinterDinQuery.Inks.Add(new InkCompatible());
            PrinterDinQuery.Inks[0].Name = null;
            PrinterDinQuery.Papers.Add(new PapersCompatible());
            PrinterDinQuery.Papers[0].Name = null;


        }

        public void LoadComboBoxCollections()
        {
            //Supress the select event
            NameComboBox.SelectedIndexChanged -= new EventHandler(NameComboBox_SelectedIndexChanged);
            BrandComboBox.SelectedIndexChanged -= new EventHandler(BrandComboBox_SelectedIndexChanged);
            BuilderComboBox.SelectedIndexChanged -= new EventHandler(BuilderComboBox_SelectedIndexChanged);
            ProviderComboBox.SelectedIndexChanged -= new EventHandler(ProviderComboBox_SelectedIndexChanged);
            NumericResComboBox.SelectedIndexChanged -= new EventHandler(NumericResComboBox_SelectedIndexChanged);
            PrintResComboBox.SelectedIndexChanged -= new EventHandler(PrintResComboBox_SelectedIndexChanged);
            InksComboBox.SelectedIndexChanged -= new EventHandler(InksComboBox_SelectedIndexChanged);
            PriceComboBox.SelectedIndexChanged -= new EventHandler(PriceComboBox_SelectedIndexChanged);
            WeightComboBox.SelectedIndexChanged -= new EventHandler(WeightComboBox_SelectedIndexChanged);
            PrintSpeedComboBox.SelectedIndexChanged -= new EventHandler(PrintSpeedComboBox_SelectedIndexChanged);
            RankingComboBox.SelectedIndexChanged -= new EventHandler(RankingComboBox_SelectedIndexChanged);
            PriceComboBox.SelectedIndexChanged -= new EventHandler(PriceComboBox_SelectedIndexChanged);
            PaperComboBox.SelectedIndexChanged -= new EventHandler(PaperComboBox_SelectedIndexChanged);

            //Load data sources
            NameComboBox.DataSource = ViewController.GetPrinterNames();
            BrandComboBox.DataSource = ViewController.GetBrands();
            BuilderComboBox.DataSource = ViewController.GetBuilders();
            ProviderComboBox.DataSource = ViewController.GetProviders();
            NumericResComboBox.DataSource = ViewController.GetScanResolutions();
            PrintResComboBox.DataSource = ViewController.GetPrintResolution();
            InksComboBox.DataSource = ViewController.GetInksResolution();
            PaperComboBox.DataSource = ViewController.GetPapers();

            //Default Options
            NameComboBox.SelectedItem = null;
            BrandComboBox.SelectedItem = null;
            BuilderComboBox.SelectedItem = null;
            ProviderComboBox.SelectedItem = null;
            NumericResComboBox.SelectedItem = null;
            PrintResComboBox.SelectedItem = null;
            InksComboBox.SelectedItem = null;
            PaperComboBox.SelectedItem = null;
            PriceComboBox.SelectedItem = null;
            RankingComboBox.SelectedItem = null;
            PrintSpeedComboBox.SelectedItem = null;
            SizeComboBox.SelectedItem = null;
            WeightComboBox.SelectedItem = null;

            NameComboBox.Text = "Modele";
            BrandComboBox.Text = "Marque";
            BuilderComboBox.Text = "Contructeur";
            ProviderComboBox.Text = "Fournisseur";
            NumericResComboBox.Text = "Res.Numerique";
            PrintResComboBox.Text = "Res.Impression";
            InksComboBox.Text = "Encres";
            PaperComboBox.Text = "Papiers";
            PriceComboBox.Text = "Price";
            RankingComboBox.Text = "Ranking";
            PrintSpeedComboBox.Text = "Vitesse d'Impression";
            SizeComboBox.Text = "Taille";
            WeightComboBox.Text = "Poid";


            PriceComboBox.Items.Add("Croissant");
            PriceComboBox.Items.Add("Decroissant");

            WeightComboBox.Items.Add("Croissant");
            WeightComboBox.Items.Add("Decroissant");

            PrintSpeedComboBox.Items.Add("Croissant");
            PrintSpeedComboBox.Items.Add("Decroissant");

            SizeComboBox.Items.Add("Croissant");
            SizeComboBox.Items.Add("Decroissant");

            RankingComboBox.Items.Add("Top 3");
            RankingComboBox.Items.Add("Top 5");
            RankingComboBox.Items.Add("Top 10");

            //Give back selectEvent
            //Supress the select event
            NameComboBox.SelectedIndexChanged += new EventHandler(NameComboBox_SelectedIndexChanged);
            BrandComboBox.SelectedIndexChanged += new EventHandler(BrandComboBox_SelectedIndexChanged);
            BuilderComboBox.SelectedIndexChanged += new EventHandler(BuilderComboBox_SelectedIndexChanged);
            ProviderComboBox.SelectedIndexChanged += new EventHandler(ProviderComboBox_SelectedIndexChanged);
            NumericResComboBox.SelectedIndexChanged += new EventHandler(NumericResComboBox_SelectedIndexChanged);
            PrintResComboBox.SelectedIndexChanged += new EventHandler(PrintResComboBox_SelectedIndexChanged);
            InksComboBox.SelectedIndexChanged += new EventHandler(InksComboBox_SelectedIndexChanged);
            PriceComboBox.SelectedIndexChanged += new EventHandler(PriceComboBox_SelectedIndexChanged);
            WeightComboBox.SelectedIndexChanged += new EventHandler(WeightComboBox_SelectedIndexChanged);
            PrintSpeedComboBox.SelectedIndexChanged += new EventHandler(PrintSpeedComboBox_SelectedIndexChanged);
            RankingComboBox.SelectedIndexChanged += new EventHandler(RankingComboBox_SelectedIndexChanged);
            PriceComboBox.SelectedIndexChanged += new EventHandler(PriceComboBox_SelectedIndexChanged);
            PaperComboBox.SelectedIndexChanged += new EventHandler(PaperComboBox_SelectedIndexChanged);
        }

        

        private void NameComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (NameComboBox.SelectedItem == null) { PrinterDinQuery.Name = null; }
            else {PrinterDinQuery.Name = NameComboBox.Text; }
            
            getFocuse();
            UpdatePrinterView();
        }

        private void BuilderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BuilderComboBox.SelectedItem == null) { PrinterDinQuery.Builder.Name = null; }
            else { PrinterDinQuery.Builder.Name = BuilderComboBox.Text; }
            getFocuse();
            UpdatePrinterView();
        }

        private void ProviderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ProviderComboBox.SelectedItem == null) { PrinterDinQuery.Provider.Name = null; }
            else { PrinterDinQuery.Provider.Name = ProviderComboBox.Text; }
            getFocuse();
            UpdatePrinterView();
        }

        private void NumericResComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (NumericResComboBox.SelectedItem == null) { PrinterDinQuery.ScanResolution = null; }
            else { PrinterDinQuery.ScanResolution = NumericResComboBox.Text; }
            getFocuse();
            UpdatePrinterView();
        }

        private void PrintResComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PrintResComboBox.SelectedItem == null) { PrinterDinQuery.PrintResolution = null; }
            else { PrinterDinQuery.PrintResolution = PrintResComboBox.Text; }
            getFocuse();
            UpdatePrinterView();
        }

        private void InksComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (InksComboBox.SelectedItem == null) 
            {
                InkCompatible ink = new InkCompatible();
                PrinterDinQuery.Inks[0].Name = null;
            }
            else 
            { 
                InkCompatible ink = new InkCompatible();
                PrinterDinQuery.Inks[0].Name = InksComboBox.Text;

            }

            getFocuse();
            UpdatePrinterView();
        }

        private void BrandComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BrandComboBox.SelectedItem == null) { PrinterDinQuery.Builder.Brand = null; }
            else { PrinterDinQuery.Builder.Brand = BrandComboBox.Text; }
            
            getFocuse();
            UpdatePrinterView();
        }

        private void PaperComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PaperComboBox.SelectedItem == null) 
            {
                PapersCompatible pc = new PapersCompatible();
                PrinterDinQuery.Papers[0].Name = null;

            }
            else 
            {
                PapersCompatible pc = new PapersCompatible();
                PrinterDinQuery.Papers[0].Name = PaperComboBox.Text;

            }

            getFocuse();
            UpdatePrinterView();
        }

        private void PriceComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PriceComboBox.SelectedItem == null) { PrinterDinQuery.PriceSort = null; }
            else { PrinterDinQuery.PriceSort = PriceComboBox.Text; }
           
            getFocuse();UpdatePrinterView();
        }

        private void PrintSpeedComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (PrintSpeedComboBox.SelectedItem == null) { PrinterDinQuery.PrintSpeedSort = null; }
            else { PrinterDinQuery.PrintSpeedSort = PrintSpeedComboBox.Text; }
            
            getFocuse();
            UpdatePrinterView();
        }

        private void WeightComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (WeightComboBox.SelectedItem == null) { PrinterDinQuery.WeightSort = null; }
            else { PrinterDinQuery.WeightSort = WeightComboBox.Text; }
            getFocuse();
            UpdatePrinterView();
        }

        private void SizeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (WeightComboBox.SelectedItem == null) 
            {
                PrinterDinQuery.Size.HeigthSort = null;
                PrinterDinQuery.Size.WidthSort = null;
                PrinterDinQuery.Size.LengthSort = null;
            }
            else 
            {
                PrinterDinQuery.Size.HeigthSort = InksComboBox.Text;
                PrinterDinQuery.Size.WidthSort = InksComboBox.Text;
                PrinterDinQuery.Size.LengthSort = InksComboBox.Text;
            }
           
            getFocuse();
            UpdatePrinterView();
        }

        private void RankingComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RankingComboBox.SelectedItem == null) { PrinterDinQuery.QueryLimit = null; }
            else 
            {
                string i = RankingComboBox.Text;
                switch (i) 
                {
                    case "Top 3":
                        PrinterDinQuery.QueryLimit = 3;
                        break;
                    case "Top 5":
                        PrinterDinQuery.QueryLimit = 5;
                        break;
                    case "Top 10":
                        PrinterDinQuery.QueryLimit = 10;
                        break;
                }
                    
            }
            getFocuse();
            UpdatePrinterView();
        }
        public void getFocuse()
        {
            printerFLowLayoutPanel.Focus();
        }

    }
}
