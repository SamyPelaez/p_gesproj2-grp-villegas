﻿namespace ProjetPrinter.View
{
    partial class Sign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.Close = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnnewacc = new System.Windows.Forms.Button();
            this.newemail = new System.Windows.Forms.TextBox();
            this.newpassword = new System.Windows.Forms.TextBox();
            this.grppass = new System.Windows.Forms.GroupBox();
            this.grpmail = new System.Windows.Forms.GroupBox();
            this.grpuser = new System.Windows.Forms.GroupBox();
            this.newusername = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.grppass.SuspendLayout();
            this.grpmail.SuspendLayout();
            this.grpuser.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.panel1.Controls.Add(this.Close);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 50);
            this.panel1.TabIndex = 12;
            // 
            // Close
            // 
            this.Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.Close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.ForeColor = System.Drawing.Color.White;
            this.Close.Location = new System.Drawing.Point(240, 0);
            this.Close.Margin = new System.Windows.Forms.Padding(0);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(60, 50);
            this.Close.TabIndex = 15;
            this.Close.Text = "X";
            this.Close.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 50);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(300, 1);
            this.panel2.TabIndex = 12;
            // 
            // btnnewacc
            // 
            this.btnnewacc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(185)))), ((int)(((byte)(17)))), ((int)(((byte)(68)))));
            this.btnnewacc.FlatAppearance.BorderSize = 0;
            this.btnnewacc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnnewacc.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnnewacc.ForeColor = System.Drawing.Color.White;
            this.btnnewacc.Location = new System.Drawing.Point(8, 279);
            this.btnnewacc.Name = "btnnewacc";
            this.btnnewacc.Size = new System.Drawing.Size(280, 38);
            this.btnnewacc.TabIndex = 4;
            this.btnnewacc.Text = "CREATE ACCOUNT";
            this.btnnewacc.UseVisualStyleBackColor = false;
            // 
            // newemail
            // 
            this.newemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newemail.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.newemail.Location = new System.Drawing.Point(6, 25);
            this.newemail.MaxLength = 255;
            this.newemail.Name = "newemail";
            this.newemail.Size = new System.Drawing.Size(268, 38);
            this.newemail.TabIndex = 1;
            this.newemail.Leave += new System.EventHandler(this.Check);
            // 
            // newpassword
            // 
            this.newpassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newpassword.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.newpassword.Location = new System.Drawing.Point(6, 20);
            this.newpassword.MaxLength = 255;
            this.newpassword.Name = "newpassword";
            this.newpassword.PasswordChar = '*';
            this.newpassword.Size = new System.Drawing.Size(268, 38);
            this.newpassword.TabIndex = 2;
            this.newpassword.Leave += new System.EventHandler(this.Check);
            // 
            // grppass
            // 
            this.grppass.Controls.Add(this.newpassword);
            this.grppass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grppass.ForeColor = System.Drawing.Color.White;
            this.grppass.Location = new System.Drawing.Point(10, 208);
            this.grppass.Name = "grppass";
            this.grppass.Size = new System.Drawing.Size(280, 65);
            this.grppass.TabIndex = 18;
            this.grppass.TabStop = false;
            this.grppass.Text = "Password*";
            // 
            // grpmail
            // 
            this.grpmail.Controls.Add(this.newemail);
            this.grpmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpmail.ForeColor = System.Drawing.Color.White;
            this.grpmail.Location = new System.Drawing.Point(10, 132);
            this.grpmail.Name = "grpmail";
            this.grpmail.Size = new System.Drawing.Size(280, 74);
            this.grpmail.TabIndex = 19;
            this.grpmail.TabStop = false;
            this.grpmail.Text = "Email";
            // 
            // grpuser
            // 
            this.grpuser.Controls.Add(this.newusername);
            this.grpuser.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpuser.ForeColor = System.Drawing.Color.White;
            this.grpuser.Location = new System.Drawing.Point(10, 57);
            this.grpuser.Name = "grpuser";
            this.grpuser.Size = new System.Drawing.Size(280, 69);
            this.grpuser.TabIndex = 20;
            this.grpuser.TabStop = false;
            this.grpuser.Text = "Username*";
            // 
            // newusername
            // 
            this.newusername.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newusername.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.newusername.Location = new System.Drawing.Point(6, 25);
            this.newusername.MaxLength = 255;
            this.newusername.Name = "newusername";
            this.newusername.Size = new System.Drawing.Size(268, 38);
            this.newusername.TabIndex = 0;
            this.newusername.Leave += new System.EventHandler(this.Check);
            // 
            // Sign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.ClientSize = new System.Drawing.Size(300, 327);
            this.Controls.Add(this.grpuser);
            this.Controls.Add(this.grpmail);
            this.Controls.Add(this.grppass);
            this.Controls.Add(this.btnnewacc);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Sign";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Newlog";
            this.Deactivate += new System.EventHandler(this.close);
            this.Load += new System.EventHandler(this.Open);
            this.panel1.ResumeLayout(false);
            this.grppass.ResumeLayout(false);
            this.grppass.PerformLayout();
            this.grpmail.ResumeLayout(false);
            this.grpmail.PerformLayout();
            this.grpuser.ResumeLayout(false);
            this.grpuser.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnnewacc;
        private System.Windows.Forms.TextBox newemail;
        private System.Windows.Forms.TextBox newpassword;
        private new System.Windows.Forms.Button Close;
        private System.Windows.Forms.GroupBox grppass;
        private System.Windows.Forms.GroupBox grpmail;
        private System.Windows.Forms.GroupBox grpuser;
        private System.Windows.Forms.TextBox newusername;
    }
}