﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition...
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;..
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;...
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;...
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.

using System;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ProjetPrinter.Controller;


namespace ProjetPrinter.View
{
    public partial class SignView : Form, IView
    {
        public MainController ViewController { get; set; }

        public SignView()
        {
            InitializeComponent();
        }

        private void Check(object sender, EventArgs e)
        {
            TextBox check = (TextBox)sender;
            if (check.Name == newusername.Name)
            {
                //model
                string patern = @"^[A-za-z]{2,20}?[0-9]{0,9}$";
                //view
                if (!Regex.IsMatch(newusername.Text, patern))
                {
                    //error
                    grpuser.ForeColor = Color.Red;
                }
                else
                {
                    //check
                    grpuser.ForeColor = Color.Lime;
                }
            }
            if (check.Name == newpassword.Name)
            {
                //model
                string patern = @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$";
                //view
                if (!Regex.IsMatch(newpassword.Text, patern))
                {
                    //error
                    grppass.ForeColor = Color.Red;

                }
                else
                {
                    //check
                    grppass.ForeColor = Color.Lime;
                }
            }
            if (check.Name == newemail.Name)
            {
                //model
                string patern = @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
                //view
                if (!Regex.IsMatch(newemail.Text, patern))
                {
                    //error
                    grpmail.ForeColor = Color.Red;

                }
                else
                {
                    //check
                    grpmail.ForeColor = Color.Lime;
                }
            }
        }

        private void close(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Open(object sender, EventArgs e)
        {
            if (this.BackColor == Color.Silver)
            {
                //light mode
                foreach (Control x in this.Controls)
                {                    
                    x.ForeColor = Color.Black;                    
                }
            }
            else
            {
                //dark mode
                foreach (Control x in this.Controls)
                {
                    x.ForeColor = Color.White;
                }

            }
        }
    }
}
