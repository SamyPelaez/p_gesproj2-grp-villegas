﻿namespace ProjetPrinter
{
    partial class Projet
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bar = new System.Windows.Forms.Panel();
            this.Light = new System.Windows.Forms.Button();
            this.signin = new System.Windows.Forms.Button();
            this.login = new System.Windows.Forms.Button();
            this.Min = new System.Windows.Forms.Button();
            this.full = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.paneltop = new System.Windows.Forms.Panel();
            this.btnpannier = new System.Windows.Forms.Button();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.animationTimer = new System.Windows.Forms.Timer(this.components);
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Home = new System.Windows.Forms.Button();
            this.Impr = new System.Windows.Forms.Button();
            this.Classement = new System.Windows.Forms.Button();
            this.Consom = new System.Windows.Forms.Button();
            this.Autre = new System.Windows.Forms.Button();
            this.barbtn = new System.Windows.Forms.Panel();
            this.shop = new System.Windows.Forms.Panel();
            this.home1 = new ProjetPrinter.Home();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.bar.SuspendLayout();
            this.paneltop.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // bar
            // 
            this.bar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.bar.Controls.Add(this.Light);
            this.bar.Controls.Add(this.signin);
            this.bar.Controls.Add(this.login);
            this.bar.Controls.Add(this.Min);
            this.bar.Controls.Add(this.full);
            this.bar.Controls.Add(this.Close);
            this.bar.Cursor = System.Windows.Forms.Cursors.Default;
            this.bar.Location = new System.Drawing.Point(0, 0);
            this.bar.Margin = new System.Windows.Forms.Padding(0);
            this.bar.Name = "bar";
            this.bar.Size = new System.Drawing.Size(1300, 50);
            this.bar.TabIndex = 5;
            // 
            // Light
            // 
            this.Light.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Light.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Light.FlatAppearance.BorderSize = 0;
            this.Light.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Light.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Light.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Light.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Light.ForeColor = System.Drawing.Color.White;
            this.Light.Location = new System.Drawing.Point(1067, 5);
            this.Light.Name = "Light";
            this.Light.Size = new System.Drawing.Size(40, 40);
            this.Light.TabIndex = 5;
            this.Light.UseVisualStyleBackColor = false;
            this.Light.Click += new System.EventHandler(this.Light_Click);
            // 
            // signin
            // 
            this.signin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.signin.FlatAppearance.BorderSize = 0;
            this.signin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signin.ForeColor = System.Drawing.Color.White;
            this.signin.Location = new System.Drawing.Point(12, 14);
            this.signin.Name = "signin";
            this.signin.Size = new System.Drawing.Size(75, 25);
            this.signin.TabIndex = 4;
            this.signin.Text = "Sign in";
            this.signin.UseVisualStyleBackColor = false;
            this.signin.Click += new System.EventHandler(this.signin_Click);
            // 
            // login
            // 
            this.login.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.login.FlatAppearance.BorderSize = 0;
            this.login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.ForeColor = System.Drawing.Color.White;
            this.login.Location = new System.Drawing.Point(98, 14);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(75, 25);
            this.login.TabIndex = 3;
            this.login.Text = "Login";
            this.login.UseVisualStyleBackColor = false;
            this.login.Click += new System.EventHandler(this.login_Click);
            // 
            // Min
            // 
            this.Min.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Min.FlatAppearance.BorderSize = 0;
            this.Min.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Gray;
            this.Min.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.Min.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Min.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Min.ForeColor = System.Drawing.Color.White;
            this.Min.Location = new System.Drawing.Point(1120, 0);
            this.Min.Margin = new System.Windows.Forms.Padding(0);
            this.Min.Name = "Min";
            this.Min.Size = new System.Drawing.Size(60, 50);
            this.Min.TabIndex = 2;
            this.Min.Text = "--";
            this.Min.UseVisualStyleBackColor = false;
            this.Min.Click += new System.EventHandler(this.Min_Click);
            // 
            // full
            // 
            this.full.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.full.Enabled = false;
            this.full.FlatAppearance.BorderSize = 0;
            this.full.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(0)))), ((int)(((byte)(158)))));
            this.full.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.full.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.full.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.full.ForeColor = System.Drawing.Color.White;
            this.full.Location = new System.Drawing.Point(1180, 0);
            this.full.Margin = new System.Windows.Forms.Padding(0);
            this.full.Name = "full";
            this.full.Size = new System.Drawing.Size(60, 50);
            this.full.TabIndex = 1;
            this.full.Text = "[ ]";
            this.full.UseVisualStyleBackColor = false;
            // 
            // Close
            // 
            this.Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Close.FlatAppearance.BorderSize = 0;
            this.Close.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.Close.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.ForeColor = System.Drawing.Color.White;
            this.Close.Location = new System.Drawing.Point(1240, 0);
            this.Close.Margin = new System.Windows.Forms.Padding(0);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(60, 50);
            this.Close.TabIndex = 0;
            this.Close.Text = "X";
            this.Close.UseVisualStyleBackColor = false;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // paneltop
            // 
            this.paneltop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.paneltop.Controls.Add(this.comboBox8);
            this.paneltop.Controls.Add(this.comboBox7);
            this.paneltop.Controls.Add(this.comboBox6);
            this.paneltop.Controls.Add(this.btnpannier);
            this.paneltop.Controls.Add(this.comboBox5);
            this.paneltop.Controls.Add(this.comboBox4);
            this.paneltop.Controls.Add(this.comboBox3);
            this.paneltop.Controls.Add(this.comboBox1);
            this.paneltop.Controls.Add(this.comboBox2);
            this.paneltop.Location = new System.Drawing.Point(0, 50);
            this.paneltop.Name = "paneltop";
            this.paneltop.Size = new System.Drawing.Size(1300, 40);
            this.paneltop.TabIndex = 16;
            // 
            // btnpannier
            // 
            this.btnpannier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.btnpannier.FlatAppearance.BorderSize = 0;
            this.btnpannier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpannier.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpannier.ForeColor = System.Drawing.Color.White;
            this.btnpannier.Location = new System.Drawing.Point(1090, 2);
            this.btnpannier.Name = "btnpannier";
            this.btnpannier.Size = new System.Drawing.Size(200, 33);
            this.btnpannier.TabIndex = 6;
            this.btnpannier.Text = "Panier";
            this.btnpannier.UseVisualStyleBackColor = false;
            this.btnpannier.Click += new System.EventHandler(this.btnpannier_Click);
            // 
            // comboBox5
            // 
            this.comboBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox5.ForeColor = System.Drawing.Color.White;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(396, 8);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 24);
            this.comboBox5.TabIndex = 5;
            // 
            // comboBox4
            // 
            this.comboBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.ForeColor = System.Drawing.Color.White;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(523, 8);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 24);
            this.comboBox4.TabIndex = 4;
            this.comboBox4.Text = "Fournisseur";
            // 
            // comboBox3
            // 
            this.comboBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox3.ForeColor = System.Drawing.Color.White;
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "All",
            "Top 10",
            "Top 5",
            "Top 3"});
            this.comboBox3.Location = new System.Drawing.Point(951, 8);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 24);
            this.comboBox3.TabIndex = 3;
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.Color.White;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(777, 8);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(168, 24);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.Text = "Technique d\'impression";
            // 
            // comboBox2
            // 
            this.comboBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox2.ForeColor = System.Drawing.Color.White;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(650, 8);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 24);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.Text = "Constructeur";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.Home);
            this.flowLayoutPanel1.Controls.Add(this.Impr);
            this.flowLayoutPanel1.Controls.Add(this.Classement);
            this.flowLayoutPanel1.Controls.Add(this.Consom);
            this.flowLayoutPanel1.Controls.Add(this.Autre);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 615);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1300, 200);
            this.flowLayoutPanel1.TabIndex = 18;
            // 
            // Home
            // 
            this.Home.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Home.FlatAppearance.BorderSize = 0;
            this.Home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Home.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Home.ForeColor = System.Drawing.Color.White;
            this.Home.Location = new System.Drawing.Point(0, 0);
            this.Home.Margin = new System.Windows.Forms.Padding(0);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(260, 200);
            this.Home.TabIndex = 15;
            this.Home.Text = "Home";
            this.Home.UseVisualStyleBackColor = false;
            this.Home.Click += new System.EventHandler(this.Home_Click);
            // 
            // Impr
            // 
            this.Impr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Impr.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Impr.FlatAppearance.BorderSize = 0;
            this.Impr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Impr.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Impr.ForeColor = System.Drawing.Color.White;
            this.Impr.Location = new System.Drawing.Point(260, 0);
            this.Impr.Margin = new System.Windows.Forms.Padding(0);
            this.Impr.Name = "Impr";
            this.Impr.Size = new System.Drawing.Size(260, 200);
            this.Impr.TabIndex = 16;
            this.Impr.Text = "Imprimante";
            this.Impr.UseVisualStyleBackColor = false;
            this.Impr.Click += new System.EventHandler(this.Impr_Click);
            // 
            // Classement
            // 
            this.Classement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Classement.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Classement.FlatAppearance.BorderSize = 0;
            this.Classement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Classement.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Classement.ForeColor = System.Drawing.Color.White;
            this.Classement.Location = new System.Drawing.Point(520, 0);
            this.Classement.Margin = new System.Windows.Forms.Padding(0);
            this.Classement.Name = "Classement";
            this.Classement.Size = new System.Drawing.Size(260, 200);
            this.Classement.TabIndex = 20;
            this.Classement.Text = "Classement";
            this.Classement.UseVisualStyleBackColor = false;
            this.Classement.Click += new System.EventHandler(this.Classement_Click);
            // 
            // Consom
            // 
            this.Consom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Consom.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Consom.FlatAppearance.BorderSize = 0;
            this.Consom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Consom.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Consom.ForeColor = System.Drawing.Color.White;
            this.Consom.Location = new System.Drawing.Point(780, 0);
            this.Consom.Margin = new System.Windows.Forms.Padding(0);
            this.Consom.Name = "Consom";
            this.Consom.Size = new System.Drawing.Size(260, 200);
            this.Consom.TabIndex = 17;
            this.Consom.Text = "Consommable";
            this.Consom.UseVisualStyleBackColor = false;
            this.Consom.Click += new System.EventHandler(this.Consom_Click);
            // 
            // Autre
            // 
            this.Autre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.Autre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Autre.FlatAppearance.BorderSize = 0;
            this.Autre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Autre.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Autre.ForeColor = System.Drawing.Color.White;
            this.Autre.Location = new System.Drawing.Point(1040, 0);
            this.Autre.Margin = new System.Windows.Forms.Padding(0);
            this.Autre.Name = "Autre";
            this.Autre.Size = new System.Drawing.Size(260, 200);
            this.Autre.TabIndex = 18;
            this.Autre.Text = "Autre";
            this.Autre.UseVisualStyleBackColor = false;
            this.Autre.Click += new System.EventHandler(this.Autre_Click);
            // 
            // barbtn
            // 
            this.barbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.barbtn.ForeColor = System.Drawing.Color.Black;
            this.barbtn.Location = new System.Drawing.Point(0, 600);
            this.barbtn.Name = "barbtn";
            this.barbtn.Size = new System.Drawing.Size(260, 15);
            this.barbtn.TabIndex = 19;
            // 
            // shop
            // 
            this.shop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.shop.Location = new System.Drawing.Point(1300, 90);
            this.shop.Name = "shop";
            this.shop.Size = new System.Drawing.Size(300, 525);
            this.shop.TabIndex = 20;
            // 
            // home1
            // 
            this.home1.Location = new System.Drawing.Point(0, 90);
            this.home1.Name = "home1";
            this.home1.Size = new System.Drawing.Size(1300, 510);
            this.home1.TabIndex = 21;
            this.home1.Visible = false;
            this.home1.Click += new System.EventHandler(this.getfocus);
            // 
            // comboBox6
            // 
            this.comboBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox6.ForeColor = System.Drawing.Color.White;
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(269, 8);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(121, 24);
            this.comboBox6.TabIndex = 7;
            // 
            // comboBox7
            // 
            this.comboBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox7.ForeColor = System.Drawing.Color.White;
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(142, 8);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(121, 24);
            this.comboBox7.TabIndex = 8;
            // 
            // comboBox8
            // 
            this.comboBox8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
            this.comboBox8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.comboBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox8.ForeColor = System.Drawing.Color.White;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(15, 9);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(121, 24);
            this.comboBox8.TabIndex = 9;
            // 
            // Projet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.ClientSize = new System.Drawing.Size(1300, 815);
            this.Controls.Add(this.home1);
            this.Controls.Add(this.barbtn);
            this.Controls.Add(this.shop);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.paneltop);
            this.Controls.Add(this.bar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Projet";
            this.bar.ResumeLayout(false);
            this.paneltop.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel bar;
        private System.Windows.Forms.Button Min;
        private System.Windows.Forms.Button full;
        private new System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button signin;
        private System.Windows.Forms.Button login;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel paneltop;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Timer animationTimer;
        private System.Windows.Forms.Button btnpannier;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button Home;
        private System.Windows.Forms.Button Impr;
        private System.Windows.Forms.Button Classement;
        private System.Windows.Forms.Button Autre;
        private System.Windows.Forms.Button Consom;
        private System.Windows.Forms.Panel barbtn;
        private System.Windows.Forms.Panel shop;
        private System.Windows.Forms.Button Light;
        private Home home1;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox6;
    }
}

