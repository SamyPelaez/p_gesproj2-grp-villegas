﻿namespace ProjetPrinter.View
{
    partial class Panier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closthis = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.scrollbar = new System.Windows.Forms.Panel();
            this.scroll = new System.Windows.Forms.Panel();
            this.scrollbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // closthis
            // 
            this.closthis.AutoSize = true;
            this.closthis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(62)))), ((int)(((byte)(71)))));
            this.closthis.FlatAppearance.BorderSize = 0;
            this.closthis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closthis.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closthis.ForeColor = System.Drawing.Color.White;
            this.closthis.Location = new System.Drawing.Point(0, 0);
            this.closthis.Margin = new System.Windows.Forms.Padding(0);
            this.closthis.Name = "closthis";
            this.closthis.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.closthis.Size = new System.Drawing.Size(40, 40);
            this.closthis.TabIndex = 0;
            this.closthis.Text = "X";
            this.closthis.UseVisualStyleBackColor = false;
            this.closthis.Click += new System.EventHandler(this.closthis_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 9);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(403, 479);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // scrollbar
            // 
            this.scrollbar.BackColor = System.Drawing.Color.White;
            this.scrollbar.Controls.Add(this.scroll);
            this.scrollbar.Controls.Add(this.closthis);
            this.scrollbar.Location = new System.Drawing.Point(395, 9);
            this.scrollbar.Name = "scrollbar";
            this.scrollbar.Size = new System.Drawing.Size(40, 479);
            this.scrollbar.TabIndex = 0;
            // 
            // scroll
            // 
            this.scroll.BackColor = System.Drawing.Color.Black;
            this.scroll.Location = new System.Drawing.Point(0, 41);
            this.scroll.Name = "scroll";
            this.scroll.Size = new System.Drawing.Size(40, 40);
            this.scroll.TabIndex = 1;
            // 
            // Panier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(440, 500);
            this.Controls.Add(this.scrollbar);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Panier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Panier";
            this.Deactivate += new System.EventHandler(this.Close);
            this.Leave += new System.EventHandler(this.Close);
            this.scrollbar.ResumeLayout(false);
            this.scrollbar.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button closthis;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel scrollbar;
        private System.Windows.Forms.Panel scroll;
    }

}