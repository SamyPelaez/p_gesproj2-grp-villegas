﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition...
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;..
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;...
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;...
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetPrinter.Controller;

namespace ProjetPrinter.View
{
    public partial class LoginView : Form, IView
    {
        public MainController ViewController { get; set; }


        public LoginView()
        {
            InitializeComponent();
        }

        private void Close(object sender, EventArgs e)
        {
            this.Close();
        }

        private void check(object sender, EventArgs e)
        {
            TextBox check = (TextBox)sender;
            if (check.Name == username.Name)
            {
                //model
                string patern = @"^[A-za-z]{2,20}?[0-9]{0,9}$";
                //view
                if (!Regex.IsMatch(username.Text, patern))
                {
                    //error
                    usernamebox.ForeColor = Color.Red;
                }
                else
                {
                    //check
                    usernamebox.ForeColor = Color.Lime;
                }
            }
            if (check.Name == password.Name)
            {
                //model
                string patern = @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$";
                //view
                if (!Regex.IsMatch(password.Text, patern))
                {
                    //error
                    passwordbox.ForeColor = Color.Red;

                }
                else
                {
                    //check
                    passwordbox.ForeColor = Color.Lime;
                }
            }
        }

        private void Open(object sender, EventArgs e)
        {
            if (this.BackColor == Color.Silver)
            {
                //light mode
                foreach (Label x in this.Controls.OfType<Label>())
                {
                    x.ForeColor = Color.Black;
                }
            }
            else
            {
                //dark mode
                foreach (Label x in this.Controls.OfType<Label>())
                {
                    x.ForeColor = Color.White;
                }
            }
        }
    }
}
