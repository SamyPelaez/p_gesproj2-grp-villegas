﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using ProjetPrinter.Controller;
using ProjetPrinter.Model;

namespace ProjetPrinter.View
{
    public partial class ChartView : Form, IView
    {
        private Panel charPanel;
        Panel[] points;
        private PrinterModel printer;
        private List<PriceInTime> currentPriceInTime;

        public MainController ViewController { get; set; }

        public ChartView()
        {
            InitializeComponent();
            label1.Text = "Evolution prix :";
        }

        private void closthis_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void close_this(object sender, EventArgs e)
        {
            this.Close();
        }

        public void GetPrinter(PrinterModel _printer)
        {
            printer = _printer;
        }

        public void DrawArrayToGraph(List<PriceInTime> priceInTime)
        {
            currentPriceInTime = priceInTime;

            Label test = new Label();
            this.Controls.Add(test);

            charPanel = new Panel();
            charPanel.Size = new Size(this.Width / 10 * 7, this.Height / 10 * 7);
            charPanel.Location = new Point(this.Width / 10, 55);
            charPanel.BackColor = this.BackColor; //gggggggggggggggg

            points = new Panel[currentPriceInTime.Count];

            int width = charPanel.Width / 25;
            if (points.Length > 20)
            {
                width = (charPanel.Width - points.Length * 2) / points.Length;
            }
            int offset = width + 2;

            int min = Convert.ToInt32(currentPriceInTime[0].Price) / 10;
            for (int i = 1; i < points.Length; i++)
            {
                if (currentPriceInTime[i].Price < min)
                {
                    min = Convert.ToInt32(currentPriceInTime[0].Price) / 10;
                }
            }

            int max = 340;
            for (int i = 1; i < points.Length; i++)
            {
                if (currentPriceInTime[i].Price > max)
                {
                    max = Convert.ToInt32(currentPriceInTime[i].Price);
                }
            }
            int counter = 0;
            int temp = max - min * counter;

            while (temp > 345)
            {
                ++counter;
                temp = max - min * counter;
            }
            temp = min * counter;

            for (int i = 0; i < points.Length; i++)
            {
                points[i] = new Panel();
                charPanel.Controls.Add(points[i]);
                points[i].Name = "PanelPrice" + i;
                points[i].Size = new Size(width, Convert.ToInt32(currentPriceInTime[i].Price) - temp);
                points[i].BackColor = Color.White; //ggggggggggggggggggggggg


                points[i].Location = new Point(offset * i, charPanel.Height - points[i].Height);

                points[i].Visible = true;
                points[i].MouseHover += PriceHoover;
                //Thread.Sleep(10);
                test.Text = Convert.ToString(min);
                test.Hide();

                label2.Text = currentPriceInTime[0].Price + "CHF  -  " + currentPriceInTime[0].Date.ToShortDateString();
            }
            DrawAxes();
        }

        private void PriceHoover(object sender, EventArgs e)
        {
            Panel button = (Panel)sender;
            int index = Convert.ToInt32(button.Name.Remove(0, 10));
            label2.Text = currentPriceInTime[index].Price + "CHF  -  " + currentPriceInTime[index].Date.ToShortDateString();
        }

        private void DrawAxes()
        {
            Panel axeX = new Panel();
            this.Controls.Add(axeX);
            axeX.Size = new Size(charPanel.Width, 5);
            axeX.Location = new Point(charPanel.Location.X, charPanel.Location.Y + charPanel.Height + 1);
            axeX.BackColor = Color.Gray; //gggggggggggggggg

            Panel axeY = new Panel();
            this.Controls.Add(axeY);
            axeY.Size = new Size(5, charPanel.Height);
            axeY.Location = new Point(charPanel.Location.X - axeY.Width - 1, charPanel.Location.Y);
            axeY.BackColor = Color.Gray; //gggggggggggggggggggggg

            Label dates = new Label();
            this.Controls.Add(dates);
            dates.Size = new Size(charPanel.Width, 25);
            dates.Location = new Point(axeX.Location.X, axeX.Location.Y + axeX.Height + 1);
            dates.BackColor = Color.Transparent;
            dates.Text = "Dates   →";
            dates.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dates.TextAlign = ContentAlignment.MiddleCenter;

            myLabel price = new myLabel();
            this.Controls.Add(price);
            price.Text = "";
            price.AutoSize = false;
            price.NewText = "Price   →";
            price.Size = new Size(25, charPanel.Height);
            price.Location = new Point(axeY.Location.X - price.Width - 1, axeY.Location.Y);
            price.BackColor = Color.Transparent;
            price.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            price.RotateAngle = -90;
        }

        private void ChartView_Load(object sender, EventArgs e)
        {

            this.Controls.Add(charPanel);

        }
    }

    class myLabel : System.Windows.Forms.Label
    {
        public int RotateAngle { get; set; }  // to rotate your text
        public string NewText { get; set; }   // to draw text
        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            Brush b = new SolidBrush(this.ForeColor);
            e.Graphics.TranslateTransform(0, this.Height / 2);
            e.Graphics.RotateTransform(this.RotateAngle);
            e.Graphics.DrawString(this.NewText, this.Font, b, 0f, 0f);
            base.OnPaint(e);
        }
    }
}
