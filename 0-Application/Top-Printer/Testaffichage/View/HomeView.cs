﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition...
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;..
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;...
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;...
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.


using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ProjetPrinter.Controller;
using ProjetPrinter.Model;

namespace ProjetPrinter.View
{
    public partial class HomeView: UserControl, IView
    {
        public MainController ViewController { get; set; }
        private List<PrinterModel> Printers { get; set; }

        public HomeView()
        {
            InitializeComponent();
        }

        ///// <summary>
        ///// [GET REQUEST] : Model-View transaction request
        ///// </summary>
        ///// <param name="printers"></param>
        public void GetDefaultPrinterModel(List<PrinterModel> printers)
        {
            Printers = printers;
            this.Start();
            this.Show();
        }

        ///// <summary>
        ///// [GET REQUEST] : Model-View transaction request
        ///// </summary>
        ///// <param name="printers"></param>
        public void GetCustomPrinterModel(List<PrinterModel> printers)
        {
            Printers = printers;
            this.Start();
            this.Show();
        }


        /// <summary>
        /// Variables of visual objs
        /// </summary>
        private int nbPrinter;

        private bool[] open;
        private Button[] btninfo;
        private Button[] btnadd;
        private Panel[] panel;
        private PictureBox[] img;
        private TableLayoutPanel[] table;
        private Label[] Infooneline;
        private RichTextBox[] Description;



        /// <summary>
        /// Initialize all the components and assign the pertinent values
        /// </summary>
        public void Start()
        {
            nbPrinter = Printers.Count;

            open = new bool[nbPrinter];
            btninfo = new Button[nbPrinter];
            btnadd = new Button[nbPrinter];
            panel = new Panel[nbPrinter];
            img = new PictureBox[nbPrinter];
            table = new TableLayoutPanel[nbPrinter];
            Infooneline = new Label[nbPrinter];
            Description = new RichTextBox[nbPrinter];


            /// <summary>
            /// Init
            /// </summary>
            for (int H = 0; H < nbPrinter; H++)
            {
                panel[H] = new Panel();
                panel[H].Name = Convert.ToString(H);
                panel[H].Size = new Size(420, 450);
                panel[H].TabIndex = H;
                panel[H].BackColor = Color.White;
                panel[H].Margin = new Padding(6);
                Test.Controls.Add(panel[H]);

                //table layout
                table[H] = new TableLayoutPanel();
                table[H].CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
                table[H].RowCount = 5;
                table[H].ColumnCount = 2;
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
                table[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));

                table[H].Location = new Point(5, 205);
                table[H].Name = "table1";
                table[H].Size = new Size(410, 100);
                table[H].BackColor = Color.White;

                Label NameLabel;
                Label PriceLabel;
                Label BuilderLabel;
                Label ProviderLabel;
                Label PrintResLabel;
                Label ScanResLabel;
                Label ReleaseDateLabel;
                Label SizeLabel;
                Label WifiLabel;
                Label RectoVersoLabel;
                //Label CompatiblePaperLabel;
                //Label InksLabel;

                NameLabel = new Label() { Name = "Namelbl", Text = "Nom : " + Convert.ToString(Printers[H].Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                PriceLabel = new Label() { Text = "Prix : " + Convert.ToString(Printers[H].LastPrice) + ".-", Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                BuilderLabel = new Label() { Text = "Constructeur : " + Convert.ToString(Printers[H].Builder.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ProviderLabel = new Label() { Text = "Fournisseur : " + Convert.ToString(Printers[H].Provider.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                PrintResLabel = new Label() { Text = "Résolution : " + Convert.ToString(Printers[H].PrintResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ScanResLabel = new Label() { Text = "Scan : " + Convert.ToString(Printers[H].ScanResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ReleaseDateLabel = new Label() { Text = "Date de sortie : " + Convert.ToString(Printers[H].ReleaseDate.Year), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                SizeLabel = new Label() { Text = "Taille : " + Convert.ToString(Printers[H].Size.Heigth + " * " + Printers[H].Size.Length + " * " + Printers[H].Size.Width), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                WifiLabel = new Label() { Text = "Sans-fil : " + Convert.ToString(Printers[H].Wireless), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                RectoVersoLabel = new Label() { Text = "Recto-Verso : " + Convert.ToString(Printers[H].RectoVerso), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                StringBuilder strBlr = new StringBuilder();
                foreach (PapersCompatible pp in Printers[H].Papers)
                {
                    strBlr.Append(pp.Name + ",");

                }
                //CompatiblePaperLabel = new Label() { Text = "Papier : " + strBlr.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                StringBuilder strInk = new StringBuilder();
                for (int i = 0; i < Printers[H].Inks.Count; i++)
                {
                    strInk.Append(Printers[H].Inks[i].Name);
                }
                //InksLabel           = new Label() { Text = "Consomable : " + strInk.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                table[H].Controls.Add(NameLabel, 0, 0);
                table[H].Controls.Add(PriceLabel, 1, 0);
                table[H].Controls.Add(BuilderLabel, 2, 0);
                table[H].Controls.Add(ProviderLabel, 3, 0);
                table[H].Controls.Add(PrintResLabel, 4, 0);
                table[H].Controls.Add(ScanResLabel, 5, 0);
                table[H].Controls.Add(ReleaseDateLabel, 6, 0);
                table[H].Controls.Add(SizeLabel, 7, 0);
                table[H].Controls.Add(WifiLabel, 8, 0);
                table[H].Controls.Add(RectoVersoLabel, 9, 0);

                //table[H].Controls.Add(CompatiblePaperLabel  , 10, 0);
                //table[H].Controls.Add(InksLabel             , 11, 0);


                //Table1[H].Controls.Add(new Label() { Text = "", Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 11, 0);



                //this.label1.AutoSize = true;
                //this.label1.Location = new System.Drawing.Point(436, 202);
                //this.label1.Name = "label1";
                //this.label1.Size = new System.Drawing.Size(35, 13);
                //this.label1.TabIndex = 22;
                //this.label1.Text = "label1";


                Infooneline[H] = new Label();
                Infooneline[H].AutoSize = true;
                Infooneline[H].Location = new Point(8, 305);
                Infooneline[H].Name = "consomable" + H;
                Infooneline[H].Size = new Size(410, 20);
                Infooneline[H].TabIndex = H;
                Infooneline[H].Text = "Autre :\n Paper : " + strBlr.ToString() + "\n Consomable : " + strInk.ToString();
                Infooneline[H].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                panel[H].Controls.Add(Infooneline[H]);

                Description[H] = new RichTextBox();
                Description[H].AutoSize = true;
                Description[H].BorderStyle = BorderStyle.None;
                Description[H].Location = new Point(10, 350);
                Description[H].Name = "description" + H;
                Description[H].Size = new Size(410, 100);
                Description[H].TabIndex = H;
                Description[H].Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam id malesuada urna. Vestibulum felis sem, tristique ut magna nec, ullamcorper vulputate eros. Nam ullamcorper viverra nulla in fringilla. Curabitur id tellus lectus. In blandit posuere nisi vitae pharetra. In quis felis mollis, iaculis quam condimentum, posuere erat. Morbi luctus libero.";
                Description[H].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                panel[H].Controls.Add(Description[H]);

                panel[H].Controls.Add(table[H]);

                //bouton pour avoir les info des prix
                btninfo[H] = new Button();
                btninfo[H].Name = Convert.ToString(H);
                btninfo[H].Text = "Chart Prix";
                btninfo[H].Click += new EventHandler(this.Info_Click);
                btninfo[H].BackColor = Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
                btninfo[H].FlatStyle = FlatStyle.Flat;
                btninfo[H].Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                btninfo[H].ForeColor = Color.White;
                panel[H].Controls.Add(btninfo[H]);

                //bouton ajouteur au panier
                btnadd[H] = new Button();
                btnadd[H].Name = Convert.ToString(H);
                btnadd[H].Text = "Add to Panier";
                btnadd[H].TabStop = false;// 0, 173, 181
                btnadd[H].BackColor = Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
                btnadd[H].FlatStyle = FlatStyle.Flat;
                btnadd[H].Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                btnadd[H].ForeColor = Color.White;
                panel[H].Controls.Add(btnadd[H]);

                //image box pour l'emplacement des images
                img[H] = new PictureBox();
                panel[H].Controls.Add(img[H]);
                img[H].BackColor = Color.White;
                img[H].Name = "img" + H;
                img[H].BackgroundImageLayout = ImageLayout.Zoom;

                //emplacement + taille du bouton info
                btninfo[H].Location = new Point(245, 60);
                btninfo[H].Size = new Size(170, 50);

                //emplacement + taille du bouton ajouter au pannier
                btnadd[H].Location = new Point(245, 110);
                btnadd[H].Size = new Size(170, 50);

                //emplacement + taille + url fichier pour les image
                img[H].Location = new Point(5, 5);
                img[H].Size = new Size(200, 200);

                try
                {
                    img[H].BackgroundImage = Image.FromFile(@"../../../photo/Print Photo/" + Printers[H].Name + ".jpg");
                }
                catch
                {
                    img[H].BackgroundImage = Image.FromFile(@"../../Resources/Img/Error.png");
                }

            }
        }

        

        /// <summary>
        /// Updates all the components and assign the pertinent values
        /// </summary>
        public void Update()
        {
            nbPrinter = Printers.Count;

            open = new bool[nbPrinter];
            btninfo = new Button[nbPrinter];
            btnadd = new Button[nbPrinter];
            panel = new Panel[nbPrinter];
            img = new PictureBox[nbPrinter];
            table = new TableLayoutPanel[nbPrinter];
            Infooneline = new Label[nbPrinter];
            Description = new RichTextBox[nbPrinter];


            /// <summary>
            /// Init
            /// </summary>
            for (int H = 0; H < nbPrinter; H++)
            {
                panel[H] = new Panel();
                panel[H].Name = Convert.ToString(H);
                panel[H].Size = new Size(420, 450);
                panel[H].TabIndex = H;
                panel[H].BackColor = Color.White;
                panel[H].Margin = new Padding(6);
                Test.Controls.Add(panel[H]);

                //table layout
                table[H] = new TableLayoutPanel();
                table[H].CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
                table[H].RowCount = 5;
                table[H].ColumnCount = 2;
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                table[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
                table[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));

                table[H].Location = new Point(5, 205);
                table[H].Name = "table1";
                table[H].Size = new Size(410, 100);
                table[H].BackColor = Color.White;

                Label NameLabel;
                Label PriceLabel;
                Label BuilderLabel;
                Label ProviderLabel;
                Label PrintResLabel;
                Label ScanResLabel;
                Label ReleaseDateLabel;
                Label SizeLabel;
                Label WifiLabel;
                Label RectoVersoLabel;
                //Label CompatiblePaperLabel;
                //Label InksLabel;

                NameLabel = new Label() { Name = "Namelbl", Text = "Nom : " + Convert.ToString(Printers[H].Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                PriceLabel = new Label() { Text = "Prix : " + Convert.ToString(Printers[H].LastPrice) + ".-", Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                BuilderLabel = new Label() { Text = "Constructeur : " + Convert.ToString(Printers[H].Builder.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ProviderLabel = new Label() { Text = "Fournisseur : " + Convert.ToString(Printers[H].Provider.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                PrintResLabel = new Label() { Text = "Résolution : " + Convert.ToString(Printers[H].PrintResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ScanResLabel = new Label() { Text = "Scan : " + Convert.ToString(Printers[H].ScanResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                ReleaseDateLabel = new Label() { Text = "Date de sortie : " + Convert.ToString(Printers[H].ReleaseDate.Year), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                SizeLabel = new Label() { Text = "Taille : " + Convert.ToString(Printers[H].Size.Heigth + " * " + Printers[H].Size.Length + " * " + Printers[H].Size.Width), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                WifiLabel = new Label() { Text = "Sans-fil : " + Convert.ToString(Printers[H].Wireless), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };
                RectoVersoLabel = new Label() { Text = "Recto-Verso : " + Convert.ToString(Printers[H].RectoVerso), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                StringBuilder strBlr = new StringBuilder();
                foreach (PapersCompatible pp in Printers[H].Papers)
                {
                    strBlr.Append(pp.Name + ",");

                }
                //CompatiblePaperLabel = new Label() { Text = "Papier : " + strBlr.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                StringBuilder strInk = new StringBuilder();
                for (int i = 0; i < Printers[H].Inks.Count; i++)
                {
                    strInk.Append(Printers[H].Inks[i].Name);
                }
                //InksLabel           = new Label() { Text = "Consomable : " + strInk.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true };


                table[H].Controls.Add(NameLabel, 0, 0);
                table[H].Controls.Add(PriceLabel, 1, 0);
                table[H].Controls.Add(BuilderLabel, 2, 0);
                table[H].Controls.Add(ProviderLabel, 3, 0);
                table[H].Controls.Add(PrintResLabel, 4, 0);
                table[H].Controls.Add(ScanResLabel, 5, 0);
                table[H].Controls.Add(ReleaseDateLabel, 6, 0);
                table[H].Controls.Add(SizeLabel, 7, 0);
                table[H].Controls.Add(WifiLabel, 8, 0);
                table[H].Controls.Add(RectoVersoLabel, 9, 0);

                //table[H].Controls.Add(CompatiblePaperLabel  , 10, 0);
                //table[H].Controls.Add(InksLabel             , 11, 0);


                //Table1[H].Controls.Add(new Label() { Text = "", Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 11, 0);



                //this.label1.AutoSize = true;
                //this.label1.Location = new System.Drawing.Point(436, 202);
                //this.label1.Name = "label1";
                //this.label1.Size = new System.Drawing.Size(35, 13);
                //this.label1.TabIndex = 22;
                //this.label1.Text = "label1";


                Infooneline[H] = new Label();
                Infooneline[H].AutoSize = true;
                Infooneline[H].Location = new Point(8, 305);
                Infooneline[H].Name = "consomable" + H;
                Infooneline[H].Size = new Size(410, 20);
                Infooneline[H].TabIndex = H;
                Infooneline[H].Text = "Autre :\n Paper : " + strBlr.ToString() + "\n Consomable : " + strInk.ToString();
                Infooneline[H].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                panel[H].Controls.Add(Infooneline[H]);

                Description[H] = new RichTextBox();
                Description[H].AutoSize = true;
                Description[H].BorderStyle = BorderStyle.None;
                Description[H].Location = new Point(10, 350);
                Description[H].Name = "description" + H;
                Description[H].Size = new Size(410, 100);
                Description[H].TabIndex = H;
                Description[H].Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam id malesuada urna. Vestibulum felis sem, tristique ut magna nec, ullamcorper vulputate eros. Nam ullamcorper viverra nulla in fringilla. Curabitur id tellus lectus. In blandit posuere nisi vitae pharetra. In quis felis mollis, iaculis quam condimentum, posuere erat. Morbi luctus libero.";
                Description[H].Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                panel[H].Controls.Add(Description[H]);

                panel[H].Controls.Add(table[H]);

                //bouton pour avoir les info des prix
                btninfo[H] = new Button();
                btninfo[H].Name = Convert.ToString(H);
                btninfo[H].Text = "Chart Prix";
                btninfo[H].Click += new EventHandler(this.Info_Click);
                btninfo[H].BackColor = Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
                btninfo[H].FlatStyle = FlatStyle.Flat;
                btninfo[H].Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                btninfo[H].ForeColor = Color.White;
                panel[H].Controls.Add(btninfo[H]);

                //bouton ajouteur au panier
                btnadd[H] = new Button();
                btnadd[H].Name = Convert.ToString(H);
                btnadd[H].Text = "Add to Panier";
                btnadd[H].TabStop = false;// 0, 173, 181
                btnadd[H].BackColor = Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
                btnadd[H].FlatStyle = FlatStyle.Flat;
                btnadd[H].Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                btnadd[H].ForeColor = Color.White;
                panel[H].Controls.Add(btnadd[H]);

                //image box pour l'emplacement des images
                img[H] = new PictureBox();
                panel[H].Controls.Add(img[H]);
                img[H].BackColor = Color.White;
                img[H].Name = "img" + H;
                img[H].BackgroundImageLayout = ImageLayout.Zoom;

                //emplacement + taille du bouton info
                btninfo[H].Location = new Point(245, 60);
                btninfo[H].Size = new Size(170, 50);

                //emplacement + taille du bouton ajouter au pannier
                btnadd[H].Location = new Point(245, 110);
                btnadd[H].Size = new Size(170, 50);

                //emplacement + taille + url fichier pour les image
                img[H].Location = new Point(5, 5);
                img[H].Size = new Size(200, 200);
                try
                {
                    img[H].BackgroundImage = Image.FromFile(@"../../../photo/Print Photo/" + Printers[H].Name + ".jpg");
                }
                catch
                {
                    img[H].BackgroundImage = Image.FromFile(@"../../Resources/Img/Error.png");
                }
                Refresh();
            }
        }

        private void Info_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

    } 
}
