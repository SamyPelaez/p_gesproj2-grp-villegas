﻿namespace ProjetPrinter
{
    partial class Chart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.closthis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // closthis
            // 
            this.closthis.AutoSize = true;
            this.closthis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(62)))), ((int)(((byte)(71)))));
            this.closthis.FlatAppearance.BorderSize = 0;
            this.closthis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.closthis.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closthis.ForeColor = System.Drawing.Color.White;
            this.closthis.Location = new System.Drawing.Point(555, 5);
            this.closthis.Margin = new System.Windows.Forms.Padding(0);
            this.closthis.Name = "closthis";
            this.closthis.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.closthis.Size = new System.Drawing.Size(40, 39);
            this.closthis.TabIndex = 0;
            this.closthis.Text = "X";
            this.closthis.UseVisualStyleBackColor = false;
            this.closthis.Click += new System.EventHandler(this.closthis_Click);
            // 
            // Pannier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.ClientSize = new System.Drawing.Size(600, 500);
            this.Controls.Add(this.closthis);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Pannier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Chart";
            this.Deactivate += new System.EventHandler(this.close_this);
            this.Leave += new System.EventHandler(this.close_this);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button closthis;
    }
}