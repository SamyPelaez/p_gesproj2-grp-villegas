﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetPrinter.View
{
    public partial class Sign : Form
    {
        public Sign()
        {
            InitializeComponent();
        }

        private void Check(object sender, EventArgs e)
        {
            TextBox check = (TextBox)sender;
            if (check.Name == newusername.Name)
            {
                //model
                string patern = @"^[A-za-z]{2,20}?[0-9]{0,9}$";
                //view
                if (!Regex.IsMatch(newusername.Text, patern))
                {
                    //error
                    grpuser.ForeColor = Color.Red;
                }
                else
                {
                    //check
                    grpuser.ForeColor = Color.Lime;
                }
            }
            if (check.Name == newpassword.Name)
            {
                //model
                string patern = @"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$";
                //view
                if (!Regex.IsMatch(newpassword.Text, patern))
                {
                    //error
                    grppass.ForeColor = Color.Red;

                }
                else
                {
                    //check
                    grppass.ForeColor = Color.Lime;
                }
            }
            if (check.Name == newemail.Name)
            {
                //model
                string patern = @"^([0-9a-zA-Z]([\+\-_\.][0-9a-zA-Z]+)*)+@(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]*\.)+[a-zA-Z0-9]{2,17})$";
                //view
                if (!Regex.IsMatch(newemail.Text, patern))
                {
                    //error
                    grpmail.ForeColor = Color.Red;

                }
                else
                {
                    //check
                    grpmail.ForeColor = Color.Lime;
                }
            }
        }

        private void close(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Open(object sender, EventArgs e)
        {
            if (this.BackColor == Color.Silver)
            {
                //light mode
                foreach (Control x in this.Controls)
                {                    
                    x.ForeColor = Color.Black;                    
                }
            }
            else
            {
                //dark mode
                foreach (Control x in this.Controls)
                {
                    x.ForeColor = Color.White;
                }

            }
        }
    }
}
