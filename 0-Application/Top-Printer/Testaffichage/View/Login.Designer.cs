﻿namespace ProjetPrinter.View
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.bar = new System.Windows.Forms.Panel();
            this.logs = new System.Windows.Forms.Button();
            this.forgotpsw = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.usernamebox = new System.Windows.Forms.GroupBox();
            this.username = new System.Windows.Forms.TextBox();
            this.passwordbox = new System.Windows.Forms.GroupBox();
            this.password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.usernamebox.SuspendLayout();
            this.passwordbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // bar
            // 
            this.bar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.bar.Cursor = System.Windows.Forms.Cursors.Default;
            this.bar.Location = new System.Drawing.Point(0, 0);
            this.bar.Margin = new System.Windows.Forms.Padding(0);
            this.bar.Name = "bar";
            this.bar.Size = new System.Drawing.Size(520, 27);
            this.bar.TabIndex = 6;
            // 
            // logs
            // 
            this.logs.BackColor = System.Drawing.Color.White;
            this.logs.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.logs.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.logs.Location = new System.Drawing.Point(367, 180);
            this.logs.Name = "logs";
            this.logs.Size = new System.Drawing.Size(133, 34);
            this.logs.TabIndex = 7;
            this.logs.Text = "Login";
            this.logs.UseVisualStyleBackColor = false;
            // 
            // forgotpsw
            // 
            this.forgotpsw.AutoSize = true;
            this.forgotpsw.BackColor = System.Drawing.Color.Transparent;
            this.forgotpsw.FlatAppearance.BorderSize = 0;
            this.forgotpsw.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.forgotpsw.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.forgotpsw.ForeColor = System.Drawing.Color.White;
            this.forgotpsw.Location = new System.Drawing.Point(20, 180);
            this.forgotpsw.Name = "forgotpsw";
            this.forgotpsw.Size = new System.Drawing.Size(341, 34);
            this.forgotpsw.TabIndex = 17;
            this.forgotpsw.Text = "Mot de passe oublier ?";
            this.forgotpsw.UseVisualStyleBackColor = false;
            // 
            // usernamebox
            // 
            this.usernamebox.BackColor = System.Drawing.Color.Transparent;
            this.usernamebox.Controls.Add(this.username);
            this.usernamebox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernamebox.ForeColor = System.Drawing.Color.White;
            this.usernamebox.Location = new System.Drawing.Point(20, 30);
            this.usernamebox.Name = "usernamebox";
            this.usernamebox.Size = new System.Drawing.Size(480, 69);
            this.usernamebox.TabIndex = 21;
            this.usernamebox.TabStop = false;
            this.usernamebox.Text = "Username*";
            // 
            // username
            // 
            this.username.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.username.ForeColor = System.Drawing.SystemColors.GrayText;
            this.username.Location = new System.Drawing.Point(6, 25);
            this.username.MaxLength = 255;
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(468, 38);
            this.username.TabIndex = 0;
            this.username.Leave += new System.EventHandler(this.check);
            // 
            // passwordbox
            // 
            this.passwordbox.BackColor = System.Drawing.Color.Transparent;
            this.passwordbox.Controls.Add(this.password);
            this.passwordbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordbox.ForeColor = System.Drawing.Color.White;
            this.passwordbox.Location = new System.Drawing.Point(20, 105);
            this.passwordbox.Name = "passwordbox";
            this.passwordbox.Size = new System.Drawing.Size(480, 69);
            this.passwordbox.TabIndex = 21;
            this.passwordbox.TabStop = false;
            this.passwordbox.Text = "Password*";
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.ForeColor = System.Drawing.SystemColors.GrayText;
            this.password.Location = new System.Drawing.Point(6, 25);
            this.password.MaxLength = 255;
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(468, 38);
            this.password.TabIndex = 0;
            this.password.Leave += new System.EventHandler(this.check);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(140, 222);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(243, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "Projet Imprimante 2019 View : Maxime Sickenberg";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
            this.ClientSize = new System.Drawing.Size(520, 244);
            this.Controls.Add(this.forgotpsw);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.passwordbox);
            this.Controls.Add(this.usernamebox);
            this.Controls.Add(this.logs);
            this.Controls.Add(this.bar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.Text = "Login";
            this.Deactivate += new System.EventHandler(this.Close);
            this.Load += new System.EventHandler(this.Open);
            this.usernamebox.ResumeLayout(false);
            this.usernamebox.PerformLayout();
            this.passwordbox.ResumeLayout(false);
            this.passwordbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel bar;
        private System.Windows.Forms.Button logs;
        private System.Windows.Forms.Button forgotpsw;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox usernamebox;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.GroupBox passwordbox;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Label label1;
    }
}