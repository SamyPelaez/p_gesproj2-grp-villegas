﻿using System;
using System.Collections;
using System.Globalization;
using System.Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetPrinter.Controller;
using ProjetPrinter.Model;

namespace ProjetPrinter
{
    public partial class Home : UserControl
    {
        MainController HomeControler { get; set; }
        List<PrinterModel> Printers { get; set; }

        public Home()
        {
            InitializeComponent();
            
        }

        public void GetDefaultModel(List<PrinterModel> printers)
        {
            Printers = printers;
            Start();
        }




        public void Start()
        {
            int nbpriner = Printers.Count;

            bool[] open = new bool[nbpriner];
            Button[] btninfo = new Button[nbpriner];
            Button[] btnadd = new Button[nbpriner];
            Panel[] panel = new Panel[nbpriner];
            PictureBox[] img = new PictureBox[nbpriner];
            TableLayoutPanel[] Table1 = new TableLayoutPanel[nbpriner];
            //int nbRows = Printers.Count / 3;
            //int rest = nbRows * 3 - Printers.Count;

            //foreach(PrinterModel pm in Printers)
            //{

            //}
            //throw new NotImplementedException(); 
            // 1.- COMMENCER ICI --> Le layout panel doit ajouter de panels dinamiquement par rapport a la liste Printers qui a été passé avec la query generale.
            // 2.- Dans Projet.cs --> On doit construire une méthode a executer pour toutes les liste déroulantes.
            //                        Il faut récupérer toute les valeurs des liste que on passera au controlleur par l'une de ses méthodes

            int nbimg = 1;
            for (int H = 0; H < nbpriner; H++)
            {
                panel[H] = new Panel();
                panel[H].Name = Convert.ToString(H);
                panel[H].Size = new Size(420, 450);
                panel[H].TabIndex = H;
                panel[H].BackColor = Color.White;
                panel[H].Margin = new Padding(6);
                Test.Controls.Add(panel[H]);

                //table layout
                Table1[H] = new TableLayoutPanel();
                Table1[H].CellBorderStyle = TableLayoutPanelCellBorderStyle.None;
                Table1[H].RowCount = 10;
                Table1[H].ColumnCount = 1;
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
                Table1[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
                //Table1[H].ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));

                Table1[H].Location = new Point(5, 205);
                Table1[H].Name = "table1";
                Table1[H].Size = new Size(210, 240);
                Table1[H].BackColor = Color.White;
                

                Table1[H].Controls.Add(new Label() { Text = "Nom : " +Convert.ToString(Printers[H].Name),Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))),AutoSize = true}, 0, 0);
                Table1[H].Controls.Add(new Label() { Text = "Prix : "+Convert.ToString(Printers[H].LastPrice)+".-", Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true },1, 0);
                Table1[H].Controls.Add(new Label() { Text = "Constructeur : " + Convert.ToString(Printers[H].Builder.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 2, 0);
                StringBuilder strBlr = new StringBuilder();
                foreach (PapersCompatible pp in Printers[H].Papers)
                {
                    strBlr.Append(pp.Name + ", ");

                }

                Table1[H].Controls.Add(new Label() { Text = "Papier : "+strBlr.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 3, 0);
                Table1[H].Controls.Add(new Label() { Text = "Résolution : " + Convert.ToString(Printers[H].PrintResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 4, 0);
                Table1[H].Controls.Add(new Label() { Text = "Fournisseur : " + Convert.ToString(Printers[H].Provider.Name), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 5, 0);
                Table1[H].Controls.Add(new Label() { Text = "Date de sortie : " + Convert.ToString(Printers[H].ReleaseDate.Year), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 6, 0);
                Table1[H].Controls.Add(new Label() { Text = "Résolution du scan : "+Convert.ToString(Printers[H].ScanResolution), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 7, 0);
                Table1[H].Controls.Add(new Label() { Text = "Taille : "+Convert.ToString(Printers[H].Size.Height+ " * "+Printers[H].Size.Lenght +" * "+ Printers[H].Size.Width), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 8, 0);
                Table1[H].Controls.Add(new Label() { Text = "Sans-fil : " + Convert.ToString(Printers[H].Wireless), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 9, 0);

                StringBuilder strink = new StringBuilder();
                for(int i=0;i< Printers[H].Inks.Count;i++)
                {
                    strink.Append(Printers[H].Inks[i].Name + " : "+ Printers[H].Inks[i].Price+".-");

                }
                Table1[H].Controls.Add(new Label() { Text = "Consomable : " + strink.ToString(), Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0))), AutoSize = true }, 10, 0);

                panel[H].Controls.Add(Table1[H]);

                //bouton pour avoir les info des prix
                btninfo[H] = new Button();
                btninfo[H].Name = Convert.ToString(H);
                btninfo[H].Text = "Chart Prix";
                btninfo[H].Click += new EventHandler(this.Info_Click);
                btninfo[H].BackColor = Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
                btninfo[H].FlatStyle = FlatStyle.Flat;
                btninfo[H].Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                btninfo[H].ForeColor = Color.White;
                panel[H].Controls.Add(btninfo[H]);

                //bouton ajouteur au panier
                btnadd[H] = new Button();
                btnadd[H].Name = Convert.ToString(H);
                btnadd[H].Text = "Add to Panier";
                btnadd[H].TabStop = false;// 0, 173, 181
                btnadd[H].BackColor = Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(80)))), ((int)(((byte)(180)))));
                btnadd[H].FlatStyle = FlatStyle.Flat;
                btnadd[H].Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
                btnadd[H].ForeColor = Color.White;
                panel[H].Controls.Add(btnadd[H]);

                //image box pour l'emplacement des images
                img[H] = new PictureBox();
                panel[H].Controls.Add(img[H]);
                img[H].BackColor = Color.White;
                img[H].Name = "img" + H;
                img[H].BackgroundImageLayout = ImageLayout.Zoom;

                //emplacement + taille du bouton info
                btninfo[H].Location = new Point(245, 60);
                btninfo[H].Size = new Size(170, 50);

                //emplacement + taille du bouton ajouter au pannier
                btnadd[H].Location = new Point(245, 110);
                btnadd[H].Size = new Size(170, 50);

                //emplacement + taille + url fichier pour les image
                img[H].Location = new Point(5, 5);
                img[H].Size = new Size(200, 200);
                try
                {
                        img[H].BackgroundImage = Image.FromFile(@"../../../photo/Print Photo/" + Printers[H].Name + ".jpg");
                }
                catch
                {
                    img[H].BackgroundImage = Image.FromFile(@"../../Resources/Img/Error.png");
                }

                nbimg++;
                Refresh();
            }
        }
    

        public MainController AController { get; internal set; }

        private void Info_Click(object sender, EventArgs e)
        {
            Chart graphique = new Chart();
            graphique.StartPosition = FormStartPosition.Manual;
            graphique.Location = new Point((Parent.Location.X + Parent.Width / 2) - (graphique.Width / 2), (Parent.Location.Y + Parent.Height / 2) - (graphique.Height / 2));
            graphique.Show();
        }
    }
}
