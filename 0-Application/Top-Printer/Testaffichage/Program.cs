﻿///ETML
///Team : grp2c
///Authors: 
/// --> Pelaez, Samy        : Analysis and Initial planning; DB conceptualisation, DB .sql and data model; db connection; Interfaces; DAL data model integration; mvc control and edition...
/// --> Brunner, Theo       : Analisis, collection, control and formating of the required data; DAL query building;..
/// --> Villegas, Adrian    : Analisis and collection of required data; DB conceptualisation; Uwamp integration and db connection;...
/// --> Sickeberg, Maxime   : UI design; Views; Views Control Integration; Interface Integration;...
/// 
///Projet                   : P_gesproj2 - "P_imprimantes"
///Description              : The tasks are shared between a group of 4 people. We have been hired by  style PC-Top, an informatic material seller company to devellop a heavy client for their selling force
///                           allowing them to query a dataset of printers. They need a user interface which ease their capacity to query following customer characteristics demands to offer the best services.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using ProjetPrinter.Controller;
using ProjetPrinter.Model;
using ProjetPrinter.View;

namespace ProjetPrinter
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Création de la vue, du model et du contoller 

            /// <summary>
            /// Creation and initialize all the views
            /// </summary>
            MainView mainView = new MainView();
            ChartView chartView = new ChartView();
            LoginView loginView = new LoginView();
            SignView signView = new SignView();

            /// <summary>
            /// All views are passed to IoC container
            /// </summary>
            List<IView> views = new List<IView>();
            views.Add(mainView);
            views.Add(chartView);
            views.Add(loginView);
            views.Add(signView);

            /// <summary>
            /// Create and initialize all the models
            /// </summary>
            MainModel mainModel = new MainModel();
            DAL_Printer dalPrinter = new DAL_Printer();

            /// <summary>
            /// All the models are passed to IoC container
            /// </summary>
            //List<IModel> models = new List<IModel>();

            /// <summary>
            /// Creation of the Main Controller and pass both, view and model IoC containers
            /// </summary>
            MainController mainController = new MainController(views, mainModel);
            mainView.LoadComboBoxCollections();
            Application.Run(mainView);
        }
    }
}
