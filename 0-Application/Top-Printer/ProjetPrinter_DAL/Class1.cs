﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using ProjetPrinter.Model;


namespace ProjetPrinter
{
    public class DAL_Printer : IDAL
    {
        public string ServerName { get; set; }
        public string DBName { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }

        //variable de connection à la base mySQL
        public MySqlConnection mySqlConnection { get; set; }

        public DAL_Printer()
        {
            this.ServerName = "localhost";
            this.DBName = "db_printers";
            this.UserName = "root";
            this.UserPassword = "root";

            mySqlConnection = new MySqlConnection($"server={ServerName}; database={DBName}; uid={UserName}; pwd={UserPassword};");

        }

        public List<PrinterModel> GetDBPrinterQuery()
        {
            List<PrinterModel> ReturnedObjList = new List<PrinterModel>();


            //se connecte à la base de donnée
            //mySqlConnectionPrice.Open();



            //se connecte à la base de donnée
            //mySqlConnectionInk.Open();

            //Parameters of the Query
            string strWHEREAllDataPrint = @"t_printer.priName != 'Tank555'";

            string strSelectAllDataPrint = @"t_printer.idPrinter,
                                                t_printer.priLink, 
                                                t_printer.priName, 
                                                t_printer.priPrintResolution, 
                                                t_printer.priScanResolution, 
                                                t_printer.priPrintSpeed, 
                                                t_printer.priDouble_Sided,
                                                t_printer.priWireless, 
                                                t_printer.priWitdh, 
                                                t_printer.priLenght, 
                                                t_printer.priHeight, 
                                                t_printer.priWeight, 
                                                t_printer.priDateRelease,

                                                t_technology.tecType, 
                                                
                                                t_provider.idProvider, 
                                                t_provider.proName, 
                                                t_provider.proLink, 

                                                t_builder.idBuilder, 
                                                t_builder.buiName, 
                                                t_builder.buiBrand";

            string strJoinAllDataPrint = "t_printer Natural Join t_builder Natural Join t_provider Natural Join t_technology Natural Join t_useink Natural Join t_ink";
            string groupByPrint = "Group By t_printer.priName";

            string orderByIdPrint = "ORDER BY t_printer.idPrinter";

            string strPaper = "t_printer Natural Join t_usepaper Natural Join t_paper ";
            string strSelectPaper = "t_paper.papName, t_paper.idPaper";
            string strInk = "t_printer Natural Join t_useink Natural Join t_ink ";
            string strSelectInk = "t_ink.idInk, t_ink.inkName, t_ink.inkType, t_ink.inkPrice";
            string idPrint = "3";

            string strSelectProvider = "t_provider.idProvider, t_provider.proName ,t_provider.proLink";
            string strFromProvider = "t_evoprice Natural Join t_printer";

            string strSelectBuilder = "t_builder.idBuilder, t_builder.buiName ,t_builder.buibrand";
            string strFromBuilder = "t_evoprice Natural Join t_printer";

            string strSelectLastPrice = "t_evoprice.EvoPrice";
            string strFromLastPrice = "t_evoprice";

            string strSelectEvoPrice = "t_evoprice.EvoPrice, t_evoprice.EvoDate";
            string strFromEvoPrice = "t_evoprice Natural Join t_printer";
            //Build up the Query
            //MySqlCommand select = new MySqlCommand("SELECT " + strSelectAllDataPrint + " FROM " + strJoinAllDataPrint + " WHERE " + strWHEREAllDataPrint + groupByPrint, mySqlConnection);
            //MySqlCommand selectPrinter = new MySqlCommand("SELECT * FROM t_printer", mySqlConnectionPrinter);
            //déclarations des commandes
            //MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " WHERE " + strWHEREAllDataPrint", mySqlConnectionPaper);
            //déclarations des commandes
            //MySqlCommand selectInk = new MySqlCommand("SELECT * FROM t_ink", mySqlConnectionInk);
            ////déclarations des commandes
            //MySqlCommand selectPrice = new MySqlCommand("SELECT * FROM t_evoprice", mySqlConnectionPrice);


            //se connecte à la base de donnée
            using (mySqlConnection)
            {
                mySqlConnection.Open();

                MySqlCommand selectPrinter = new MySqlCommand($"SELECT {strSelectAllDataPrint} FROM t_printer {strJoinAllDataPrint} {groupByPrint} {orderByIdPrint}", mySqlConnection);
                //QueryDB
                MySqlDataReader printerReader = selectPrinter.ExecuteReader();
                //Read and Store the returned table
                while (printerReader.Read())
                {
                    PrinterModel pm = new PrinterModel();
                    ReturnedObjList.Add(pm);


                    pm.Id = Convert.ToInt32(printerReader["idPrinter"]);
                    pm.Name = printerReader["priName"].ToString();

                    pm.Size.Width = Convert.ToInt32(printerReader["priWitdh"]);
                    pm.Size.Lenght = Convert.ToInt32(printerReader["priLenght"]);
                    pm.Size.Height = Convert.ToInt32(printerReader["priHeight"]);

                    pm.Weight = Convert.ToInt32(printerReader["priWeight"]);
                    pm.RectoVerso = printerReader["priDouble_Sided"].ToString();
                    pm.ArticleLink = printerReader["priLink"].ToString();
                    pm.ReleaseDate = Convert.ToDateTime(printerReader["priDateRelease"]);
                    pm.PrintResolution = printerReader["priPrintResolution"].ToString();
                    pm.ScanResolution = printerReader["priScanResolution"].ToString();
                    pm.Wireless = printerReader["priWireless"].ToString();

                    pm.Provider.Id = Convert.ToInt32(printerReader["idProvider"].ToString());
                    pm.Provider.Name = printerReader["proName"].ToString();
                    pm.Provider.Link = printerReader["proLink"].ToString();

                    pm.Builder.Id = Convert.ToInt32(printerReader["idBuilder"].ToString());
                    pm.Builder.Name = printerReader["buiName"].ToString();
                    pm.Builder.Brand = printerReader["buiBrand"].ToString();

                }
                printerReader.Close();
                mySqlConnection.Close();
            }

            //GET Provider/

            //using (mySqlConnection)
            //{
            //    mySqlConnection.Open();

            //    //For each printer gets the compatible papers
            //    foreach (PrinterModel pm in ReturnedObjList)
            //    {
            //        MySqlCommand selectProvider = new MySqlCommand("SELECT " + strSelectBuilder + " FROM " + strFromBuilder + " WHERE " + "t_printer.idPrinter=" + pm.Id, mySqlConnection);
            //        //Execute query and read
            //        MySqlDataReader reader = selectProvider.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            pm.Provider.Id = Convert.ToInt32(reader["idProvider"].ToString());
            //            pm.Provider.Name = reader["proName"].ToString();
            //            pm.Provider.Link = reader["proLink"].ToString();
            //        }

            //        reader.Close();
            //    }

            //    mySqlConnection.Close();
            //}

            //GET Builder/

            //using (mySqlConnection)
            //{
            //    mySqlConnection.Open();

            //    //For each printer gets the compatible papers
            //    foreach (PrinterModel pm in ReturnedObjList)
            //    {
            //        MySqlCommand selectBuilder = new MySqlCommand("SELECT " + strSelectBuilder + " FROM " + strFromBuilder + " WHERE " + "t_printer.idPrinter=" + pm.Id, mySqlConnection);
            //        //Execute query and read
            //        MySqlDataReader reader = selectBuilder.ExecuteReader();

            //        while (reader.Read())
            //        {
            //            pm.Builder.Id       = Convert.ToInt32(reader["idBuilder"].ToString());
            //            pm.Builder.Name     = reader["buiName"].ToString();
            //            pm.Builder.Brand    = reader["buiBrand"].ToString();
            //        }

            //        reader.Close();
            //    }

            //    mySqlConnection.Close();
            //}

            // GET Last Prices //

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectLastPrices = new MySqlCommand("SELECT " + strSelectLastPrice + " FROM " + strFromLastPrice + " WHERE " + "t_evoprice.idPrinter=" + pm.Id + " AND t_evoprice.evodate =(select max(t_evoprice.EvoDate) from t_evoprice where + t_evoprice.idPrinter = " + pm.Id + ")", mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectLastPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        pm.LastPrice = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET ALL PRICES//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPrices = new MySqlCommand("SELECT " + strSelectEvoPrice + " FROM " + strFromEvoPrice + " WHERE " + "t_printer.idPrinter=" + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPrices.ExecuteReader();

                    while (reader.Read())
                    {
                        PriceInTime pic = new PriceInTime();
                        pm.PriceEvolution.Add(pic);

                        pic.Date = Convert.ToDateTime(reader["evodate"]);
                        pic.Price = Convert.ToInt32(reader["evoprice"]);
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }


            //GET COMPATIBLE PAPERS//

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible papers
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectPaper = new MySqlCommand("SELECT " + strSelectPaper + " FROM " + strPaper + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Execute query and read
                    MySqlDataReader reader = selectPaper.ExecuteReader();

                    while (reader.Read())
                    {
                        PapersCompatible pc = new PapersCompatible();
                        pm.Papers.Add(pc);

                        pc.Id = Convert.ToInt32(reader["idPaper"]);
                        pc.Name = reader["papName"].ToString();
                    }

                    reader.Close();
                }

                mySqlConnection.Close();
            }

            //GET COMPATIBLE INKS

            using (mySqlConnection)
            {
                mySqlConnection.Open();

                //For each printer gets the compatible inks
                foreach (PrinterModel pm in ReturnedObjList)
                {
                    MySqlCommand selectInk = new MySqlCommand("SELECT " + strSelectInk + " FROM " + strInk + " WHERE " + "t_printer.idPrinter= " + pm.Id, mySqlConnection);
                    //Db connect
                    MySqlDataReader reader = selectInk.ExecuteReader();

                    while (reader.Read())
                    {
                        InkCompatible ic = new InkCompatible();
                        pm.Inks.Add(ic);

                        ic.Id = Convert.ToInt32(reader["idInk"]);
                        ic.Name = reader["inkName"].ToString();
                        ic.Type = reader["inkType"].ToString();
                        ic.Price = Convert.ToInt32(reader["inkPrice"]);

                    }

                    reader.Close();
                }

                mySqlConnection.Close();

            }

            return ReturnedObjList;
        }
    }
}
