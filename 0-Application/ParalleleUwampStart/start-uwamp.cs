﻿using System;
using System.Collections.Generic;
using System.Diagnostics;      // Necessaire
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace uwamp2
{
    class Program
    {
        static void Main(string[] args)
        {

            Process myProcess = new Process();

            //Console.WriteLine(Directory.GetCurrentDirectory());

            myProcess = DessignateUWAMP(myProcess);

            myProcess.Start();
            
            //Console.WriteLine("Press enter to close UwAmp");
            //Console.ReadLine();

            myProcess.Kill();
            //Console.WriteLine("OK");
            //Console.ReadLine();
        }

        public static Process DessignateUWAMP(Process _myProcess)
        {
            string uwAmpPath = ".\\UwAmp\\UwAmp.exe";
            _myProcess.StartInfo.FileName = uwAmpPath;
            _myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            return _myProcess;
        }
    }
}
